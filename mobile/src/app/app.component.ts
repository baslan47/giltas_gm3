import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { Storage } from '@ionic/storage';
import { LoginPage } from '../pages/login/login';
import { SettingsPage } from '../pages/settings/settings';
import { ServerService } from "../classes/server.service";
import { Subscription } from "rxjs/Subscription";
import { TranslateService } from "@ngx-translate/core";
import { CampaignsPage } from "../pages/campaigns/campaigns";
import { OpenOrdersPage } from '../pages/open-orders/open-orders';
import { ShipmentsPage } from '../pages/shipments/shipments';
//import {ImageLoaderConfig} from "ionic-image-loader";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = LoginPage;
  pushPages: Array<{ title: string, icon: string, component: any }>;
  onPauseSubscription: Subscription;

  constructor(platform: Platform, public splashScreen: SplashScreen, public serverService: ServerService,
    public statusBar: StatusBar, public storage: Storage, private translateService: TranslateService,
    // private imageLoaderConfig:ImageLoaderConfig
  ) {
    platform.ready().then(() => {
      this.splashScreen.hide();
      // this.imageLoaderConfig.enableDebugMode();
      // this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
      // this.imageLoaderConfig.setFallbackUrl('assets/imgs/logo.png');
      // this.imageLoaderConfig.setMaximumCacheAge(24 * 60 * 60 * 1000);
      this.statusBar.styleDefault();
      this.initTranslate();
      console.log(this.serverService.isOpenCamera)

      this.onPauseSubscription = platform.pause.subscribe(() => {
        console.log('onPauseSubscription'); // 30 sn
        if (this.serverService.isOpenCamera == false) {
          this.logout();
        }
      });

    });

    this.pushPages = [
      { title: 'Settings', icon: 'basket', component: SettingsPage },
      { title: 'Log Out', icon: 'log-out', component: LoginPage }
    ];
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translateService.setDefaultLang('tr');
    /*
    if (this.translateService.getBrowserLang() !== undefined) {
      this.translateService.use(this.translate.getBrowserLang());
    } else {
      this.translateService.use('en'); // Set your language here
    }
    */
  }

  goToCampaignsPage() {
    this.nav.push(CampaignsPage);
  }

  goToSettings(p) {
    this.nav.push(SettingsPage);
  }

  logout() {
    this.serverService.disconnect()
      .then(
        (res: any) => {
          console.log(res);
          this.serverService.Settings.Token = null;
          //this.storage.set('settings', this.serverService.Settings);
          //todo storage eklemek gerekebilir
          //internet bağlantısı yoksa storage bakarak bağlanıcak
          //kullanıcı adı şifresini storage de tutabiliriz
          //network plugin ine bak (ping at storage bak)
          this.storage.clear();
          //this.nav.setRoot(LoginPage);
        },
        (error) => console.log(error)
      );

    this.nav.setRoot(LoginPage);
  }
}
