import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {ProductsPage} from '../pages/products/products';
import {CustomersPage} from '../pages/customers/customers';
import {HomePage} from '../pages/home/home';
import {TabsPage} from '../pages/tabs/tabs';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from '../pages/login/login';
import {ServerService} from '../classes/server.service';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ProductPage} from '../pages/product/product';
import {ProposalPage} from '../pages/proposal/proposal';
import {CustomerPage} from '../pages/customer/customer'
import {BarcodeScanner} from '@ionic-native/barcode-scanner';
import {SelectCustomerPage} from '../pages/select-customer/select-customer';
import {SelectProductPage} from '../pages/select-product/select-product';
import {IonicStorageModule} from '@ionic/storage';
import {SettingsPage} from '../pages/settings/settings';
import {OrderDetailPage} from "../pages/order-detail/order-detail";
import {PhotoViewer} from "@ionic-native/photo-viewer";
import {AttributesPage} from "../pages/attributes/attributes";
import {AttributeTypesPage} from "../pages/attribute-types/attribute-types";
import {ProductMenuPage} from "../pages/product-menu/product-menu";
import {InventoryPage} from "../pages/inventory/inventory";
import {CampaignCreatePage} from "../pages/campaign-create/campaign-create";
import {CampaignsPage} from "../pages/campaigns/campaigns";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {InstallmentsDetailPage} from "../pages/installments-detail/installments-detail";
import {BarcodeTestPage} from "../pages/barcode-test/barcode-test";
import {InstallmentsPage} from "../pages/installments/installments";
import {CountryPage} from "../pages/country/country";
import {WarehousePage} from "../pages/warehouse/warehouse";
import {InvoiceDetailPage} from "../pages/invoice-detail/invoice-detail";
import {ReportsPage} from "../pages/reports/reports";
import {ExtraReportPage} from "../pages/extra-report/extra-report";
import {OrderReportPage} from "../pages/order-report/order-report";
// import {IonicImageLoader} from 'ionic-image-loader';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { LazyLoadImageModule } from 'ng2-lazyload-image';
import { DistributionPage } from '../pages/distribution/distribution';
import { CampaignMainPage } from '../pages/campaign-main/campaign-main';
import { ModalTimePeriodPage } from '../pages/modal-time-period/modal-time-period';
import { ModalFilterOrderPage } from '../pages/modal-filter-order/modal-filter-order';
import { ModalAmountRulePage } from '../pages/modal-amount-rule/modal-amount-rule';
import { ModalCampaignGroupPage } from '../pages/modal-campaign-group/modal-campaign-group';
import { ModalProductListPage } from '../pages/modal-product-list/modal-product-list';
import { ModalCustomerListPage } from '../pages/modal-customer-list/modal-customer-list';
import { ModalFilterReportPage } from '../pages/modal-filter-report/modal-filter-report';
import { CampaignSuggestsPage } from '../pages/campaign-suggests/campaign-suggests';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProposalTypePage } from '../pages/proposal-type/proposal-type';
import { RetailCustomerPage } from '../pages/retail-customer/retail-customer';
import { ProducInventoryReportPage } from '../pages/produc-inventory-report/produc-inventory-report';
import { OtherwarehousePage } from '../pages/otherwarehouse/otherwarehouse';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { ModalCustomersPage } from '../pages/modal-customers/modal-customers';
import { ModalProductfilterPage } from '../pages/modal-productfilter/modal-productfilter';
import { ModalGuarantorPage } from '../pages/modal-guarantor/modal-guarantor';
import { Camera } from '@ionic-native/camera';
import { ModalAddaddressPage } from '../pages/modal-addaddress/modal-addaddress';
import { ModalAttributeType17Page } from '../pages/modal-attribute-type17/modal-attribute-type17';
import { AwsTestPage } from '../pages/aws-test/aws-test';
import { AwsServiceProvider } from '../providers/s3-service';
import { ProposalReceiptPage } from '../pages/proposal-receipt/proposal-receipt';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { CampaignDetailPage } from '../pages/campaign-detail/campaign-detail';
import { ModalCampaignProductsPage } from '../pages/modal-campaign-products/modal-campaign-products';
import { IonicStepperModule } from 'ionic-stepper';
import { EmailComposer } from '@ionic-native/email-composer';
import { OpenOrdersPage } from '../pages/open-orders/open-orders';
import { ShipmentsPage } from '../pages/shipments/shipments';
import { NotificationsPage } from '../pages/notifications/notifications';


@NgModule({
  declarations: [
    MyApp,
    ProductsPage,
    CustomersPage,
    HomePage,
    TabsPage,
    LoginPage,
    ProductPage,
    ProposalPage,
    CustomerPage,
    SelectCustomerPage,
    SelectProductPage,
    SettingsPage,
    OrderDetailPage,
    AttributesPage,
    AttributeTypesPage,
    ProductMenuPage,
    InventoryPage,
    CampaignsPage,
    CampaignCreatePage,
    CampaignMainPage,
    CampaignSuggestsPage,
    CampaignDetailPage,
    DistributionPage,
    InstallmentsPage,
    InstallmentsDetailPage,
    BarcodeTestPage,
    CountryPage,
    WarehousePage,
    InvoiceDetailPage,
    ReportsPage,
    ExtraReportPage,
    OrderReportPage,
    ModalAddaddressPage,
    ModalTimePeriodPage,
    ModalFilterOrderPage,
    ModalAmountRulePage,
    ModalCampaignGroupPage,
    ModalFilterReportPage,
    ModalProductListPage,
    ModalCustomerListPage,
    ModalCampaignProductsPage,
    ProposalTypePage,
    RetailCustomerPage,
    ProducInventoryReportPage,
    OtherwarehousePage,
    ModalCustomersPage,
    ModalProductfilterPage,
    ModalGuarantorPage,
    ModalAttributeType17Page,
    AwsTestPage,
    ProposalReceiptPage,
    OpenOrdersPage,
    ShipmentsPage,
    NotificationsPage
  ],
  imports: [
    BrowserModule,
    IonicStepperModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
    }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    SelectSearchableModule,
    IonicStorageModule.forRoot(),
    LazyLoadImageModule,
    BrMaskerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ProductsPage,
    CustomersPage,
    HomePage,
    TabsPage,
    LoginPage,
    ProductPage,
    ProposalPage,
    CustomerPage,
    SelectCustomerPage,
    SelectProductPage,
    SettingsPage,
    OrderDetailPage,
    AttributesPage,
    AttributeTypesPage,
    ProductMenuPage,
    InventoryPage,
    CampaignsPage,
    CampaignCreatePage,
    CampaignMainPage,
    CampaignSuggestsPage,
    CampaignDetailPage,
    DistributionPage,
    InstallmentsPage,
    InstallmentsDetailPage,
    BarcodeTestPage,
    CountryPage,
    WarehousePage,
    InvoiceDetailPage,
    ReportsPage,
    ExtraReportPage,
    OrderReportPage,
    ModalAddaddressPage,
    ModalTimePeriodPage,
    ModalFilterOrderPage,
    ModalAmountRulePage,
    ModalCampaignGroupPage,
    ModalFilterReportPage,
    ModalProductListPage,
    ModalCustomerListPage,
    ModalCampaignProductsPage,
    ProposalTypePage,
    RetailCustomerPage,
    ProducInventoryReportPage,
    OtherwarehousePage,
    ModalCustomersPage,
    ModalProductfilterPage,
    ModalGuarantorPage,
    ModalAttributeType17Page,
    AwsTestPage,
    ProposalReceiptPage,
    OpenOrdersPage,
    ShipmentsPage,
    NotificationsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    File,
    FileOpener,
    ServerService,
    BarcodeScanner,
    Camera,    
    IonicStorageModule,
    MyApp,
    PhotoViewer,
    AwsServiceProvider,   
    EmailComposer
  ]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/languages/', '.json');
}
