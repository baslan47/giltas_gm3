
-- =============================================
-- 1.2.7
-- =============================================
ALTER PROCEDURE [dbo].[G3_CustomerListRetail](@FirstLastName AS VARCHAR(50))
AS
BEGIN

DECLARE @DynamicQuery NVARCHAR(MAX)
SET @DynamicQuery = N'select top 100 acc.CurrAccCode, acc.FirstName , acc.LastName, phone.CommAddress Phone ,email.CommAddress Email ,mobile.CommAddress Mobile,
					(select top 1 Address from prCurrAccPostalAddress where CurrAccCode = acc.CurrAccCode and CurrAccTypeCode = 4 AND AddressTypeCode = 1 ) Address 
					from cdCurrAcc acc
					left join prCurrAccCommunication phone on acc.CurrAccCode = phone.CurrAccCode and phone.CommunicationTypeCode=1
					left join prCurrAccCommunication email on acc.CurrAccCode = email.CurrAccCode and email.CommunicationTypeCode=3
					left join prCurrAccCommunication mobile on acc.CurrAccCode = mobile.CurrAccCode and mobile.CommunicationTypeCode=7'

declare @whr NVARCHAR(MAX)
set @whr = 'where acc.CurrAccTypeCode = 4 '

if(@FirstLastName != '')
	set @whr = @whr + ' and ( acc.FirstLastname like ''%' + @FirstLastName 
	+ '%'' OR acc.CurrAccCode like ''%' + @FirstLastName 
	+ '%'' OR acc.TaxNumber like ''%' + @FirstLastName 
	+ '%'' OR acc.IdentityNum like ''%' + @FirstLastName
	+ '%'' OR phone.CommAddress like ''%' + @FirstLastName
	+ '%'' OR email.CommAddress like ''%' + @FirstLastName
	+ '%'' OR mobile.CommAddress like ''%' + @FirstLastName + '%'')'

set @DynamicQuery = @DynamicQuery + @whr
print @DynamicQuery
          
execute(@DynamicQuery)

END

-- =============================================
-- 
-- =============================================


ALTER PROCEDURE [dbo].[G3_WholesaleOrders](@Count AS VARCHAR(50), @WhereStr AS VARCHAR(100))
AS
BEGIN

DECLARE @DynamicQuery NVARCHAR(MAX)
SET @DynamicQuery = N'select top ' + @Count +' o.OrderNumber, o.OrderDate, cr.CurrAccDescription, ao.Doc_Amount
			from trOrderHeader o
			left join cdCurrAccDesc cr on cr.CurrAccCode = o.CurrAccCode
			left join AllOrders ao on ao.OrderNumber = o.OrderNumber
			where o.CurrAccTypeCode = 3 ' + @WhereStr +'
			order by o.CreatedDate desc'

print @DynamicQuery         
execute(@DynamicQuery)

END

-- =============================================
-- YENİ PROSEDÜR
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[G3_GetCustomerPersonalInfo](@CurrAccCode AS VARCHAR(50), @LangCode AS VARCHAR(4))
AS
BEGIN

 SELECT *
from prCurrAccPersonalInfo
where CurrAccCode=@CurrAccCode 
END


-- =============================================
-- =============================================


USE [KonfSun]
GO
/****** Object:  StoredProcedure [dbo].[G3_WholesaleOrders]    Script Date: 17.04.2019 12:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[G3_WholesaleOrders](@Count AS VARCHAR(50), @WhereStr AS VARCHAR(100))
AS
BEGIN

DECLARE @DynamicQuery NVARCHAR(MAX)
SET @DynamicQuery = N'select top ' + @Count +' o.OrderNumber, o.OrderDate, cr.CurrAccDescription,  o.CreatedDate, Sum(ao.Doc_AmountVI) as Tutar
			from trOrderHeader o
			left join cdCurrAccDesc cr on cr.CurrAccCode = o.CurrAccCode
			left join AllOrders ao on ao.OrderNumber = o.OrderNumber
			where o.CurrAccTypeCode = 3 ' + @WhereStr +'	
			group by o.OrderNumber, o.OrderDate, cr.CurrAccDescription, o.CreatedDate		
			order by o.CreatedDate desc'

print @DynamicQuery         
execute(@DynamicQuery)

END





-- =============================================
-- =============================================





ALTER PROCEDURE [dbo].[G3_GetProductPrices](@ItemCode AS VARCHAR(50), @PriceGroupCode AS VARCHAR(50))
AS
BEGIN	
	SELECT PriceGroupCode			= cdPriceGroup.PriceGroupCode
	 , PriceGroupDescription	= ISNULL((SELECT PriceGroupDescription FROM cdPriceGroupDesc WITH(NOLOCK) WHERE cdPriceGroupDesc.PriceGroupCode = cdPriceGroup.PriceGroupCode AND cdPriceGroupDesc.LangCode = 'tr') , SPACE(0))
	 , PriceListNumber			= ISNULL(ProductValidPrices.PriceListNumber , SPACE(0))
	 , PriceListDate			= ISNULL(ProductValidPrices.PriceListDate , SPACE(0))
	 , ValidDate				= ISNULL(ProductValidPrices.ValidDate , SPACE(0))
	 , ValidTime				= ISNULL(ProductValidPrices.ValidTime , SPACE(0))
	 , ExpiredDate				= ISNULL(ProductValidPrices.ExpiredDate  , SPACE(0))
	 , ExpiredTime				= ISNULL(ProductValidPrices.ExpiredTime  , SPACE(0))
	 , IsTaxIncluded			= ISNULL(ProductValidPrices.IsTaxIncluded , SPACE(0))
	
	 , ProductCode				= cdItem.ItemCode
	 , ProductDescription		= ISNULL((SELECT ItemDescription FROM cdItemDesc WITH(NOLOCK) WHERE cdItemDesc.ItemTypeCode = cdItem.ItemTypeCode AND cdItemDesc.ItemCode = cdItem.ItemCode AND cdItemDesc.LangCode = 'tr'),SPACE(0))
	 , ColorCode				= ISNULL(ProductValidPrices.ColorCode , SPACE(0))
	 , ColorDescription			= ISNULL((SELECT ColorDescription FROM cdColorDesc WITH(NOLOCK) WHERE cdColorDesc.ColorCode = ProductValidPrices.ColorCode AND cdColorDesc.LangCode = 'tr'),SPACE(0))
	 , ItemDim1Code				= ISNULL(ProductValidPrices.ItemDim1Code , SPACE(0))
	 , ItemDim2Code				= ISNULL(ProductValidPrices.ItemDim2Code , SPACE(0))
	 , ItemDim3Code				= ISNULL(ProductValidPrices.ItemDim3Code , SPACE(0))
	 , UnitOfMeasureCode		= ISNULL(ProductValidPrices.UnitOfMeasureCode , SPACE(0))
	 , PaymentPlanCode			= ISNULL(ProductValidPrices.PaymentPlanCode , SPACE(0))
	 , DocCurrencyCode			= ISNULL(ProductValidPrices.DocCurrencyCode , SPACE(0))
	 , Price					= ISNULL(ProductValidPrices.Price , SPACE(0))
	 , PaymentPlanDescription   = ISNULL(cdPaymentPlanDesc.PaymentPlanDescription , 'Pesin Fiyat')

	FROM cdItem WITH(NOLOCK)		
		INNER JOIN cdPriceGroup 
			ON cdPriceGroup.PriceGroupCode <> SPACE(0)
			AND PriceGroupCode = @PriceGroupCode
			AND cdPriceGroup.IsBlocked = 0
		LEFT OUTER JOIN ProductValidPrices(GETDATE(), GETDATE())
			ON ProductValidPrices.ItemTypeCode		= cdItem.ItemTypeCode 
			AND ProductValidPrices.ItemCode			= cdItem.ItemCode
			AND ProductValidPrices.PriceGroupCode	= cdPriceGroup.PriceGroupCode 
		LEFT JOIN cdPaymentPlanDesc
			ON cdPaymentPlanDesc.PaymentPlanCode = ProductValidPrices.PaymentPlanCode
		
		
	WHERE cdItem.ItemTypeCode = 1 AND cdItem.ItemCode = @ItemCode
END;




-- =============================================
-- =============================================




ALTER PROCEDURE [dbo].[G3_CustomerListRetail](@FirstLastName AS VARCHAR(50))
AS
BEGIN

DECLARE @DynamicQuery NVARCHAR(MAX)
SET @DynamicQuery = N'select top 100 acc.CurrAccCode, acc.FirstName , acc.LastName, acc.IdentityNum, acc.TaxOfficeCode, acc.TaxNumber, phone.CommAddress Phone ,email.CommAddress Email ,mobile.CommAddress Mobile,
					(select top 1 Address from prCurrAccPostalAddress where CurrAccCode = acc.CurrAccCode and CurrAccTypeCode = 4 AND AddressTypeCode = 1 ) Address 
					from cdCurrAcc acc
					left join prCurrAccCommunication phone on acc.CurrAccCode = phone.CurrAccCode and phone.CommunicationTypeCode=1
					left join prCurrAccCommunication email on acc.CurrAccCode = email.CurrAccCode and email.CommunicationTypeCode=3
					left join prCurrAccCommunication mobile on acc.CurrAccCode = mobile.CurrAccCode and mobile.CommunicationTypeCode=7 '

declare @whr NVARCHAR(MAX)
set @whr = 'where acc.CurrAccTypeCode = 4 '

if(@FirstLastName != '')
	set @whr = @whr + ' and ( acc.FirstLastname like ''%' + @FirstLastName 
	+ '%'' OR acc.CurrAccCode like ''%' + @FirstLastName 
	+ '%'' OR acc.TaxNumber like ''%' + @FirstLastName 
	+ '%'' OR acc.IdentityNum like ''%' + @FirstLastName
	+ '%'' OR phone.CommAddress like ''%' + @FirstLastName
	+ '%'' OR email.CommAddress like ''%' + @FirstLastName
	+ '%'' OR mobile.CommAddress like ''%' + @FirstLastName + '%'')'

set @DynamicQuery = @DynamicQuery + @whr
print @DynamicQuery
          
execute(@DynamicQuery)

END

