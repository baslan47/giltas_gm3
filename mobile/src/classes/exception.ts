export class Data {
}

export class Exception {
  ExceptionMessage: string;
  StackTrace: string;
  Source: string;
  Data: Data;
  InnerException?: any;
}
