import {Component, OnInit} from '@angular/core';
import {AlertController, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ServerService} from "../../classes/server.service";
import {AttributesPage} from "../attributes/attributes";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'page-attribute-types',
  templateUrl: 'attribute-types.html',
})
export class AttributeTypesPage implements OnInit {

  MarkaList = [];
  MarkaListTmp = [];
  public myInput: string = '';
  userData = {"ProcName": "", "Parameters": [{"Name": "Language", "Value": "TR"},{"Name": "AttributeTypeDescription", "Value": ""}]};
  loading: Loading;  

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
              public serverService: ServerService, public alertCtrl: AlertController,
              public translateService: TranslateService) {
  }

  ngOnInit() {
    this.presentLoading();
    this.getAttributeTypes();
    
  }

  onInput(event: any) {
    //this.MarkaListTmp = this.MarkaList.filter( item => {return item.AttributeTypeDescription.toLowerCase().includes(this.myInput);});  
    this.getAttributeTypes();
    return this.MarkaListTmp;
  }

  onCancel(){
    this.MarkaListTmp = this.MarkaList;
  }
  getAttributeTypes(){
    this.userData.ProcName = "G3_GetAttributeTypes";
    this.userData.Parameters[1].Value = this.myInput;
    this.serverService.getAny(this.userData).then(res => {
      //console.log(res);      
      this.MarkaList = res;
      console.log(this.MarkaList);
      this.MarkaListTmp = this.MarkaList;      
      this.dismissLoading();
    }).catch(this.handleError);
  }

  goToAttributePage(attributeTypeCode: any) {
    this.navCtrl.push(AttributesPage, {attributeTypeCode: attributeTypeCode});
  }

  handleError(error: any) {
    this.dismissLoading();
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string[]) => {
      this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  showAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }
}
