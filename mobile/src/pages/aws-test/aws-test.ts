import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  Platform,
  AlertController,
  ViewController
} from "ionic-angular";
import { Camera } from "@ionic-native/camera";
import { AwsServiceProvider } from "../../providers/s3-service";

@Component({
  selector: 'page-aws-test',
  templateUrl: 'aws-test.html',
})
export class AwsTestPage {

  public titleName: string;
  public imageData: string;
  public imageData2: string;
  public imageView: string;
  public imageView2: string;
  public activeback: boolean = false;
  public imageName: string;
  data: any = [];

  constructor(
    public navCtrl: NavController, public navParams: NavParams, public s3Service: AwsServiceProvider,
      public platform: Platform, private camera: Camera, private loader: AlertController,
      public viewCtrl: ViewController) {
        this.titleName = "T.C Kimlik Ön Yüz Resim Ekle"
      }  

  takePictureWeb() {
    let self: any = this;
    var fup = {
        fileSelector: document.createElement('input'),
        outputArea: undefined,
        listImages: function (evt) {
            var file = evt.target.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                let data: any = e.target                
                self.imageView = data.result; 
                self.imageData = data.result;                                           
            };
            reader.readAsDataURL(file);
        }
    };

    fup.fileSelector.setAttribute('type', 'file');
    fup.fileSelector.setAttribute('accept', '.jpg');
    fup.fileSelector.onchange = function (event) {
        fup.listImages(event);
    };
    fup.fileSelector.click();
  }

  takePictureWebFront() {
    let self: any = this;
    var fup = {
        fileSelector: document.createElement('input'),
        outputArea: undefined,
        listImages: function (evt) {
            var file = evt.target.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                let data: any = e.target                
                self.imageView2 = data.result; 
                self.imageData2 = data.result;                                           
            };
            reader.readAsDataURL(file);
        }
    };

    fup.fileSelector.setAttribute('type', 'file');
    fup.fileSelector.setAttribute('accept', '.jpg');
    fup.fileSelector.onchange = function (event) {
        fup.listImages(event);
    };
    fup.fileSelector.click();
  }

  uploadPhotoFront() {    
    this.s3Service.text_rekognition(this.imageData, this.activeback).then(
      res => {
        this.activeback = true;                     
        this.imageData = "";
        this.imageView = "";      
        this.titleName = "T.C Kimlik Arka Yüz Resim Ekle";                    
      }
    )         
  }

  uploadPhotoBack(){
    this.s3Service.text_rekognition(this.imageData2, this.activeback).then(
      res => {
        this.data = res;                          
        this.closeModalCancel(this.data); 
      }
    )   
  }

  closeModalCancel(data){    
    this.viewCtrl.dismiss({result: data});
  }
}
