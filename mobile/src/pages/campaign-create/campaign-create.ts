import { Component } from '@angular/core';
import {
  NavController, NavParams, ModalController,
  Loading, LoadingController, ActionSheetController,
  AlertController
} from 'ionic-angular';
import { TranslateService } from "@ngx-translate/core";
import { ServerService } from "../../classes/server.service";
import { CampaignSuggestsPage } from '../campaign-suggests/campaign-suggests';

@Component({
  selector: 'page-campaign-create',
  templateUrl: 'campaign-create.html',
})
export class CampaignCreatePage {

  loading: Loading;
  itemArray: any[] = [];
  alert: any;
  stepperStuation: any = 0;

  campaignData = { "ProcName": "" };
  lowSaleItems = { "ProcName": "", "Parameters": [{ "Name": "Day", "Value": "" }] };
  notSaleItems = { "ProcName": "", "Parameters": [{ "Name": "Day", "Value": "" }] };
  seasonItems = { "ProcName": "", "Parameters": [{ "Name": "AttributeType", "Value": "" }, { "Name": "SeasonCode", "Value": "" }] };

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public loadingCtrl: LoadingController, public translateService: TranslateService,
    public actionSheetCtrl: ActionSheetController, public alertCtrl: AlertController,
    public serverService: ServerService) {

  }

  ngOnInit() {
    // this.getAllItems();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CampaignPage');
  }

  selectChange(e) {
    console.log(e);
    this.stepperStuation = e;
  }

  getLowSaleItems() {
    this.lowSaleItems.ProcName = 'G3AI_GetLowSaleItems';
    this.lowSaleItems.Parameters[0].Value = '1000';//1000 günlük veri
    this.serverService.getAny(this.lowSaleItems).then(res => {
      console.log('G3AI_GetLowSaleItems', res);
      if (res.length >= 30) {
        for (let i = 0; i < 30; i++) {
          this.itemArray.push(res[i]);
        }
      } else {
        for (let i = 0; i < res.length; i++) {
          this.itemArray.push(res[i]);
        }
      }
      // console.log(this.itemArray);
    })
  }
  start() {
    this.getAllItems();
  }

  getNotSaleItems() {
    this.notSaleItems.ProcName = 'G3AI_GetNotSaleItems';
    this.notSaleItems.Parameters[0].Value = '30';
    this.serverService.getAny(this.notSaleItems).then(res => {
      console.log('G3AI_GetNotSaleItems', res);
      if (res.length >= 30) {
        for (let i = 0; i < 30; i++) {
          this.itemArray.push(res[i]);
        }
      } else {
        for (let i = 0; i < res.length; i++) {
          this.itemArray.push(res[i]);
        }
      }
      // console.log(this.itemArray);

    })
  }

  getSeasonItems() {
    this.seasonItems.ProcName = 'G3AI_GetSeasonList';
    this.seasonItems.Parameters[0].Value = '1'; // özellik kodu
    this.seasonItems.Parameters[1].Value = '01'; // YAZ - KIŞ
    this.serverService.getAny(this.seasonItems).then(res => {
      console.log('G3AI_GetSeasonList', res);
      if (res.length >= 30) {
        for (let i = 0; i < 30; i++) {
          this.itemArray.push(res[i]);
        }
      } else {
        for (let i = 0; i < res.length; i++) {
          this.itemArray.push(res[i]);
        }
      }
      //console.log(this.itemArray);
    })
  }

  getSortedInventoryList() {
    this.campaignData.ProcName = 'G3AI_SortInventory';
    this.serverService.getAny(this.campaignData).then(res => {
      console.log('G3AI_SortInventory', res);
      if (res.length >= 30) {
        for (let i = 0; i < 30; i++) {
          this.itemArray.push(res[i]);
        }
      } else {
        for (let i = 0; i < res.length; i++) {
          this.itemArray.push(res[i]);
        }
      }
      console.log(this.itemArray);
      // this.dismissLoading();

    })
  }

  getAllItems() {
    this.presentLoading();
    this.getLowSaleItems();
    this.getNotSaleItems();
    this.getSeasonItems();
    this.getSortedInventoryList();
    setTimeout(() => {
      this.changeLoadingContent(0)
    }, 2000);
    setTimeout(() => {
      this.changeLoadingContent(1)
    }, 5000);
    setTimeout(() => {
      this.changeLoadingContent(2)
    }, 8000);
    setTimeout(() => {
      this.changeLoadingContent(3)
    }, 11000);
    setTimeout(() => {
      this.changeLoadingContent(4)
    }, 14000);
  }


  changeLoadingContent(i) {
    switch (i) {
      case 0:
        this.loading.setContent("Ürünler Hazırlanıyor...");
        break;
      case 1:
        this.loading.setContent("Müşteriler Değerlendiriliyor...");
        document.getElementById("btnnext").click();
        break;
      case 2:
        this.loading.setContent("Mağazalar Değerlendiriliyor...");
        document.getElementById("btnnext").click();
        break;
      case 3:
        this.loading.setContent("Yaklaşan Perakende Günleri İnceleniyor...");
        document.getElementById("btnnext").click();
        break;
      case 4:
        this.loading.setContent("Önerilen Kampanyalar Listesi Hazırlanıyor...");
        document.getElementById("btnnext").click();
        setTimeout(() => {
          this.dismissLoading();
          this.presentAlert();
          setTimeout(() => {
            this.alert.dismiss();
          }, 1500)
        }, 1000)
    }
  }

  goToCampaignsPage(){
    this.navCtrl.push(CampaignSuggestsPage);
  }

  presentAlert() {

    this.alert = this.alertCtrl.create({
      title: "<i class=\"icon icon-md ion-ios-checkmark-circle-outline fontsize100\"></i>",
      subTitle: 'Kampanyalar Oluşturuldu.'
    });
    this.alert.present();

  }
  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
