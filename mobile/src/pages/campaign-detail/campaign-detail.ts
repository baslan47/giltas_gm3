import { Component } from '@angular/core';
import {
  NavController,
  NavParams,
  AlertController,
  ViewController,
  Loading,
  ModalController,
  ActionSheetController
} from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import { TranslateService } from '@ngx-translate/core';
import { ModalCampaignProductsPage } from '../modal-campaign-products/modal-campaign-products';
import { ModalTimePeriodPage } from '../modal-time-period/modal-time-period';
import { ModalAmountRulePage } from '../modal-amount-rule/modal-amount-rule';
import { ModalCampaignGroupPage } from '../modal-campaign-group/modal-campaign-group';
import { ModalProductListPage } from '../modal-product-list/modal-product-list';
import { ModalCustomerListPage } from '../modal-customer-list/modal-customer-list';

@Component({
  selector: 'page-campaign-detail',
  templateUrl: 'campaign-detail.html',
})
export class CampaignDetailPage {

  campaignData: any[] = [];

  discountOffers: any[] = [];
  discountOfferType: any[] = [];
  discountOfferMethod: any[] = [];
  discountProcess: any[] = [];
  discountVoucherType: any[] = [];
  discountPointType: any[] = [];
  discountPointBase: any[] = [];
  discountProductPointType: any[] = [];
  discountOfferApply: any[] = [];
  discountOfferStage: any[] = [];
  discountTimePeriod: any[] = [];
  discountAmountRule: any[] = [];

 
  selectedDiscountOffers:any;
  selectedDiscountOfferType:any;
  selectedDiscountOfferMethod:any;
  selectedDiscountProcess:any;
  selectedDiscountVoucherType:any;
  selectedDiscountPointType:any;
  selectedDiscountPointBase:any;
  selectedDiscountProductPointType:any;
  selectedDiscountOfferApply:any;
  selectedDiscountOfferStage:any;
  selectedDiscountTimePeriod: any;
  selectedDiscountAmountRule:any;

  startDate: any; //= "2015-11-11";
  endDate: any; //= "12-12-2012";

  insertItems = {
    "ProcName": "", "Parameters": [
      { "Name": "Index", "Value": 0 },
      { "Name": "ItemCode", "Value": "" }, { "Name": "ItemDescription", "Value": "" },
      { "Name": "ColorCode", "Value": "" }, { "Name": "ItemDim1Code", "Value": "" },
      { "Name": "InventoryQty1", "Value": "" }, { "Name": "ItemFailTypeCode", "Value": "" },
      { "Name": "ItemFailTypeDesc", "Value": "" }]
  };

  V3CampaignData = {"ProcName": ""}

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public serverService: ServerService, public alertCtrl: AlertController,
    public translateService: TranslateService, public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController
  ) {
    console.log(this.navParams.data.campaign);
    this.campaignData = this.navParams.data.campaign;
    this.selectedDiscountOfferApply = {Code: this.navParams.data.campaign.DiscountOfferApplyCode, Description: this.navParams.data.campaign.DiscountOfferApplyDescription};
    
    this.selectedDiscountOffers = {Code: this.navParams.data.campaign.DiscountOfferCode, Description: this.navParams.data.campaign.DiscountOfferDescription};
    this.selectedDiscountOfferType = {Code: this.navParams.data.campaign.DiscountOfferTypeCode, Description: this.navParams.data.campaign.DiscountOfferTypeDescription};
    this.selectedDiscountOfferMethod = {Code: this.navParams.data.campaign.DiscountOfferMethodCode, Description: this.navParams.data.campaign.DiscountOfferMethodDescription};
    this.selectedDiscountProcess = {Code: this.navParams.data.campaign.ProcessCode, Description: this.navParams.data.campaign.ProcessDescription};
    this.selectedDiscountVoucherType = {Code: this.navParams.data.campaign.DiscountVoucherTypeCode, Description: this.navParams.data.campaign.DiscountVoucherTypeDescription};
    this.selectedDiscountPointType = {Code: this.navParams.data.campaign.DiscountPointTypeCode, Description: this.navParams.data.campaign.DiscountPointTypeDescription};
    this.selectedDiscountPointBase = {Code: this.navParams.data.campaign.PointBaseCode, Description: this.navParams.data.campaign.PointBaseDescription};
    this.selectedDiscountProductPointType = {Code: this.navParams.data.campaign.ProductPointTypeCode, Description: this.navParams.data.campaign.ProductPointTypeDescription};
    this.selectedDiscountOfferStage = {Code: this.navParams.data.campaign.DiscountOfferStageCode, Description: this.navParams.data.campaign.DiscountOfferStageDescription};//----state
    this.selectedDiscountTimePeriod = {Code: this.navParams.data.campaign.TimePeriodCode, Description: this.navParams.data.campaign.TimePeriodDescription};
    this.selectedDiscountAmountRule = {Code: this.navParams.data.campaign.AmountRuleCode, Description: this.navParams.data.campaign.AmountRuleDescription};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CampaignDetailPage');
  }

  ngOnInit() {
    this.endDate = this.serverService.convertISODate(this.navParams.data.campaign.EndDate);
    this.startDate = this.serverService.convertISODate(this.navParams.data.campaign.StartDate);
    console.log(this.endDate,this.startDate);

    this.getDiscountOfferType();
    this.getDiscountOffers();
    this.getDiscountOfferMethod();
    this.getDiscountProcess();
    this.getDiscountVoucherType();
    this.getDiscountPointType();
    this.getDiscountPointBase();
    this.getDiscountProductPointType();
    this.getDiscountOfferApply();
    this.getDiscountOfferStage();
    this.getDiscountTimePeriod();
    this.getDiscountAmountRule();
  }


  goToItemsModal() {
    let modal;
    modal = this.modalCtrl.create(ModalCampaignProductsPage);
    modal.present();
    modal.onDidDismiss(data => {
      if(data.result == false) return;
      console.log(data.newItemArray)
      this.insertItems.ProcName = 'G3AI_InsertItems';
      for (let i = 0; i < data.newItemArray.length; i++) {
        this.insertItems.Parameters[0].Value = i;
        this.insertItems.Parameters[1].Value = data.newItemArray[i].ItemCode;
        this.insertItems.Parameters[2].Value = data.newItemArray[i].ItemDescription? data.newItemArray[i].ItemDescription: '';
        this.insertItems.Parameters[3].Value = data.newItemArray[i].ColorCode? data.newItemArray[i].ColorCode: '';
        this.insertItems.Parameters[4].Value = data.newItemArray[i].ItemDim1Code? data.newItemArray[i].ItemDim1Code: '';
        this.insertItems.Parameters[5].Value = data.newItemArray[i].InventoryQty1? data.newItemArray[i].InventoryQty1: '';
        this.insertItems.Parameters[6].Value = data.newItemArray[i].ItemFailTypeCode? data.newItemArray[i].ItemFailTypeCode: '';
        this.insertItems.Parameters[7].Value = data.newItemArray[i].ItemFailTypeDesc? data.newItemArray[i].ItemFailTypeDesc: '';
        this.serverService.getAny(this.insertItems)
      }


    })
  }
  getDiscountOfferType(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountOfferType";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountOfferType',res);
      this.discountOfferType = res;
    })
  }

  getDiscountOffers(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountOffers";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountOffers',res)
      this.discountOffers = res;
    })
  }

  getDiscountOfferMethod(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountOfferMethod";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountOfferMethod',res)
      this.discountOfferMethod = res;
    })
  }

  getDiscountProcess(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountProcess";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountProcess',res);
      this.discountProcess = res;
    })
  }

  getDiscountVoucherType(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountVoucherType";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountVoucherType',res);
      this.discountVoucherType = res;
    })
  }

  getDiscountPointType(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountPointType";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountPointType',res);
      this.discountPointType = res;
    })
  }

  getDiscountPointBase(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountPointBase";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountPointBase',res);
      this.discountPointBase = res;
    })
  }

  getDiscountProductPointType(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountProductPointType";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountProductPointType',res);
      this.discountProductPointType = res;
    })
  }

  getDiscountOfferApply(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountOfferApply";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountOfferApply',res);
      this.discountOfferApply = res;
      
    })
  }

  getDiscountOfferStage(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountOfferStage";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountOfferStage',res);
      this.discountOfferStage = res;
    })
  }

  getDiscountTimePeriod(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountTimePeriod";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountTimePeriod',res);
      this.discountTimePeriod = res;
    })
  }

  getDiscountAmountRule(){
    this.V3CampaignData.ProcName = "G3AI_GetDiscountAmountRule";
    this.serverService.getAny(this.V3CampaignData).then(res => {
      console.log('G3AI_GetDiscountAmountRule',res);
      this.discountAmountRule = res;
    })
  }

  plusButtonActionSheet() {
    this.translateService.get(['Zaman Periyodu Ekle', 'Tutar Kuralı Ekle', 'Kampanya Grubu Ekle', 'Ürün Listesi Ekle', 'Müşteri Listesi Ekle','Kampanya Modulü Ekle']).subscribe((value: string[]) => {
      let actionSheetButtons = [];

      actionSheetButtons.push({
        text: value['Zaman Periyodu Ekle'],
        handler: () => {
          this.timePeriodModal();
        }
      });

      actionSheetButtons.push({
        text: value['Tutar Kuralı Ekle'],
        handler: () => {
          this.amountRuleModal();
        }
      });

      actionSheetButtons.push({
        text: value['Kampanya Grubu Ekle'],
        handler: () => {
          this.campaignGroupModal();
        }
      });

      actionSheetButtons.push({
        text: value['Ürün Listesi Ekle'],
        handler: () => {
          this.productListModal();
        }
      });

      actionSheetButtons.push({
        text: value['Müşteri Listesi Ekle'],
        handler: () => {
          this.customerListModal();
        }
      });

      let actionSheet = this.actionSheetCtrl.create({
        title: value['Kampanya Modulü Ekle'],
        buttons: actionSheetButtons
      });
      actionSheet.present();
    });
  }

  timePeriodModal() {
    let modal;
    modal = this.modalCtrl.create(ModalTimePeriodPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data.result) {
       this.getDiscountTimePeriod();
       this.presentAlert('Bilgi', 'Zaman Periyodu Başarıyla Eklendi');
      }
    });
  }

  amountRuleModal() {
    let modal;
    modal = this.modalCtrl.create(ModalAmountRulePage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data.result) {
      
      }
    });
  }

  campaignGroupModal() {
    let modal;
    modal = this.modalCtrl.create(ModalCampaignGroupPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data.result) {
      
      }
    });
  }

  productListModal() {
    let modal;
    modal = this.modalCtrl.create(ModalProductListPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data.result) {
       
      }
    });
  }

  customerListModal() {
    let modal;
    modal = this.modalCtrl.create(ModalCustomerListPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data.result) {
      
      }
    });
  }

  presentAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

}
