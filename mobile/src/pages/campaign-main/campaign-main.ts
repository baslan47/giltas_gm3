import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import { CampaignsPage } from '../campaigns/campaigns';
import { CampaignSuggestsPage } from '../campaign-suggests/campaign-suggests';
import { CampaignCreatePage } from '../campaign-create/campaign-create';

@Component({
  selector: 'page-campaign-main',
  templateUrl: 'campaign-main.html',
})
export class CampaignMainPage {

  loading: Loading;
  itemArray: any[] = [];
  lowSaleItemLength: any;
  notSaleItemLength: any;
  overStockLength: any;

  lowSaleItemRate: any;
  notSaleItemRate: any;
  overStockItemRate: any;
  itemRateArray: any[] = [];
  customerArray: any[] = [];

  campaignData = { "ProcName": "" };
  lowSaleItems = { "ProcName": "", "Parameters": [{ "Name": "Day", "Value": "" }] };
  notSaleItems = { "ProcName": "", "Parameters": [{ "Name": "Day", "Value": "" }] };
  seasonItems = { "ProcName": "", "Parameters": [{ "Name": "AttributeType", "Value": "" }, { "Name": "SeasonCode", "Value": "" }] };
  manyIncomingCustomers = { "ProcName": "G3AI_GetManyIncomingCustomers", "Parameters": [{ "Name": "Day", "Value": "" }] };
  neverComingCustomers = { "ProcName": "G3AI_GetNeverComingCustomers", "Parameters": [{ "Name": "Day", "Value": "" }] };
  insertCustomersData = { "ProcName": "G3AI_InsertCustomers", "Parameters": [{ "Name": "Index", "Value": 0},{ "Name": "Name", "Value": "" },{ "Name": "Surname", "Value": "" }] };
  insertProductRate = { "ProcName": "G3AI_SetItemFailTypeRate", "Parameters": 
  [{ "Name": "Index", "Value": 0 }, { "Name": "ItemFailTypeID", "Value": 0 },{ "Name": "Rate", "Value": 0 }]};

  insertItems = {
    "ProcName": "", "Parameters": [
      { "Name": "Index", "Value": 0 },
      { "Name": "ItemCode", "Value": "" }, { "Name": "ItemDescription", "Value": "" },
      { "Name": "ColorCode", "Value": "" }, { "Name": "ItemDim1Code", "Value": "" },
      { "Name": "InventoryQty1", "Value": "" }, { "Name": "ItemFailTypeID", "Value": "" }]
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loadingCtrl: LoadingController, public serverService: ServerService) {

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CampaignMainPage');
    
  }

  ngOnInit() {
    this.presentLoading();
    this.getManyIncomingCustomers();
    this.getLowSaleItems();
    this.getNotSaleItems();
    this.getSeasonItems();
    this.getSortedInventoryList();
    this.getCampaignListFromV3();
  }

  getManyIncomingCustomers(){
    this.manyIncomingCustomers.Parameters[0].Value = "10000";
    this.serverService.getAny(this.manyIncomingCustomers).then(res => {
      console.log('G3AI_GetManyIncomingCustomers', res);
      for(let i = 0;i<res.length;i++){
      this.customerArray.push(res[i]);
      if(i== res.length-1) this.getNeverComingCustomers();
      }
      console.log(this.customerArray)
    })
  }

  getNeverComingCustomers(){
    this.neverComingCustomers.Parameters[0].Value = "10000";
    this.serverService.getAny(this.neverComingCustomers).then(res => {
      console.log('G3AI_GetNeverComingCustomers', res);
      for(let i = 0;i<res.length;i++){
        this.customerArray.push(res[i]);
        if(i == res.length-1) this.insertCustomers();
        }
    })
  }
  insertCustomers(){
    for(let i = 0; i<this.customerArray.length; i++){
      this.insertCustomersData.Parameters[0].Value = i;
      this.insertCustomersData.Parameters[1].Value = this.customerArray[i].FirstName;
      this.insertCustomersData.Parameters[2].Value = this.customerArray[i].LastName;
      this.serverService.getAny(this.insertCustomersData);
    }
  }


  getLowSaleItems() {
    this.lowSaleItems.ProcName = 'G3AI_GetLowSaleItems';
    this.lowSaleItems.Parameters[0].Value = '1000';//1000 günlük veri
    this.serverService.getAny(this.lowSaleItems).then(res => {
      console.log('G3AI_GetLowSaleItems', res);
      this.lowSaleItemLength = res.length;
      if (res.length >= 30) {
        for (let i = 0; i < 30; i++) {
          this.itemArray.push(res[i]);
        }
      } else {
        for (let i = 0; i < res.length; i++) {
          this.itemArray.push(res[i]);
        }
      }
      // console.log(this.itemArray);
    })
  }

  getNotSaleItems() {
    this.notSaleItems.ProcName = 'G3AI_GetNotSaleItems';
    this.notSaleItems.Parameters[0].Value = '30';
    this.serverService.getAny(this.notSaleItems).then(res => {
      console.log('G3AI_GetNotSaleItems', res);
      this.notSaleItemLength = res.length;
      if (res.length >= 30) {
        for (let i = 0; i < 30; i++) {
          this.itemArray.push(res[i]);
        }
      } else {
        for (let i = 0; i < res.length; i++) {
          this.itemArray.push(res[i]);
        }
      }
      // console.log(this.itemArray);
      this.insertItemsFromItemArray();
      this.dismissLoading();
      
    })
  }

  getSeasonItems() {
    this.seasonItems.ProcName = 'G3AI_GetSeasonList';
    this.seasonItems.Parameters[0].Value = '1'; // özellik kodu
    this.seasonItems.Parameters[1].Value = '01'; // YAZ - KIŞ
    this.serverService.getAny(this.seasonItems).then(res => {
      console.log('G3AI_GetSeasonList', res);

      if (res.length >= 30) {
        for (let i = 0; i < 30; i++) {
          this.itemArray.push(res[i]);
        }
      } else {
        for (let i = 0; i < res.length; i++) {
          this.itemArray.push(res[i]);
        }
      }
      //console.log(this.itemArray);
    })
  }

  getSortedInventoryList() {
    this.campaignData.ProcName = 'G3AI_SortInventory';
    this.serverService.getAny(this.campaignData).then(res => {
      console.log('G3AI_SortInventory', res);
      this.overStockLength = res.length;
      if (res.length >= 30) {
        for (let i = 0; i < 30; i++) {
          this.itemArray.push(res[i]);
        }
      } else {
        for (let i = 0; i < res.length; i++) {
          this.itemArray.push(res[i]);
        }
      }
      console.log(this.itemArray);
    })
  }
  
  getCampaignListFromV3() {
    this.campaignData.ProcName = 'G3AI_GetV3CampaignList';
    this.serverService.getAny(this.campaignData).then(res => {
      console.log('G3AI_GetV3CampaignList', res);
     
    })
  }

  insertItemsFromItemArray() {
    this.insertItems.ProcName = 'G3AI_InsertItems';
    for (let i = 0; i < this.itemArray.length; i++) {
      this.insertItems.Parameters[0].Value = i;
      this.insertItems.Parameters[1].Value = this.itemArray[i].ItemCode;
      this.insertItems.Parameters[2].Value = this.itemArray[i].ItemDescription? this.itemArray[i].ItemDescription: '';
      this.insertItems.Parameters[3].Value = this.itemArray[i].ColorCode? this.itemArray[i].ColorCode: '';
      this.insertItems.Parameters[4].Value = this.itemArray[i].ItemDim1Code? this.itemArray[i].ItemDim1Code: '';
      this.insertItems.Parameters[5].Value = this.itemArray[i].InventoryQty1? this.itemArray[i].InventoryQty1: '';
      this.insertItems.Parameters[6].Value = this.itemArray[i].ItemFailTypeID? this.itemArray[i].ItemFailTypeID: '';
      this.serverService.getAny(this.insertItems)
    }
  }

  calculateProductRate(){
    let totalItems = this.lowSaleItemLength + this.notSaleItemLength + this.overStockLength;
    this.notSaleItemRate = this.notSaleItemLength / totalItems;
    this.itemRateArray.push(this.notSaleItemRate)
    this.lowSaleItemRate = this.lowSaleItemLength / totalItems;
    this.itemRateArray.push(this.lowSaleItemRate)
    this.overStockItemRate = this.overStockLength / totalItems;
    this.itemRateArray.push(this.overStockItemRate)
  }

  setProductRateAndInsertTable(){
    this.calculateProductRate();
    console.log(this.lowSaleItemRate)
    for(let i =0;i<this.itemRateArray.length;i++){
    this.insertProductRate.Parameters[0].Value = i;
    this.insertProductRate.Parameters[1].Value = i+1;
    this.insertProductRate.Parameters[2].Value = this.itemRateArray[i];
    this.serverService.getAny(this.insertProductRate);
    }
  }


  goToCampaignCreatePage() {
    this.navCtrl.push(CampaignCreatePage)
  }

  goToActiveCampaignsPage() {
    this.navCtrl.push(CampaignsPage)
  }

  public presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Lütfen bekleyiniz...",
    });
    this.loading.present();
  }

  public dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

}
