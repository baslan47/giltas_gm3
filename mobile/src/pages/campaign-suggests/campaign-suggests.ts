import {Component} from '@angular/core';
import {AlertController, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ServerService} from "../../classes/server.service";
import {TranslateService} from "@ngx-translate/core";
import { CampaignDetailPage } from '../campaign-detail/campaign-detail';

@Component({
  selector: 'page-campaign-suggests',
  templateUrl: 'campaign-suggests.html',
})
export class CampaignSuggestsPage {
  campaigns: any[] = [];
  loading: Loading;
  campaignsData = {
    "ProcName": "G3_GetDiscountList",
    "Parameters": [{"Name": "Language", "Value": "tr"}]
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
              public serverService: ServerService, public alertCtrl: AlertController,
              public translateService: TranslateService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CampaignSuggestsPage');
  }

  ngOnInit() {
    this.presentLoading();
    this.serverService.getAny(this.campaignsData)
      .then(res => {
        this.dismissLoading();
        this.campaigns = res;
        console.log(res);
      })
      .catch(this.handleError);
  }

  goTo(campaign) {
    this.navCtrl.push(CampaignDetailPage, {campaign: campaign});
  }

  handleError(error: any) {
    console.error('An error occurred', error);
    this.dismissLoading();
    /*this.translateService.get('ALERT_ERROR_TITLE_TEXT').subscribe((value: string) => {
      this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], error.ExceptionMessage);
    });*/
  }

  presentAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        spinner: null,
      //duration: 50,
      content: value,
      cssClass: 'custom-class custom-loading'
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
 

}
