import {Component, OnInit} from '@angular/core';
import {AlertController, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ServerService} from "../../classes/server.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'page-campaigns',
  templateUrl: 'campaigns.html',
})
export class CampaignsPage implements OnInit {
  campaigns: any[] = [];
  loading: Loading;
  campaignsData = {
    "ProcName": "G3_GetDiscountList",
    "Parameters": [{"Name": "Language", "Value": "TR"}]
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
              public serverService: ServerService, public alertCtrl: AlertController,
              public translateService: TranslateService) {
  }

  ngOnInit() {
    this.presentLoading();
    this.serverService.getAny(this.campaignsData)
      .then(res => {
        this.dismissLoading();
        this.campaigns = res;
        console.log(res);
      })
      .catch(this.handleError);
  }

  handleError(error: any) {
    console.error('An error occurred', error);
    this.dismissLoading();
    /*this.translateService.get('ALERT_ERROR_TITLE_TEXT').subscribe((value: string) => {
      this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], error.ExceptionMessage);
    });*/
  }

  presentAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
