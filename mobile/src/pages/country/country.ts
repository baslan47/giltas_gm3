import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

@Component({
    selector: 'page-country',
    templateUrl: 'country.html',
})
export class CountryPage {

    countryList = [];
    countryListTmp = [];
    public myInput: string = '';

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,) {
        this.countryList = this.navParams.data.countries;
        this.countryListTmp = this.countryList;
    }

    onInput(event: any) {
        this.countryListTmp = this.countryList.filter( item => {return item.Description.toLowerCase().includes(this.myInput);});
        console.log(this.countryListTmp);
        return this.countryListTmp;
    }

    onCancel(){
        this.countryListTmp = this.countryList;
    }

    back() {
        this.viewCtrl.dismiss();
    }

    selectCountry(item: any){
        this.viewCtrl.dismiss(item);
    }
}
