import { Component, OnInit } from '@angular/core';
import {
    NavController,
    NavParams,
    LoadingController,
    AlertController,
    ViewController,
    Platform,
    Loading,
    ToastController, ModalController
} from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { Customer, CurrAccPersonalInfo, CurrAccDefault } from "../../classes/customer";
import { Exception, Data } from "../../classes/exception";
import * as $ from 'jquery';
import { TranslateService } from "@ngx-translate/core";
import { InstallmentsDetailPage } from "../installments-detail/installments-detail";
import { InstallmentsPage } from '../installments/installments';
import { CustomerDebit } from "../../models/models";
import { CountryPage } from "../country/country";
import { ExtraReportPage } from "../extra-report/extra-report";
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ModalGuarantorPage } from '../modal-guarantor/modal-guarantor';
import { ModalAddaddressPage } from '../modal-addaddress/modal-addaddress';
import { ModalAttributeType17Page } from '../modal-attribute-type17/modal-attribute-type17';


// interface IData {
//     [key: string]: any;
// }

@Component({
    selector: 'page-customer',
    templateUrl: 'customer.html',
})

export class CustomerPage implements OnInit {
    customer: Customer;
    customers: any[] = [];
    personelInfo: CurrAccPersonalInfo;
    currAccDefault: CurrAccDefault;
    customerModel: any = {
        "ModelType": 3, // TODO Set
        "CurrAccCode": "",
        "FirstName": "",
        "LastName": "",
        "CurrAccDescription": "",
        "OfficeCode": "M", // TODO Set
        "RetailSalePriceGroupCode": "",// TODO Set
        "WholeSalePriceGroupCode": "",
        "IdentityNum": "",
        "TaxOfficeCode": "",
        "TaxNumber": "",
        "CreditLimit": 7000, // TODO Set
        "CurrencyCode": "TRY",
        "PostalAddresses": [],
        "Attributes": [],
        "Communications": [],
        "CurrAccPersonalInfo": {},
        "CurrAccDefault": {}
    };

    world_check: any;
    installments: any;
    installments_summary = [];
    curPlans: any[] = new Array();
    extras_summary = [];
    loading: Loading;
    checkdatabase: number = 0;
    customerDebit: CustomerDebit;
    buttonClicked: boolean = false;
    overduePayment = [];
    attributes: any;
    guarantors: any;
    showBack: boolean = false;
    showImage: boolean;
    data: any = [];
    editMode: boolean = false;
    newMode: boolean = false;
    countryList = [];
    place = "TRY";
    imageURL: any;
    imageNameReturn: boolean;
    public checkimage: boolean = false;
    base64Image: string;
    datestring: string;
    customerInformation: any;
    customerWarning: any;
    stateList: any[] = [];
    cityList: any[] = [];
    districtList: any[] = [];
    attributeType17List: any[] = [];
    customerExtra: any;
    taxOffices: any[] = [];
    taxOfficesTemp: any[] = [];
    currencyTemp: any[] = [];
    imageName: string;
    tempTax: string;
    tempTaxCheck: boolean = false;
    currencyCheck: boolean = false;

    CountryCode: 'TR';

    //TC Kimlik Numarası 
    total1: any;
    total2: any;
    number10: any;
    number11: any;

    //customer Attributes
    AttributeCode13: any;
    AttributeCode14: any;
    AttributeCode17: any;
    AttDesc17: any;

    index: number = 0; //adress array count


    customersData = {
        "ProcName": "", "Parameters": [{ "Name": "FirstName", "Value": "" }, { "Name": "LastName", "Value": "" },
        { "Name": "IdentityNumber", "Value": "" }, { "Name": "TaxNumber", "Value": "" }, { "Name": "PhoneNumber", "Value": "" },
        { "Name": "Code", "Value": "" }, { "Name": "FirstLastName", "Value": "" }]
    };

    g3_LogData = {
        "ProcName": "G3_AddLog", "Parameters": [{ "Name": "UserID", "Value": "" }, { "Name": "Action", "Value": "" },
        { "Name": "Request", "Value": "" }, { "Name": "Response", "Value": "" }]
    };

    userData = { "ProcName": "", "Parameters": [{ "Name": "CurrAccCode", "Value": "" }, { "Name": "LangCode", "Value": "TR" }] };
    overdueData = { "ProcName": "", "Parameters": [{ "Name": "CurrAccCode", "Value": "" }, { "Name": "OfficeCode", "Value": "TR" }] };
    guarantorsData = { "ProcName": "G3_GetGuarantors", "Parameters": [{ "Name": "CurrAccCode", "Value": "" }] };
    addrData = { "ProcName": "", "Parameters": [{ "Name": "Parent", "Value": "" }, { "Name": "Language", "Value": "TR" }] };
    taxOfficeData = { "ProcName": "G3_GetTaxOffices", "Parameters": [{ "Name": "Language", "Value": "TR" }] };
    taxOfficeDataDesc = { "ProcName": "G3_GetCustomerTaxOfficeDescription", "Parameters": [{ "Name": "TaxCode", "Value": "" }, { "Name": "Language", "Value": "TR" }] };
    updateAddressData = { "ProcName": "G3_SetCustomerPostalAddresses", "Parameters": [{ "Name": "CurrAccCode", "Value": "" }] }
    userinformation = { "ProcName": "G3_GetCustomerInformation", "Parameters": [{ "Name": "CurrAccCode", "Value": "" }] }
    userwarning = { "ProcName": "G3_GetCustomerWarning", "Parameters": [{ "Name": "CurrAccCode", "Value": "" }] }
    

    identityNumCheckArray: any[] = [];
    genderCode: any[] = [
        { "Code": "1", "Value": "Erkek" },
        { "Code": "2", "Value": "Kadın" },
        { "Code": "3", "Value": "Bilinmiyor" }
    ];
    addressDescriptions: any[] = [
        { "Code": "1", "Value": "Ev Adresi" },
        { "Code": "2", "Value": "İş Adresi" },
        { "Code": "3", "Value": "Merkez Ofis" },
        { "Code": "4", "Value": "Eski Ev Adresi" }
    ];
    customerAttributeDesc13: any[] = [
        { "Code": "0-1", "Value": "0-1" },
        { "Code": "1-3", "Value": "1-3" },
        { "Code": "3-5", "Value": "3-5" },
        { "Code": "5-7", "Value": "5-7" },
        { "Code": "7+", "Value": "7 ve üzeri" }
    ];
    customerAttributeDesc14: any[] = [
        { "Code": "1", "Value": "Kendi Evi" },
        { "Code": "2", "Value": "Kiracı" },
        { "Code": "3", "Value": "Kapıcı" },
        { "Code": "4", "Value": "Aile ile Oturuyor" },
        { "Code": "5", "Value": "Lojman" },
        { "Code": "6", "Value": "Akraba Evi" },
        { "Code": "x", "Value": "x" }
    ];
    drivingLicenceType: any[] = [
        { "Code": "A", "Value": "A" },
        { "Code": "B", "Value": "B" },
        { "Code": "C", "Value": "C" },
        { "Code": "D", "Value": "D" },
        { "Code": "E", "Value": "E" },
        { "Code": "F", "Value": "F" }
    ];
    monthlyIncome: any[] = [
        { "Code": "0", "Value": "Belirtilmemiş" },
        { "Code": "2000", "Value": "2000 ₺" },
        { "Code": "2500", "Value": "2500 ₺" },
        { "Code": "3000", "Value": "3000 ₺" },
        { "Code": "3500", "Value": "3500 ₺" },
        { "Code": "4000", "Value": "4000 ₺" },
        { "Code": "5000", "Value": "5000 ₺" },
        { "Code": "10000", "Value": "5000 ₺ ++" }
    ];
    registeredCityCode: any[] = [
        { "Code": "TR.06", "Value": "Ankara" },
        { "Code": "TR.34", "Value": "İstanbul" },
        { "Code": "TR.35", "Value": "İzmir" }
    ]

    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public storage: Storage,
        public loadingCtrl: LoadingController, public serverService: ServerService, public alertCtrl: AlertController,
        public toastCtrl: ToastController, public translateService: TranslateService, public modalCtrl: ModalController,
        private camera: Camera, public platform: Platform) {
        //JS if (this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2') {
        if (this.serverService.Settings.Integrator.DatabaseName == 'SANALV3') {
            this.checkdatabase = 1;
            this.customerModel.CurrencyCode = "";
        }
        this.curPlans = [
            {
                "name": "TRY",
                "val": "TRY"
            },
            {
                "name": "EUR",
                "val": "EUR"
            },
            {
                "name": "USD",
                "val": "USD"
            },
        ];
        console.log(this.curPlans);
        this.customer = this.navParams.data.customer;
        console.log(this.customer);
        //console.log(this.serverService.Settings.G3Settings.ImageUrl);
        this.showBack = this.navParams.data.showBack;
        this.data = this.navParams.data.res;
        this.showImage = this.navParams.data.showImage;
        console.log(this.showImage);

        if (this.customer == null) {
            this.newMode = true;
            this.editMode = true;
            this.customer = new Customer();
            this.currencyCheck = false;
        } else {
            this.currencyCheck = true;
            this.customerModel.CurrAccCode = this.customer.CurrAccCode;
            this.customerModel.FirstName = this.customer.CustomerName ? this.customer.CustomerName : this.customer.FirstName;
            this.customerModel.LastName = this.customer.LastName;
            this.customerModel.IdentityNum = this.customer.IdentityNum;
            this.customerModel.CurrencyCode = this.customer.CurrencyCode;
            //this.customerModel.TaxOfficeCode = this.customer.TaxOfficeCode;
            this.customerModel.TaxNumber = this.customer.TaxNumber;
        }
        if (this.showImage) {
            this.imageURL = this.serverService.Settings.G3Settings.ImageUrl + '/Home/GetThumbCustomer?curracccode=' + this.customer.CurrAccCode;
        } else {
            this.imageURL = "";
            console.log("Resim Boş");
        }
    }

    optionsFn() {
        console.log(this.customerModel.CurrencyCode);
        this.customerModel.CurrencyCode = this.customerModel.CurrencyCode;
    }

    ngOnInit() {
        this.presentLoading();
        if (this.data) {
            this.customerModel.IdentityNum = this.data[0];
            this.customerModel.LastName = this.data[1];
            this.customerModel.FirstName = this.data[2];
            this.personelInfo = new CurrAccPersonalInfo();
            this.personelInfo.FatherName = this.data[3];
            this.personelInfo.MotherName = this.data[4];
            this.personelInfo.BirthPlace = this.data[5];
            this.datestring = this.data[6];
            this.datestring = this.datestring.substring(10, 6) + "-" + this.datestring.substring(5, 3) + "-" + this.datestring.substring(0, 2);
            console.log(this.datestring);
            this.personelInfo.BirthDate = new Date(this.datestring).toISOString();
            this.personelInfo.RegisteredTown = this.data[7];
            this.personelInfo.RegisteredFileNum = this.data[8];
            this.personelInfo.RegisteredFamilyNum = this.data[9];
            this.personelInfo.RegisteredNum = this.data[10];            
            //this.personelInfo.BirthDate = new Date("14.04.1984").toISOString();
        } else {
            if (this.checkdatabase == 1) {
                this.getTaxOffices();
            }
            this.getCountries();
            this.customersData.ProcName = "G3_CustomerListRetailNEW";
            this.serverService.getCustomers(this.customersData)
                .then(res => {
                    //console.log(res)
                    this.customers = res;
                })
                .catch(this.handleError);
            if (!this.newMode) {
                this.userData.ProcName = "G3_GetCustomerPostalAddress";
                this.userData.Parameters[0].Value = this.customerModel.CurrAccCode;
                this.serverService.getAny(this.userData)
                    .then(res => {
                        this.customerModel.PostalAddresses = res;
                        console.log('postalAdress', res)
                        this.getAddressData();
                    })
                    .catch(this.handleError);

                this.userData.ProcName = "G3_GetCustomerCommunications";
                this.serverService.getAny(this.userData)
                    .then(res => {
                        console.log('communications', res)

                        this.customerModel.Communications = res;
                    })
                    .catch(this.handleError);

                if (this.checkdatabase == 1) {
                    this.getTaxOfficesDesc(this.customer.TaxOfficeCode);
                }

                this.getGuarantors();
                this.getPersonelInfo();
                this.getCustomerAttribute();
            }
            if (this.serverService.Settings.V3Settings.SalesType) {
                if (this.serverService.Settings.V3Settings.SalesType == 2 || this.serverService.Settings.V3Settings.SalesType == 3) {
                    this.userData.ProcName = "G3_GetCustomerDebit";
                    this.serverService.getAny(this.userData)
                        .then(res => {
                            console.log('debit', res);
                            this.customerDebit = res;
                        })
                        .catch(this.handleError);
                    console.log(this.serverService.Settings.V3Settings.OfficeCode)
                    this.overdueData.ProcName = "G3_GetCustomerOverduePayment";
                    this.overdueData.Parameters[0].Value = this.customer.CurrAccCode;
                    this.overdueData.Parameters[1].Value = this.serverService.Settings.V3Settings.OfficeCode;
                    this.serverService.getAny(this.overdueData)
                        .then(res => {
                            console.log('overduePayment', res);
                            this.overduePayment = res;
                        })
                }

                if (this.serverService.Settings.V3Settings.SalesType == 3) {
                    this.userData.ProcName = "G3_GetCustomerInstallments";
                    this.serverService.getAny(this.userData)
                        .then(res => {
                            console.log('installments');
                            this.installments = res;
                            let debit: number = 0;
                            let desc: string = '';
                            let date: string = '';
                            for (let installement of this.installments) {
                                if (date != installement.MonthYear) {
                                    if (debit > 0) {
                                        let installement_summary = {
                                            DueDate: date,
                                            ApplicationDescription: desc,
                                            InstallmentAmount: debit.toFixed(2)
                                        };
                                        this.installments_summary.push(installement_summary);
                                        debit = parseFloat(installement.InstallmentAmount);
                                        desc = installement.ApplicationDescription;
                                        date = installement.MonthYear;
                                    }
                                    else {
                                        date = installement.MonthYear;
                                        desc = installement.ApplicationDescription;
                                        debit += parseFloat(installement.InstallmentAmount);
                                    }
                                }
                                else {
                                    debit += parseFloat(installement.InstallmentAmount);
                                }
                            }
                            console.log(this.installments);
                            console.log(this.installments_summary);
                        })
                        .catch(this.handleError);
                }
            }
        }
        this.dismissLoading();
    }

    getAttributeType17() {
        let addData = { "ProcName": "G3_GetAttributeType17", "Parameters": [{ "Name": "Parent", "Value": "" }, { "Name": "Code", "Value": "" }] }
        addData.Parameters[0].Value = "";
        if (this.AttributeCode17) {
            addData.Parameters[1].Value = this.AttributeCode17
        } else {
            addData.Parameters[1].Value = ""
        }
        this.serverService.getAny(addData)
            .then(res => {
                this.attributeType17List = res;
                if (res[0] && res[0].AttributeDescription)
                    this.AttDesc17 = res[0].AttributeDescription
                console.log('getAttributeType17', this.attributeType17List);
            })
    }
   

    clickJobStuation() {
        let modal;
        modal = this.modalCtrl.create(ModalAttributeType17Page);
        modal.present();
        modal.onDidDismiss(data => {
            console.log(data);
            this.AttributeCode17 = data.txtAttCode17;
            if (data.txtAttDesc17 != "")
                this.AttDesc17 = data.txtAttDesc17;
            this.getAttributeType17();
            this.addCustomerAttribute17(this.customerModel.CurrAccCode)

        });
    }

    takePictureWeb() {
        let self: any = this;
        var fup = {
            fileSelector: document.createElement('input'),
            outputArea: undefined,
            listImages: function (evt) {
                var file = evt.target.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    let data: any = e.target
                    self.base64Image = data.result;
                    //self.imageURL = data.result.replace('data:image/jpeg;base64,', '');                                    
                    self.checkimage = true;
                    self.imagePrompt();
                };
                reader.readAsDataURL(file);
            }
        };

        fup.fileSelector.setAttribute('type', 'file');
        fup.fileSelector.setAttribute('accept', '.jpg');
        fup.fileSelector.onchange = function (event) {
            fup.listImages(event);
        };
        fup.fileSelector.click();
    }

    takePicture() {
        if (this.platform.is("cordova")) this.openCamera();
        else this.takePictureWeb();
    }

    openCamera() {
        this.serverService.isOpenCamera = true;

        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 330,
            targetHeight: 290,
            allowEdit: true,
        }
        this.camera.getPicture(options).then((imageData) => {
            this.serverService.isOpenCamera = false;

            this.base64Image = 'data:image/jpeg;base64,' + imageData;
            this.checkimage = true;
            this.imagePrompt();
        }, (err) => {
        });

    }

    onButtonClick() {
        this.buttonClicked = !this.buttonClicked;
        if (this.buttonClicked && this.customer.CurrAccCode) {
            this.presentLoading();
            this.userinformation.Parameters[0].Value = this.customer.CurrAccCode;
            this.serverService.getAny(this.userinformation).then(res => {
                console.log('G3_GetCustomerInformation');
                console.log(res);
                this.customerInformation = res;
                this.dismissLoading();
            }).catch(error => this.handleError(error));
            this.userwarning.Parameters[0].Value = this.customer.CurrAccCode;
            this.serverService.getAny(this.userwarning).then(res => {
                console.log('G3_GetCustomerWarning');
                console.log(res);
                this.customerWarning = res;
                this.dismissLoading();
            }).catch(error => this.handleError(error));
        }
    }

    presentPersonalInfoAlertRadio() {
        this.personelInfo = new CurrAccPersonalInfo();
    }

    oninput(event) {
        if (this.customerModel.IdentityNum.length == 11 && this.customerModel.IdentityNum != '11111111111' && this.customerModel.IdentityNum != '22222222222') {

            let bir = this.customerModel.IdentityNum.substring(0, 1);
            let iki = this.customerModel.IdentityNum.substring(1, 2);
            let uc = this.customerModel.IdentityNum.substring(2, 3);
            let dort = this.customerModel.IdentityNum.substring(3, 4);
            let bes = this.customerModel.IdentityNum.substring(4, 5);
            let alti = this.customerModel.IdentityNum.substring(5, 6);
            let yedi = this.customerModel.IdentityNum.substring(6, 7);
            let sekiz = this.customerModel.IdentityNum.substring(7, 8);
            let dokuz = this.customerModel.IdentityNum.substring(8, 9);
            let on = this.customerModel.IdentityNum.substring(9, 10);
            let onbir = this.customerModel.IdentityNum.substring(10, 11);
            let int1 = parseInt(bir);
            let int2 = parseInt(iki);
            let int3 = parseInt(uc);
            let int4 = parseInt(dort);
            let int5 = parseInt(bes);
            let int6 = parseInt(alti);
            let int7 = parseInt(yedi);
            let int8 = parseInt(sekiz);
            let int9 = parseInt(dokuz);
            let int10 = parseInt(on);
            let int11 = parseInt(onbir);

            //global değişkenlere atama
            this.number10 = int10;
            this.number11 = int11;

            if (int1 != 0) {
                this.total1 = ((int1 + int2 + int3 + int4 + int5 + int6 + int7 + int8 + int9 + int10) % 10)
                this.total2 = ((((int1 + int3 + int5 + int7 + int9) * 7) - (int2 + int4 + int6 + int8)) % 10)
                if (this.total1 == int11 && this.total2 == int10) {
                    console.log('Geçerli TC Kimlik Numarası');
                    this.getCustomersByIdentityNum();
                } else {
                    this.presentAlert('Hata', 'Hatalı TC Kimlik Numarası Girdiniz');
                    // return 0;
                }
            } else {
                this.presentAlert('Hata', 'TC Kimlik Numarası 0 ile Başlayamaz.')
                // return 0;
            }
        }
    }

    getCustomersByIdentityNum() {
        this.customersData.Parameters[2].Value = this.customerModel.IdentityNum
        this.serverService.getAny(this.customersData)
            .then(res => {
                this.identityNumCheckArray = res;
                if (res.length > 0) {
                    this.presentAlert('Bilgi', `Kayıtlı Müşteri. <hr />${res[0].FirstName + ' ' + res[0].LastName}`)
                } else {
                    this.presentAlert('Bilgi', 'Yeni Müşteri.');
                }
            })
    }

    maxLength(event: KeyboardEvent, field, maxlength) {
        let input = $('#' + field).find('input');
        if (input.length > 0) {
            //return /\d|Backspace/.test(event.key);
            if ([8, 9, 13, 27, 37, 38, 39, 40].indexOf(event.keyCode) > -1) {
                // backspace, enter, escape, arrows, tab
                return true;
            } else if (event.keyCode >= 48 && event.keyCode <= 57) {
                // numbers 0 to 9
                return (input.val().toString().length < maxlength);
            } else if (event.keyCode >= 96 && event.keyCode <= 105) {
                // numpad number
                return (input.val().toString().length < maxlength);
            }
            return false;
        }
        else return false;

    }

    imagePrompt() {
        let alert = this.alertCtrl.create({
            title: 'Resim İsim Formatı',
            inputs: [
                {
                    type: 'radio',
                    label: '4A hizmet dökümü',
                    value: '4A hizmet dökümü',

                },
                {
                    type: 'radio',
                    label: 'Dava dosyası',
                    value: 'Dava dosyası',

                },
                {
                    type: 'radio',
                    label: 'İcra Dosyası',
                    value: 'İcra Dosyası'
                },
                {
                    type: 'radio',
                    label: 'İkametgah',
                    value: 'İkametgah'
                },
                {
                    type: 'radio',
                    label: 'Kimlik arka yüzü',
                    value: 'Kimlik arka yüzü'
                },
                {
                    type: 'radio',
                    label: 'Kimlik ön yüzü',
                    value: 'Kimlik ön yüzü'
                },
                {
                    type: 'radio',
                    label: 'Maaş bordrosu',
                    value: 'Maaş bordrosu'
                },
                {
                    type: 'radio',
                    label: 'Taşınmaz kaydı',
                    value: 'Taşınmaz kaydı'
                },
                {
                    type: 'radio',
                    label: 'Vergi levhası',
                    value: 'Vergi levhası'
                }
            ],
            buttons: [
                {
                    text: 'İptal',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                        this.imageNameReturn = false;
                    }
                },
                {
                    text: 'Tamam',
                    handler: (data: string) => {
                        this.imageName = this.generateImageName(data);
                        this.imageNameReturn = true;
                        console.log(this.imageName);

                    }
                }
            ]
        });
        alert.present();
    }

    generateImageName(data: any) {
        var tempName: string;
        if (data == "Kimlik ön yüzü") {
            tempName = data;
            tempName = tempName.replace(/\s/g, "_");
        } else {
            var tzoffset = (new Date()).getTimezoneOffset() * 60000;
            var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1);
            var datetime = localISOTime.substr(0, 10).replace('-', '_').replace('-', '_');
            var templocal = localISOTime.substr(11, 8).replace(':', '_').replace(':', '_');
            tempName = data + '_' + datetime + '_' + templocal;
            tempName = tempName.replace(/\s/g, "_");

        }
        return tempName;
    }

    getGuarantors() {
        this.guarantorsData.Parameters[0].Value = this.customerModel.CurrAccCode;
        this.serverService.getAny(this.guarantorsData)
            .then(res => {
                console.log(res);
                if (Array.isArray(res)) this.guarantors = res;
            })
            .catch(this.handleError);
    }

    getPersonelInfo() {
        this.userData.ProcName = "G3_GetCustomerPersonalInfo";
        this.serverService.getAny(this.userData)
            .then(res => {
                this.personelInfo = res[0];
                if (res[0] && res[0].BirthDate)
                    this.personelInfo.BirthDate = this.serverService.convertISODate(res[0].BirthDate);
                console.log('personalInfo', this.personelInfo);
            })
            .catch(this.handleError);
    }

    getCustomerAttribute() {
        this.userData.ProcName = "G3_GetCustomerAttributes";
        this.userData.Parameters[0].Value = this.customer.CurrAccCode;
        this.serverService.getAny(this.userData)
            .then(res => {
                this.attributes = res;
                for (let i = 0; i < res.length; i++) {
                    if (res[i].AttributeTypeCode == 13) {
                        this.AttributeCode13 = res[i].AttributeCode;
                        console.log('AttributeCode13', this.AttributeCode13)
                    } else if (res[i].AttributeTypeCode == 14) {
                        this.AttributeCode14 = res[i].AttributeCode;
                        console.log('AttributeCode14', this.AttributeCode14)
                    } else if (res[i].AttributeTypeCode == 17) {
                        this.AttributeCode17 = res[i].AttributeCode;
                        this.getAttributeType17();
                        console.log('AttributeCode17', this.AttributeCode17)
                    }
                }
                console.log('CurrAccAttribute', res)
            })
            .catch(this.handleError);
    }

    addCustomerAttribute13(CurrAccCode) {
        let addData = {
            "ProcName": "G3_AddCustomerAttributes", "Parameters":
                [{ "Name": "CurrAccCode", "Value": "" }, { "Name": "AttributeTypeCode", "Value": 0 },
                { "Name": "AttributeCode", "Value": "" }, { "Name": "CreatedUserName", "Value": "" }]
        };
        addData.Parameters[0].Value = CurrAccCode;
        addData.Parameters[1].Value = 13;
        addData.Parameters[2].Value = this.AttributeCode13;
        addData.Parameters[3].Value = "V3   V3";
        this.serverService.getAny(addData);
        console.log(addData)
    }

    addCustomerAttribute14(CurrAccCode) {
        let addData = {
            "ProcName": "G3_AddCustomerAttributes", "Parameters":
                [{ "Name": "CurrAccCode", "Value": "" }, { "Name": "AttributeTypeCode", "Value": 0 },
                { "Name": "AttributeCode", "Value": "" }, { "Name": "CreatedUserName", "Value": "" }]
        };
        addData.Parameters[0].Value = CurrAccCode;
        addData.Parameters[1].Value = 14;
        addData.Parameters[2].Value = this.AttributeCode14;
        addData.Parameters[3].Value = "V3   V3";
        this.serverService.getAny(addData);
        console.log(addData)
    }

    addCustomerAttribute17(CurrAccCode) {
        let addData = {
            "ProcName": "G3_AddCustomerAttributes", "Parameters":
                [{ "Name": "CurrAccCode", "Value": "" }, { "Name": "AttributeTypeCode", "Value": 0 },
                { "Name": "AttributeCode", "Value": "" }, { "Name": "CreatedUserName", "Value": "" }]
        };
        addData.Parameters[0].Value = CurrAccCode;
        addData.Parameters[1].Value = 17;
        addData.Parameters[2].Value = this.AttributeCode17;
        addData.Parameters[3].Value = "V3   V3";
        this.serverService.getAny(addData);
        console.log(addData)
    }


    getAddressData() {
        for (let i = 0; i < this.customerModel.PostalAddresses.length; i++) {

            this.addrData.ProcName = "G3_GetStates";
            this.addrData.Parameters[0].Value = this.customerModel.PostalAddresses[i].CountryCode;
            this.serverService.getAny(this.addrData)
                .then(res => {

                    this.stateList[i] = res;


                }).catch(this.handleError);


            this.addrData.ProcName = "G3_GetCities";
            this.addrData.Parameters[0].Value = this.customerModel.PostalAddresses[i].StateCode;
            this.serverService.getAny(this.addrData)
                .then(res => {

                    this.cityList[i] = res;
                    //console.log(res);
                    //console.log(this.cityList)
                })
                .catch(this.handleError);

            this.addrData.ProcName = "G3_GetDistricts";
            this.addrData.Parameters[0].Value = this.customerModel.PostalAddresses[i].CityCode;
            this.serverService.getAny(this.addrData)
                .then(res => {
                    this.districtList[i] = res;

                })
                .catch(this.handleError);

        }

    }

    getCountries() {
        this.addrData.ProcName = "G3_GetCountries";
        this.serverService.getAny(this.addrData)
            .then(res => {
                this.countryList.splice(this.index, 0, res)
                //this.countryList[this.index] = res;
            })
            .catch(this.handleError);
    }


    // addG3Log(model,res){        
    //     this.g3_LogData.Parameters[0].Value = this.serverService.Settings.V3Settings.SalespersonCode;
    //     this.g3_LogData.Parameters[1].Value = "addCustomer";
    //     this.g3_LogData.Parameters[2].Value = model;
    //     this.g3_LogData.Parameters[3].Value = res;
    //     console.log(this.g3_LogData);
    //     this.serverService.getAny(this.g3_LogData)
    //         .then(res => {                
    //             console.log(res)
    //         })
    //         .catch(this.handleError);
    // }

    getStates(country: any) {
        this.addrData.ProcName = "G3_GetStates";
        this.addrData.Parameters[0].Value = country;
        this.serverService.getAny(this.addrData)
            .then(res => {
                this.stateList.splice(this.index, 0, res);

                //this.stateList[this.index] = res;
                console.log(this.stateList)
                console.log(res)
            })
            .catch(this.handleError);
    }

    getCities(state: any) {
        this.addrData.ProcName = "G3_GetCities";
        this.addrData.Parameters[0].Value = state;
        this.serverService.getAny(this.addrData)
            .then(res => {
                this.cityList.splice(this.index, 0, res);

                //this.cityList[this.index] = res;
            })
            .catch(this.handleError);
    }

    getDistricts(city: any) {
        this.addrData.ProcName = "G3_GetDistricts";
        this.addrData.Parameters[0].Value = city;
        this.serverService.getAny(this.addrData)
            .then(res => {
                this.districtList.splice(this.index, 0, res)
                //this.districtList[this.index] = res;

            })
            .catch(this.handleError);
    }

    getTaxOffices() {
        this.serverService.getAny(this.taxOfficeData)
            .then(res => {
                this.dismissLoading();
                this.taxOffices = res;
                console.log(this.taxOffices);
            })
            .catch(this.handleError);
    }

    getTaxOfficesDesc(data) {
        this.taxOfficeDataDesc.Parameters[0].Value = data;
        this.serverService.getAny(this.taxOfficeDataDesc)
            .then(res => {
                this.dismissLoading();
                this.taxOfficesTemp = res;
                if (res) {
                    this.tempTaxCheck = true;
                }
            })
            .catch(this.handleError);
    }

    presentLoading() {
        this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
            this.loading = this.loadingCtrl.create({
                content: value,
            });
            this.loading.present();
        });
    }

    dismissLoading() {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    }

    showAlert(title: string, subTitle: string) {
        this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
            let alert = this.alertCtrl.create({
                title: title,
                subTitle: subTitle,
                buttons: [value]
            });
            alert.present();
        });
    }

    goToOrderDetails(item: any) {
        //this.navCtrl.push(OrderDetailPage, item);
        let filtered_installments = [];
        for (let installment of this.installments) {
            if (installment.MonthYear == item.DueDate) {
                filtered_installments.push(installment);
            }
        }
        this.navCtrl.push(InstallmentsDetailPage, { installments: filtered_installments });
    }

    dismiss(item: any) {
        this.viewCtrl.dismiss();
    }

    addCustomer() {
        if (this.newMode != true) {
            let i = 0;
            let n = 0;
            for (i; i < this.customerModel.PostalAddresses.length; i++) {
                for (n; n < this.customerModel.PostalAddresses.length; n++) {
                    if (i != n && i < n && this.customerModel.PostalAddresses[i].AddressTypeCode == this.customerModel.PostalAddresses[n].AddressTypeCode && this.customerModel.PostalAddresses[i].AddressTypeCode == 1) {
                        this.customerModel.PostalAddresses[n].AddressTypeCode = 4;
                    }
                }
            }
        } else {
            //console.log(this.customerModel.personalInfo[0].BirthDate)
            //console.log(this.personelInfo.BirthDate)
            if (this.world_check != 'TR') {
                if (this.customerModel.IdentityNum.length != 11 || !this.customerModel.PostalAddresses[0].AddressTypeCode) {
                    this.presentAlert("Uyarı", "Lütfen Zorunlu Alanları Doldurunuz.");
                    return 0;
                }
            } else {
                if (this.customerModel.IdentityNum.length != 11 || !this.customerModel.PostalAddresses[0].AddressTypeCode ||
                    !this.customerModel.PostalAddresses[0].StateCode || !this.customerModel.PostalAddresses[0].CityCode ||
                    !this.customerModel.PostalAddresses[0].DistrictCode) {
                    //console.log(this.customerModel.IdentityNum.length, this.customerModel.PostalAddresses[0].AddressTypeCode,
                    //this.customerModel.PostalAddresses[0].StateCode, this.customerModel.PostalAddresses[0].CityCode,
                    //this.customerModel.PostalAddresses[0].DistrictCode)
                    this.presentAlert("Uyarı", "Lütfen Zorunlu Alanları Doldurunuz.");
                    return 0;
                }
            }




            if (this.customerModel.IdentityNum.length == 11) {
                if (this.customerModel.IdentityNum != '11111111111' && this.customerModel.IdentityNum != '22222222222') {
                    if (this.total1 == this.number11 && this.total2 == this.number10) {
                        console.log('Geçerli TC Kimlik No.');

                        if (this.identityNumCheckArray.length == 0) {
                            console.log('Geçerli ve Yeni TC Kimlik No.');

                        } else {
                            this.presentAlert("Uyarı", "Bu TC Kimlik Numarası Eski Kayıtlarda Mevcuttur.");
                            return 0;
                        }
                    } else {
                        this.presentAlert("Uyarı", "Geçerli TC Kimlik Numarası Giriniz!");
                        return 0;
                    }
                }
            } else {
                this.presentAlert("Uyarı", "TC Kimlik Numarası 11 Haneden Oluşmaktadır.");

                return 0;
            }
        }

        this.presentLoading();

        //Toptan musteri ise
        if (this.serverService.Settings.V3Settings.SalesType == 1 || this.serverService.Settings.V3Settings.SalesType == 4) {
            this.customerModel.ModelType = 2;
            this.customerModel.CurrAccDescription = this.customerModel.FirstName + ' ' + this.customerModel.LastName;
            this.customerModel.OfficeCode = this.serverService.Settings.V3Settings.OfficeCode;
            this.customerModel.WholeSalePriceGroupCode = this.serverService.Settings.V3Settings.PriceGroupCode;
            this.customerModel.TaxOfficeCode = this.customerModel.TaxOfficeCode;
            this.customerModel.TaxNumber = this.customerModel.TaxNumber;
        }
        if (this.personelInfo) {
            this.customerModel.CurrAccPersonalInfo = this.personelInfo;
            console.log('personalinfo', this.customerModel.CurrAccPersonalInfo)
        }


        //console.log(this.customerModel);
        this.serverService.addCustomer(this.customerModel)
            .then(res => {
                this.loading.dismiss();
                //console.log(res);

                let result: Customer = Object.assign(new Customer(), res);
                if (this.base64Image !== undefined) {
                    if (this.imageNameReturn != false) {
                        this.saveImage(result.CurrAccCode);
                    }
                }
                //console.log(JSON.stringify(this.customerModel).toString());
                //this.addG3Log(JSON.stringify(this.customerModel),JSON.stringify(res));

                if (result.ModelType == 3 || result.ModelType == 2) {
                    this.translateService.get(['ALERT_INFORMATION_TITLE_TEXT', 'CUSTOMER_ALERT_CUSTOMER_INFORMATIONS_SAVED_SUCCESSFULLY_MESSAGE_TEXT']).subscribe((value: string[]) => {
                        this.presentAlert(value['ALERT_INFORMATION_TITLE_TEXT'], value['CUSTOMER_ALERT_CUSTOMER_INFORMATIONS_SAVED_SUCCESSFULLY_MESSAGE_TEXT']);
                    });
                    this.editMode = false;

                    // add attributes start
                    if (this.AttributeCode13 != null)
                        this.addCustomerAttribute13(result.CurrAccCode);
                    if (this.AttributeCode14 != null)
                        this.addCustomerAttribute14(result.CurrAccCode);
                    if (this.AttributeCode17 != null)
                        this.addCustomerAttribute17(result.CurrAccCode);
                    // add attributes end

                    if (this.newMode) {

                        //  [G3_SetCustomerPostalAddresses]
                        this.updateAddressData.Parameters[0].Value = result.CurrAccCode;
                        this.serverService.getAny(this.updateAddressData);


                        this.newMode = false;
                        this.viewCtrl.dismiss({ customer: result });
                    }
                } else {
                    let result: Exception = Object.assign(new Exception(), res);
                    this.translateService.get('ALERT_ERROR_TITLE_TEXT').subscribe((value: string) => {
                        this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], this.serverService.getReadableMessage(result.ExceptionMessage));
                    });
                }
            })
            .catch(this.handleError);
    }

    saveImage(CurrAccCode) {
        if (CurrAccCode == "") {
            CurrAccCode = this.customerModel.currAccCode;
        }
        this.serverService.addCustomerImage(CurrAccCode, this.base64Image.replace('data:image/jpeg;base64,', ''), this.imageName);
        this.imageURL = '';
        this.imageURL = this.serverService.Settings.G3Settings.ImageUrl + '/Home/GetThumbCustomer?curracccode=' + CurrAccCode;
    }

    handleError(error: any) {
        console.error('An error occurred', error);
        this.dismissLoading();
    }

    presentAlert(title: string, subTitle: string) {
        this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
            let alert = this.alertCtrl.create({
                title: title,
                subTitle: subTitle,
                buttons: [value]
            });
            alert.present();
        });
    }

    presentToast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: 'X'
        });
        toast.present();
    }

    addCommAddress() {
        this.translateService.get(['CUSTOMER_ALERT_ADD_CUSTOMER_INFORMATIONS_TITLE_TEXT', 'CUSTOMER_ALERT_ADD_CUSTOMER_INFORMATIONS_MESSAGE_TEXT', 'ALERT_BUTTON_CANCEL_TEXT', 'ALERT_BUTTON_ADD_TEXT', 'CUSTOMER_ALERT_ADD_CUSTOMER_RADIO_PHONE_LABEL_TEXT', 'CUSTOMER_ALERT_ADD_CUSTOMER_RADIO_FAX_LABEL_TEXT', 'CUSTOMER_ALERT_ADD_CUSTOMER_RADIO_EMAIL_LABEL_TEXT', 'CUSTOMER_ALERT_ADD_CUSTOMER_RADIO_CELLPHONE_LABEL_TEXT']).subscribe((value: string[]) => {
            let prompt = this.alertCtrl.create({
                title: value['CUSTOMER_ALERT_ADD_CUSTOMER_INFORMATIONS_TITLE_TEXT'],
                message: value['CUSTOMER_ALERT_ADD_CUSTOMER_INFORMATIONS_MESSAGE_TEXT'],
                buttons: [
                    {
                        text: value['ALERT_BUTTON_CANCEL_TEXT'],
                        handler: data => {

                        }
                    },
                    {
                        text: value['ALERT_BUTTON_ADD_TEXT'],
                        handler: data => {
                            this.customerModel.Communications.push({
                                CommunicationTypeCode: data,
                                CommAddress: ""
                            });
                        }
                    }
                ]
            });
            prompt.addInput({
                type: 'radio',
                label: value['CUSTOMER_ALERT_ADD_CUSTOMER_RADIO_PHONE_LABEL_TEXT'],
                value: '1',
                checked: true
            });
            prompt.addInput({
                type: 'radio',
                label: value['CUSTOMER_ALERT_ADD_CUSTOMER_RADIO_FAX_LABEL_TEXT'],
                value: '2'
            });
            prompt.addInput({
                type: 'radio',
                label: value['CUSTOMER_ALERT_ADD_CUSTOMER_RADIO_EMAIL_LABEL_TEXT'],
                value: '3'
            });
            prompt.addInput({
                type: 'radio',
                label: value['CUSTOMER_ALERT_ADD_CUSTOMER_RADIO_CELLPHONE_LABEL_TEXT'],
                value: '7'
            });
            prompt.present();
        });
    }

    addPostalAddresses() {
        let modal;
        modal = this.modalCtrl.create(ModalAddaddressPage);
        modal.present();
        modal.onDidDismiss(data => {
            console.log(data);
            console.log(data.result);

            if (data.result == false) return;// if press 'BACK-ARROW'

            this.index = 0; //this.customerModel.PostalAddresses.length;
            this.customerModel.PostalAddresses.splice(0, 0, {});   //splice(position,numberofitemtoREMOVE,item) //other; .push({});
            this.world_check = data.txtDCoCode;
            this.customerModel.PostalAddresses[0].CountryCode = data.txtDCoCode;
            this.customerModel.PostalAddresses[0].AddressTypeCode = data.txtDaddtype;
            this.customerModel.PostalAddresses[0].Address = data.txtDAddress;
            this.customerModel.PostalAddresses[0].StateCode = data.txtDSCode;
            this.customerModel.PostalAddresses[0].CityCode = data.txtDCiCode;
            this.customerModel.PostalAddresses[0].DistrictCode = data.txtDDCode;
            this.customerModel.PostalAddresses[0].BuildingNum = data.txtDBNum;
            this.customerModel.PostalAddresses[0].FloorNum = data.txtDFNum;
            this.customerModel.PostalAddresses[0].DoorNum = data.txtDDNum;
            this.getStates(data.txtDCoCode);
            this.getCities(data.txtDSCode);
            this.getDistricts(data.txtDCiCode);

        });
    }

    onCountryClick() {
        const modal = this.modalCtrl.create(CountryPage, { countries: this.countryList });
        modal.present();
        modal.onDidDismiss(data => {
            if (data) {
                this.customerModel.PostalAddresses[this.index].CountryCode = data.Code;
                this.getStates(data.Code);
            }
        });
    }

    onCountryChange() {
        const code = this.customerModel.PostalAddresses[this.index].CountryCode;
        this.getStates(code);
    }

    onStateChange() {
        const code = this.customerModel.PostalAddresses[this.index].StateCode;
        this.getCities(code);
    }

    onCityChange() {
        const code = this.customerModel.PostalAddresses[this.index].CityCode;
        this.getDistricts(code);
    }

    gotoInstallments() {
        this.navCtrl.push(InstallmentsPage, {
            installments: this.installments,
            installments_summary: this.installments_summary
        });
    }

    gotoExtras() {
        this.navCtrl.push(ExtraReportPage, { currAccCode: this.customer.CurrAccCode });
    }

    addGuarantor() {
        let modal;
        modal = this.modalCtrl.create(ModalGuarantorPage);
        modal.present();
        modal.onDidDismiss(data => {
            console.log(data);
            let addData = {
                "ProcName": "G3_AddGuarantor", "Parameters":
                    [{ "Name": "CurrAccCode", "Value": "" }, { "Name": "FirstName", "Value": "" },
                    { "Name": "LastName", "Value": "" }, { "Name": "IdentityNum", "Value": "" },
                    { "Name": "AddressTypeCode", "Value": "" }, { "Name": "Address", "Value": "" },
                    { "Name": "CountryCode", "Value": "" }, { "Name": "StateCode", "Value": "" },
                    { "Name": "CityCode", "Value": "" }, { "Name": "DistrictCode", "Value": "" },
                    { "Name": "BuildingNum", "Value": "" }, { "Name": "FloorNum", "Value": "" },
                    { "Name": "DoorNum", "Value": "" }, { "Name": "CommunicationTypeCode", "Value": "" },
                    { "Name": "CommAddress", "Value": "" },

                    { "Name": "BirthPlace", "Value": "" }, { "Name": "MotherName", "Value": "" },
                    { "Name": "FatherName", "Value": "" }, { "Name": "MaidenName", "Value": "" },
                    { "Name": "MonthlyIncome", "Value": "" }, { "Name": "DrivingLicenceType", "Value": "" },
                    { "Name": "DrivingLicenceTypeNum", "Value": "" }, { "Name": "IdentityCardNum", "Value": "" },
                    { "Name": "RegisteredCityCode", "Value": "" }, { "Name": "RegisteredDistrictCode", "Value": "" },
                    { "Name": "RegisteredTown", "Value": "" }, { "Name": "RegisteredNum", "Value": "" },
                    { "Name": "RegisteredRecordNum", "Value": "" }, { "Name": "RegisteredFamilyNum", "Value": "" },
                    { "Name": "RegisteredFileNum", "Value": "" }, { "Name": "SocialInsuranceNumber", "Value": "" },
                    { "Name": "BirthDate", "Value": "" }, { "Name": "GenderCode", "Value": "" }


                    ]

            };


            addData.Parameters[0].Value = this.customerModel.CurrAccCode;
            addData.Parameters[1].Value = data.txtGname;
            addData.Parameters[2].Value = data.txtGLname;
            addData.Parameters[3].Value = data.txtGInumber;

            addData.Parameters[4].Value = data.txtDaddtype //AddressTypeCode
            addData.Parameters[5].Value = data.txtDAddress//Address
            addData.Parameters[6].Value = data.txtDCoCode//CountryCode
            addData.Parameters[7].Value = data.txtDSCode//StateCode
            addData.Parameters[8].Value = data.txtDCiCode         //CityCode
            addData.Parameters[9].Value = data.txtDDCode    //DistrictCode
            addData.Parameters[10].Value = data.txtDBNum         //BuildingNum
            addData.Parameters[11].Value = data.txtDFNum        //FloorNum
            addData.Parameters[12].Value = data.txtDDNum          //DoorNum

            addData.Parameters[13].Value = '1'    //CommunicationTypeCode
            addData.Parameters[14].Value = data.txtGtel //CommAddress


            addData.Parameters[15].Value = data.personelInfo_BirthPlace
            addData.Parameters[16].Value = data.personelInfo_MotherName;
            addData.Parameters[17].Value = data.personelInfo_FatherName;
            addData.Parameters[18].Value = data.personelInfo_MaidenName;
            addData.Parameters[19].Value = data.personelInfo_MonthlyIncome
            addData.Parameters[20].Value = data.personelInfo_DrivingLicenceType
            addData.Parameters[21].Value = data.personelInfo_DrivingLicenceTypeNum
            addData.Parameters[22].Value = data.personelInfo_IdentityCardNum
            addData.Parameters[23].Value = data.personelInfo_RegisteredCityCode
            addData.Parameters[24].Value = data.personelInfo_RegisteredDistrictCode
            addData.Parameters[25].Value = data.personelInfo_RegisteredTown
            addData.Parameters[26].Value = data.personelInfo_RegisteredNum
            addData.Parameters[27].Value = data.personelInfo_RegisteredRecordNum
            addData.Parameters[28].Value = data.personelInfo_RegisteredFamilyNum
            addData.Parameters[29].Value = data.personelInfo_RegisteredFileNum
            addData.Parameters[30].Value = data.personelInfo_SocialInsuranceNumber
            addData.Parameters[31].Value = data.personelInfo_BirthDate
            addData.Parameters[32].Value = data.personelInfo_GenderCode
            console.log(data.personelInfo_BirthDate, data.personelInfo_GenderCode)
            if (data.personelInfo_BirthPlace == null) addData.Parameters[15].Value = ""
            if (data.personelInfo_MotherName == null) addData.Parameters[16].Value = ""
            if (data.personelInfo_FatherName == null) addData.Parameters[17].Value = ""
            if (data.personelInfo_MaidenName == null) addData.Parameters[18].Value = ""
            if (data.personelInfo_MonthlyIncome == null) addData.Parameters[19].Value = ""
            if (data.personelInfo_DrivingLicenceType == null) addData.Parameters[20].Value = ""
            if (data.personelInfo_DrivingLicenceTypeNum == null) addData.Parameters[21].Value = ""
            if (data.personelInfo_IdentityCardNum == null) addData.Parameters[22].Value = ""
            if (data.personelInfo_RegisteredCityCode == null) addData.Parameters[23].Value = ""
            if (data.personelInfo_RegisteredDistrictCode == null) addData.Parameters[24].Value = ""
            if (data.personelInfo_RegisteredTown == null) addData.Parameters[25].Value = ""
            if (data.personelInfo_RegisteredNum == null) addData.Parameters[26].Value = ""
            if (data.personelInfo_RegisteredRecordNum == null) addData.Parameters[27].Value = ""
            if (data.personelInfo_RegisteredFamilyNum == null) addData.Parameters[28].Value = ""
            if (data.personelInfo_RegisteredFileNum == null) addData.Parameters[29].Value = ""
            if (data.personelInfo_SocialInsuranceNumber == null) addData.Parameters[30].Value = ""
            if (data.personelInfo_BirthDate == null) addData.Parameters[31].Value = ""
            if (data.personelInfo_GenderCode == null) addData.Parameters[32].Value = ""

            console.log(addData)
            this.serverService.getAny(addData)
                .then(res => {
                    console.log(res);
                    this.getGuarantors();
                })
                .catch(this.handleError);

        });
    }
}
