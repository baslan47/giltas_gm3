import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, NavParams, ModalController, ViewController, InfiniteScroll } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import { CustomerPage } from '../customer/customer';
import { Customer } from "../../classes/customer";
import { Storage } from '@ionic/storage';
import { TranslateService } from "@ngx-translate/core";
import { ExtraReportPage } from "../extra-report/extra-report";
import { OrderReportPage } from "../order-report/order-report";
import { ModalCustomersPage } from '../modal-customers/modal-customers';
import { AwsTestPage } from '../aws-test/aws-test';
import { ProposalPage } from '../proposal/proposal';

@Component({
  selector: 'page-customers',
  templateUrl: 'customers.html'
})
export class CustomersPage {
  public myInput: string;
  pagefetch: number = 0;
  checkdatabase: number = 0;
  pagefetchcount: number = 0;
  public dataList: any[];
  checklog: boolean = false;
  public items: Array<Customer>;
  userData = {
    "ProcName": "", "Parameters": [{ "Name": "FirstName", "Value": "" }, { "Name": "LastName", "Value": "" },
    { "Name": "IdentityNumber", "Value": "" }, { "Name": "PhoneNumber", "Value": "" }, { "Name": "Code", "Value": "" },
    { "Name": "FetchPage", "Value": "0" }, { "Name": "FirstLastName", "Value": "" }]
  };
  userDataFetch = {
    "ProcName": "", "Parameters": [{ "Name": "FirstName", "Value": "" }, { "Name": "LastName", "Value": "" },
    { "Name": "IdentityNumber", "Value": "" }, { "Name": "PhoneNumber", "Value": "" }, { "Name": "Code", "Value": "" },
    { "Name": "FirstLastName", "Value": "" }]
  };

  loading: Loading;

  // customerdetail, extrareport, orderreport
  nextPage: string = 'customerdetail';

  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController,public viewCtrl: ViewController,
              public loadingCtrl: LoadingController, public serverService: ServerService, public storage: Storage,
              public translateService: TranslateService, public navParams: NavParams, public modalCtrl: ModalController) {  
                this.dataList = [];                                           
                if (this.navParams.get('nextPage'))
                this.nextPage = this.navParams.get('nextPage');  
                //JS if(this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2'){
                if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
                  this.checkdatabase = 1;
                }                
  }

  ionViewWillEnter() {
    if (this.checklog == false) {
      this.presentLoading();
      console.log(this.serverService.Settings.V3Settings.SalesType)
      if (this.serverService.Settings.V3Settings.SalesType) {
        switch (this.serverService.Settings.V3Settings.SalesType.toString()) {
          case '1':
            this.userData.ProcName = "G3_CustomerListWholePaging";
            const cType = { "Name": "CustomerType", "Value": "0" };
            this.userData.Parameters.push(cType);
            this.userDataFetch.ProcName = "G3_CustomerListWholefetchCount";
            break;
          case '2':
            this.userData.ProcName = "G3_CustomerListRetailPaging";
            this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
            break;
          case '3':
            this.userData.ProcName = "G3_CustomerListRetailPaging";
            this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
            break;
          case '4':
            this.userData.ProcName = "G3_CustomerListWholePaging";
            const cType2 = { "Name": "CustomerType", "Value": "3" };
            this.userData.Parameters.push(cType2);
            this.userDataFetch.ProcName = "G3_CustomerListWholefetchCount";
            break;
          case '5':
            this.userData.ProcName = "G3_CustomerListRetailPaging";
            this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
            break;
          case '6':
            this.userData.ProcName = "G3_CustomerListRetailPaging";
            this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
            break;
          default:
            if(this.serverService.Settings.Integrator.DatabaseName == 'FIDAN')
            {
              this.userData.ProcName = "G3_CustomerListWholePaging";
              const cType0 = { "Name": "CustomerType", "Value": "0" };
              this.userData.Parameters.push(cType0);
              this.userDataFetch.ProcName = "G3_CustomerListWholefetchCount";
              break;
            }
            else
            {
              this.userData.ProcName = "G3_CustomerListRetailPaging";
              this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
              break;
            }
            
        }
      }
      else {
        this.userData.ProcName = "G3_CustomerListRetailPaging";
        this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
      }
      this.serverService.getCustomers(this.userData)
        .then(res => {
          console.log(res)
          this.items = res;
          this.loading.dismiss();
          if (res.length > 10) {
            for (let i = 0; i < 10; i++) {
              this.dataList.push(res[i]);
            }
          }else{
            for (let i = 0; i < res.length; i++) {
              this.dataList.push(res[i]);
            }
          }
          this.items=this.dataList;
          console.log(this.items);
        })
        .catch(this.handleError);
      this.serverService.getCustomers(this.userDataFetch)
        .then(res => {
          console.log(res);
          this.pagefetchcount = res[0]["Total"];
          console.log('pagefetchcount',this.pagefetchcount);
        })
        .catch(this.handleError);
    }
  }

  ionViewDidLoad() {
    //this.loader.dismiss();
  }

  loadData(infiniteScroll?) {
    this.pagefetch += 10;
    this.userData.Parameters[5].Value = this.pagefetch.toString();
    this.serverService.getCustomers(this.userData)
      .then(res => {
        console.log(res)
        this.items = res;
        for (let i = 0; i < this.items.length; i++) {
          this.dataList.push(this.items[i]);
          if (this.dataList.length == this.pagefetchcount) {
            console.log("Done");
            infiniteScroll.enable(false);
            break;
          }
        }
        infiniteScroll.complete();
        console.log(this.dataList);
      })
      .catch(this.handleError);
  }

  refreshFilter() {
    this.infiniteScroll.enable(true);
    this.dataList = [];
    this.pagefetch = 0;
    this.userData.Parameters[0].Value = "";
    this.userData.Parameters[1].Value = "";
    this.userData.Parameters[2].Value = "";
    this.userData.Parameters[3].Value = "";
    this.userData.Parameters[4].Value = "";
    this.userData.Parameters[5].Value = "0";
    this.userData.Parameters[6].Value = "";
    this.userDataFetch.Parameters[0].Value = "";
    this.userDataFetch.Parameters[1].Value = "";
    this.userDataFetch.Parameters[2].Value = "";
    this.userDataFetch.Parameters[3].Value = "";
    this.userDataFetch.Parameters[4].Value = "";
    this.userDataFetch.Parameters[5].Value = "";

  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string[]) => {
      this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
    if (this.loading)
      this.loading.dismiss();
  }

  public presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  public showAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  goTo(customer: Customer) {
    this.checklog = true;
    console.log(this.nextPage);
    switch (this.nextPage) {
      case 'customerdetail':
        this.goToCustomerDetail(customer);
        break;
      case 'extrareport':
        this.goToExtraRepoer(customer);
        break;
      case 'orderreport':
        this.goToOrderRepoer(customer);
        break;
    }
  }

  goToCustomerDetail(customer: Customer) {
    this.navCtrl.push(CustomerPage, { customer: customer, showImage: true });
  }

  goToExtraRepoer(customer: Customer) {
    this.navCtrl.push(ExtraReportPage, { currAccCode: customer.CurrAccCode });
  }

  goToOrderRepoer(customer: Customer) {
    this.navCtrl.push(OrderReportPage, { currAccCode: customer.CurrAccCode });
  }

  onInput(event: any) {
    if (this.myInput.length >= 3) {
      // TODO
    }
  }

  onCancel(event: any) {
    this.doFilter();
  }

  doFilter() {
    this.presentLoading();
    this.serverService.getCustomers(this.userData)
      .then(res => {
        this.items = res;
        console.log(res);
        this.loading.dismiss();
        if (this.items.length > 0) {
          for (let i = 0; i < 10; i++) {
            this.dataList.push(res[i]);
            if (this.dataList.length == this.items.length) {
              if (this.dataList.length < 10) {
                this.infiniteScroll.enable(false);
              }
              break;
            }
          }
        } else {
          console.log("Aranan Kayıtlar Yok");
        }
        this.serverService.getCustomers(this.userDataFetch)
          .then(res => {
            this.pagefetchcount = res[0]["Total"];
            console.log(this.pagefetchcount);
          })
          .catch(this.handleError);
      })
      .catch(this.handleError);
  }

  sendMessageWithEnterKey(event: any) {
    event.preventDefault();
    this.doFilter();
  }

  selectCustomer(customer: Customer) {
    console.log(customer);
    if (this.checkdatabase == 1) {
      this.navCtrl.push(ProposalPage, { res: true, customer: customer });
    } else {
      this.viewCtrl.dismiss({ customer: customer });
    }
  }

  addCustomer() {
    const modal = this.modalCtrl.create(CustomerPage, { showBack: true, showImage: false });
    modal.present();
    modal.onDidDismiss(
      data => {
        if (data) {
          this.selectCustomer(data.customer);
          console.log(data);
        }
      }
    );
    //this.createCustomerWithImage();    
  }

  createCustomerWithImage() {
    let alert = this.alertCtrl.create({
      title: 'Nüfüs Cüzdanı İle Müşteri Oluşturmak İster Misiniz?',
      buttons: [
        {
          text: 'Evet',
          handler: data => {
            const modal = this.modalCtrl.create(AwsTestPage);
            modal.present();
            modal.onDidDismiss(
              data => {
                this.navCtrl.push(CustomerPage, { showBack: true, res: data.result });
              }
            );
          }
        },
        {
          text: 'Hayır',
          handler: data => {
            const modal = this.modalCtrl.create(CustomerPage, { showBack: true });
            modal.present();
            modal.onDidDismiss(
              data => {
                if (data) {
                  this.selectCustomer(data.customer);
                }
              }
            );
          }
        },
      ]
    });
    alert.present();
  }


  filterModal() {
    this.refreshFilter()
    let modal;
    modal = this.modalCtrl.create(ModalCustomersPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data.txtCN) {
        this.userData.Parameters[0].Value = data.txtCN;
        this.userDataFetch.Parameters[0].Value = data.txtCN;
      }
      if (data.txtCLN) {
        this.userData.Parameters[1].Value = data.txtCLN;
        this.userDataFetch.Parameters[1].Value = data.txtCLN;
      }
      else if (data.txtCFLN) {
        this.userData.Parameters[6].Value = data.txtCFLN;
        this.userDataFetch.Parameters[5].Value = data.txtCFLN;
      }
      else if (data.txtCTC) {
        this.userData.Parameters[2].Value = data.txtCTC;
        this.userDataFetch.Parameters[2].Value = data.txtCTC;
      }
      else if (data.txtCT) {
        this.userData.Parameters[3].Value = data.txtCT;
        this.userDataFetch.Parameters[3].Value = data.txtCT;
      }
      else if (data.txtCC) {
        this.userData.Parameters[4].Value = data.txtCC;
        this.userDataFetch.Parameters[4].Value = data.txtCC;
      }
      this.doFilter();
    });
  }
}
