import { Component } from '@angular/core';
import { AlertController, Loading, LoadingController, NavController, NavParams, ToastController, ModalController, Slides } from 'ionic-angular';
import { InvoiceDetailPage } from "../invoice-detail/invoice-detail";
import { ServerService } from "../../classes/server.service";
import { TranslateService } from "@ngx-translate/core";
import { ModalFilterReportPage } from '../modal-filter-report/modal-filter-report';
import { InstallmentsPage } from '../installments/installments';
import { InstallmentsDetailPage } from '../installments-detail/installments-detail';

@Component({
  selector: 'page-extra-report',
  templateUrl: 'extra-report.html',
})
export class ExtraReportPage {
  loading: Loading;
  currAccCode: any;
  customerExtra: any[];
  tempLength: any[];
  length: number = 0;
  checkNetAmount: boolean = false;
  customerExtraResult: any[];
  userData = {
    "ProcName": "",
    "Parameters": [{ "Name": "CurrAccCode", "Value": "" }, { "Name": "LangCode", "Value": "TR" }, { "Name": "Str1", "Value": "" },  
    { "Name": "Str2", "Value": "" }]
  };

  userDataInstallment = {
    "ProcName": "",
    "Parameters": [{ "Name": "CurrAccCode", "Value": "" }, { "Name": "LangCode", "Value": "TR" }]
  };  

 

  balance_loc: number = 0;
  balance_doc: number = 0;
  total: number = 0;
  installmentTotal: number = 0;
  total_debit: number = 0;
  total_credit: number = 0;
  tempTotal: number = 0;
  buttonClicked: boolean = true;  
  page = "0";
  tempNet: boolean = false;
  installments: any;
  installments_summary = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public serverService: ServerService,
    public alertCtrl: AlertController, public toastCtrl: ToastController,
    public translateService: TranslateService, public loadingCtrl: LoadingController) {
    this.currAccCode = this.navParams.get('currAccCode');
    this.customerExtraResult = [];
    this.customerExtra = [];
  }  

  ionViewWillEnter() {
    this.reset();
    this.getExtras();
    this.getInstallments();
  }

  getInstallments() {    
      this.length = 0;     
      this.userDataInstallment.ProcName = "G3_GetCustomerInstallments";
      this.userDataInstallment.Parameters[0].Value = this.currAccCode;
      this.serverService.getAny(this.userDataInstallment)
          .then(res => {
              console.log('installments');
              this.installments = res;
              this.tempLength = res;
              this.installments_summary = [];
              let debit: number = 0;
              let desc: string = '';
              let date: string = '';   
              let temp: number = 0;           
              for (let installement of this.installments) {
                  this.length++;
                  //console.log(installement.InstallmentAmount);
                  if (date != installement.MonthYear) {                                            
                      //console.log(installement.MonthYear);                      
                      if (debit > 0) {
                          temp++;
                          let installement_summary = {
                              DueDate: this.getDateFixed(date),
                              ApplicationDescription: desc,
                              InstallmentAmount: debit.toFixed(2)
                          };
                          this.installments_summary.push(installement_summary);
                          this.installmentTotal += debit;
                          debit = parseFloat(installement.RemainingInstallment);
                          desc = installement.ApplicationDescription;
                          date = installement.MonthYear;   
                          if(this.length == this.tempLength.length){
                            let installement_summary = {
                              DueDate: this.getDateFixed(date),
                              ApplicationDescription: desc,
                              InstallmentAmount: debit.toFixed(2)
                          };
                          this.installments_summary.push(installement_summary);
                          this.installmentTotal += debit;
                          }                     
                      }
                      else {
                          date = installement.MonthYear;
                          desc = installement.ApplicationDescription;
                          debit += parseFloat(installement.RemainingInstallment);                                                                            
                      }
                  }
                  else {
                      debit += parseFloat(installement.RemainingInstallment);
                  }                                                   
              }     
              if(temp == 0 && date){
                let installement_summary = {
                  DueDate: this.getDateFixed(date),
                  ApplicationDescription: desc,
                  InstallmentAmount: debit.toFixed(2)
                };
                this.installments_summary.push(installement_summary);
                this.installmentTotal += debit;              
              }                                                          
              this.installmentTotal = parseFloat(this.installmentTotal.toFixed(2));
              console.log(this.installmentTotal);
              console.log(this.installments);
              console.log(this.installments_summary);
          })
          .catch(this.handleError);            
  } 

  goToInstallmentDetails(item: any) {
    let filtered_installments = [];
    for (let installment of this.installments) {
      if (this.getDateFixed(installment.MonthYear) == item.DueDate) {
        filtered_installments.push(installment);
      }
    }
    let modal;
    modal = this.modalCtrl.create(InstallmentsDetailPage, {installments: filtered_installments});
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data.result);
    });    
  }

  getDateFixed(tempDate: string){
    var txtsub = tempDate.substring(tempDate.length - 2, tempDate.length);
    var txtyyear = tempDate.substring(0,4);
    var txtind: number;
    if(txtsub[0] == '0'){
      txtind = parseInt(txtsub[1]);
    }else{
      txtind = parseInt(txtsub);
    }
    const monthNames = ['Ocak','Şubat','Mart','Nisan','Mayıs','Haziran','Temmuz',
    'Ağustos','Eylül','Ekim','Kasım','Aralık'];    
    return txtsub = monthNames[txtind-1] + " " + txtyyear;
  }  

  

  onButtonClick() {
    this.buttonClicked = !this.buttonClicked;
    /*if (this.buttonClicked == true) {
      document.getElementById('headerStyle').style.display = 'none';
      //document.getElementById('yns').style.textAlign = 'left';
      // (document.getElementsByClassName("textAlignSet") as HTMLCollectionOf<HTMLElement>)[0].style.textAlign = 'left';
    } else {
      document.getElementById('headerStyle').style.display = 'flex';
    }*/
  }

  reset() {
    this.balance_loc = 0;
    this.balance_doc = 0;
    this.total = 0;
    this.total_debit = 0;
    this.total_credit = 0;    
    this.installmentTotal = 0;   
    //this.tempTotal = 0;
  }

  filterModal() {
    let modal;
    modal = this.modalCtrl.create(ModalFilterReportPage);
    modal.present();
    modal.onDidDismiss(data => {             
      if(data.result != false){
        if(data.txtSD && data.txtED) {
          this.userData.ProcName = "G3_GetCustomerExtraRetailSaleWithDate";          
          this.userData.Parameters[2].Value = ((data.txtSD + '').replace('-','')).replace('-','');    
          this.userData.Parameters[3].Value = ((data.txtED + '').replace('-','')).replace('-','');  
          console.log((data.txtMinT) + " " + data.txtMaxT);                      
        }else if(data.txtMinT && data.txtMaxT){
          this.userData.ProcName = "G3_GetCustomerExtraRetailSaleWithtTotal";          
          this.userData.Parameters[2].Value = data.txtMinT;  
          this.userData.Parameters[3].Value = data.txtMaxT;       
          console.log((data.txtMinT) + " " + data.txtMaxT);
        }
        this.reset();               
        this.getExtrasWithConditions();        
      }else{
        this.refreshFilter();
      }           
    });
  }    

  filterReport() {
    this.userData.Parameters[2].Value = '';
    let prompt = this.alertCtrl.create({
      cssClass: 'alertCustomCss2',
      title: 'FİLTRELE',
      inputs: [
        {
          name: 'start_date',
          type: 'date'
        },
        {
          name: 'end_date',
          type: 'date'
        },
      ],
      buttons: [
        {
          text: 'Sonuçları Getir',
          handler: data => {
            console.log(data);
            const whrStr: string = " and orderdate between '" + data.start_date + "' and '" + data.end_date + "'";
            this.userData.Parameters[2].Value = whrStr;
            this.reset();
            this.getExtras();
          }
        },
        {
          text: 'İptal',
          handler: data => {

          }
        },
      ]
    });
    prompt.present();
  }

  getExtrasWithConditions() {
    this.customerExtraResult = [];
    this.customerExtra = [];
    this.presentLoading();    
    this.userData.Parameters[0].Value = this.currAccCode;
    this.serverService.getAny(this.userData)
      .then(res => {
        this.dismissLoading();
        console.log(this.userData.ProcName);
        console.log(res);
        this.customerExtra = res;
        for(let i = 0; i < this.customerExtra.length; i++){
          if(this.customerExtra[i]["NetAmount"] > 0){
            this.customerExtraResult.push(this.customerExtra[i]);
          }else{

          }
        }         
        this.setAcumulativeBalance();
      })
      .catch(this.handleError);
      this.getInstallments();
  }

  getExtras() {
    var temp = this.serverService.Settings.V3Settings.SalesType;
    if(this.serverService.Settings.V3Settings.SalesType==1 || this.serverService.Settings.V3Settings.SalesType==4) this.userData.ProcName = "G3_GetCustomerExtraWholeSale";
    else if(temp==3) this.userData.ProcName = "G3_GetCustomerExtraRetailSale";
    else if(temp==5) this.userData.ProcName = "G3_GetCustomerExtraRetailSale";  
    else if(temp==6) this.userData.ProcName = "G3_GetCustomerExtraRetailSale";  
    this.presentLoading();    
    this.userData.Parameters[0].Value = this.currAccCode;
    this.serverService.getAny(this.userData)
      .then(res => {
        this.dismissLoading();
        //console.log(this.userData.ProcName);                
        this.customerExtra = res;
        console.log('customerExtra', this.customerExtra);
        if(this.customerExtra.length > 0){        
          if(this.customerExtra[0]["NetAmount"]){          
          for(let i = 0; i < this.customerExtra.length; i++){          
            if(this.customerExtra[i]["NetAmount"] > 0){
              console.log(this.customerExtra[i]["NetAmount"]);
              this.tempTotal += this.customerExtra[i]["NetAmount"];
              this.customerExtraResult.push(this.customerExtra[i]);
            }
          }
          }else{
          this.checkNetAmount = true;
          for(let i = 0; i < this.customerExtra.length; i++){  
            if(this.customerExtra[i]["ItemCode"] != "" && this.customerExtra[i]["Doc_NetAmount"] > 0){
              this.tempTotal += this.customerExtra[i]["Doc_NetAmount"];
              this.customerExtraResult.push(this.customerExtra[i]);
            }                        
          }   
          console.log(this.customerExtraResult);
        }         
        this.setAcumulativeBalance();
      }
      })
      .catch(this.handleError);
  }

  refreshFilter(){
    this.tempTotal = 0;
    this.customerExtraResult = [];
    this.customerExtra = [];
    this.userData.Parameters[2].Value = "";    
    this.userData.Parameters[3].Value = ""; 
    this.reset();    
    this.getExtras();
    this.getInstallments();
  }

  setAcumulativeBalance() {
    for (let item of this.customerExtra) {
      this.balance_loc = Number((this.balance_loc + item.Loc_Balance).toFixed(2));
      item.Loc_BalanceAccumulative = this.balance_loc;

      this.balance_doc = Number((this.balance_doc + item.Doc_Balance).toFixed(2));
      item.Doc_BalanceAccumulative = this.balance_doc;
      //console.log('1',this.balance_loc);
      //console.log('2',this.balance_doc);
      

      if (this.serverService.Settings.V3Settings.IsExchange) {
        this.total = item.Doc_BalanceAccumulative;
        this.total_debit = Number((this.total_debit + item.Doc_Debit).toFixed(2));
        this.total_credit = Number((this.total_credit + item.Doc_Credit).toFixed(2));
        //console.log('3',this.total);
        //console.log('4',this.total_debit);
        //console.log('5',this.total_credit);

      } else {
        this.total = item.Loc_BalanceAccumulative;
        this.total_debit = Number((this.total_debit + item.Loc_Debit).toFixed(2));
        this.total_credit = Number((this.total_credit + item.Loc_Credit).toFixed(2));
        //console.log('6',this.total);
        //console.log('7',this.total_debit);
        //console.log('8',this.total_credit);
      }
    }
  }

  goToOrderDetails(item: any) {
    this.tempNet = this.checkNetAmount;
    console.log(this.tempNet);
    let filtered_installments = []
    filtered_installments.push(item);
    let modal;
    modal = this.modalCtrl.create(InvoiceDetailPage, { extredetail: filtered_installments, type: this.tempNet });
    modal.present();   
    modal.onDidDismiss(data => {             
      console.log(data);
    }); 
  }

  handleError(error: any) {
    console.error('An error occurred', error);
    this.dismissLoading();
    /* this.translateService.get('ALERT_ERROR_TITLE_TEXT').subscribe((value: string) => {
      this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], error.ExceptionMessage);
    }); */
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
