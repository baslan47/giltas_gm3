import {Component} from '@angular/core';
import {NavController, MenuController, NavParams, ModalController, AlertController} from 'ionic-angular';
import {ServerService} from "../../classes/server.service";
import {SettingsPage} from '../settings/settings';
import {Storage} from '@ionic/storage';
import {DomSanitizer} from "@angular/platform-browser";
import {ProductMenuPage} from "../product-menu/product-menu";
import {CustomersPage} from "../customers/customers";
import {CampaignMainPage} from "../campaign-main/campaign-main";
import {ReportsPage} from "../reports/reports";
import { DistributionPage } from '../distribution/distribution';
import { ProposalTypePage } from '../proposal-type/proposal-type';
import { ProposalPage } from '../proposal/proposal';
import { AwsTestPage } from '../aws-test/aws-test';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private rootPage;
  private ModuleLicense: string;  
  private settingsPage;
  private userData = {"ProcName": "G3_GetColors", "Parameters": [{"Name": "Language", "Value": "TR"}]};

  constructor(public navCtrl: NavController, public serverService: ServerService,
              public navParams: NavParams, public storage: Storage, private sanitizer: DomSanitizer,
              public menu: MenuController, public modalCtrl: ModalController, public alertController: AlertController) {
    this.rootPage = HomePage;
    this.settingsPage = SettingsPage;
    this.ModuleLicense = this.serverService.Settings.G3License.Modules;    
    console.log(this.ModuleLicense);    
  }

  ionViewWillEnter() {
    this.getSettings();
  }

  ionViewDidEnter() {
   this.getColors();
   console.log(this.serverService.Settings.V3Settings.OfficeCode);
  }

  presentAlert() {
    const alert = this.alertController.create({
      title: 'UYARI',      
      message: 'Yakında Hizmete Açılacaktır.',
      buttons: ['OK']
    });
    alert.present();
  }

  presentAccountAlert() {
    const alert = this.alertController.create({
      title: 'UYARI',      
      message: 'Lisansınız bu işlem için aktif değildir.',
      buttons: ['OK']
    });
    alert.present();
  }

  getSettings() {
    this.storage.get('settings').then(data => {
        if (data) this.serverService.Settings = data;
        else this.goToSettingPage();
      }, error => console.error(error));
  }

  getColors() {
    this.serverService.getAny(this.userData).then(res => {
      console.log('colors');
      console.log(res);
      this.serverService.Colors = res;
    }).catch(this.handleError);
  }

  handleError(error: any) {
    console.error('An error occurred', error);
  }

  goToSettingPage() {
    if (this.serverService.Settings == null) {
      const modal = this.modalCtrl.create(SettingsPage);
      modal.present();
    }
  }

  goToProductMenuPage() {
    if(this.ModuleLicense.charAt(0) != "0"){
      this.navCtrl.push(ProductMenuPage);
    }else{
      this.presentAccountAlert();
    }
  }

  goToAwsPage() {
    this.navCtrl.push(AwsTestPage);
  }  

  goToCustomersPage() {
    if(this.ModuleLicense.charAt(1) != "0"){
      this.navCtrl.push(CustomersPage, {nextPage: 'customerdetail'});
    }else{
      this.presentAccountAlert();
    }    
  }  

  goToProposalTypePage(){  
    let modal;
    modal = this.modalCtrl.create(ProposalTypePage, {text: 'Sipariş Kategori Tipi Seçiniz'});
    modal.present();
    modal.onDidDismiss(data => {                
          if(data.type != false){
            this.serverService.Settings.V3Settings.SalesType = data.type;
            this.navCtrl.push(ProposalPage);
          }else{

          }          
    });
  }


  goToReportsPage() {
    if(this.ModuleLicense.charAt(3) != "0"){
      this.navCtrl.push(ReportsPage);
    }else{
      this.presentAccountAlert();
    }     
  }

  goToCampaignMainPage(){
    if(this.ModuleLicense.charAt(4) != "0"){
      this.navCtrl.push(CampaignMainPage);
    }else{
      this.presentAccountAlert();
    }     
  }

  goToProposalPage(){
    if(this.ModuleLicense.charAt(2) != "0"){
      this.navCtrl.push(ProposalPage);
    }else{
      this.presentAccountAlert();
    }     
  }

  goToDistributionPage(){
    if(this.ModuleLicense.charAt(5) != "0"){
      this.navCtrl.push(DistributionPage);
    }else{
      this.presentAccountAlert();
    }     
  }
}
