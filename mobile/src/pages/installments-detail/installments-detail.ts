import {Component} from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import {ServerService} from "../../classes/server.service";

@Component({
  selector: 'page-installments-detail',
  templateUrl: 'installments-detail.html',
})
export class InstallmentsDetailPage {
  installments: any;  

  constructor(public navCtrl: NavController, public navParams: NavParams, public serverService: ServerService, public viewCtrl: ViewController) {
    this.installments = this.navParams.get('installments');
  }

  /*goToOrderDetails(item: any) {
    this.navCtrl.push(OrderDetailPage, {orderNumber: item.RefNumber});
  }*/

  closeModalCancel(){
    this.viewCtrl.dismiss({result: false});
  }
}
