import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {InstallmentsDetailPage} from "../installments-detail/installments-detail";

@Component({
  selector: 'page-installments',
  templateUrl: 'installments.html',
})
export class InstallmentsPage {
  installments_summary: any;
  installments: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.installments_summary = this.navParams.get('installments_summary');
    this.installments = this.navParams.get('installments');
  }

  goToInstallmentDetails(item: any) {
    let filtered_installments = [];
    for (let installment of this.installments) {
      if (installment.MonthYear == item.DueDate) {
        filtered_installments.push(installment);
      }
    }
    this.navCtrl.push(InstallmentsDetailPage, {installments: filtered_installments});
  }
}
