import { Component } from '@angular/core';
import {AlertController, LoadingController, Loading, NavController, NavParams} from 'ionic-angular';
import {ServerService} from "../../classes/server.service";
import {TranslateService} from "@ngx-translate/core";
import {Product} from "../../models/models";

@Component({
  selector: 'page-inventory',
  templateUrl: 'inventory.html',
})
export class InventoryPage {
  productInventory: any;
  filteredproductInventory: any;
  loading: Loading;
  public myInput: string = '';
  public items: Array<Product>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private serverService: ServerService,
              private alertCtrl: AlertController, private loadingCtrl: LoadingController,
              public translateService: TranslateService) {
    this.productInventory = this.navParams.get('productInventory');
    this.filteredproductInventory = this.productInventory;
  }

  onInput(event: any) {
    console.log('inventoryPage')
    if (this.myInput.length >= 3) {
      //TODO setToStorage it in settings
    }
  }

  onCancel(event: any) {
    this.doFilter();
  }

  tr2enLowercase(str: string) {
    var letters = {
      "İ": "I",
      "ı": "i",
      "Ş": "S",
      "ş": "s",
      "Ğ": "G",
      "ğ": "g",
      "Ü": "U",
      "ü": "u",
      "Ö": "O",
      "ö": "o",
      "Ç": "C",
      "ç": "c"
    };
    return str.replace(/(([İıŞşĞğÜüÖöÇç]))/g, function (letter) {
      return letters[letter];
    }).toLowerCase();
  }

  doFilter() {
    this.presentLoading();
    var regex = new RegExp(this.tr2enLowercase(this.myInput), "i");
    if (this.myInput != "") {
      this.filteredproductInventory = new Array();
      for (let inventory of this.productInventory) {
        if (inventory.StoreName) {
          if (this.tr2enLowercase(inventory.StoreName).search(regex) >= 0) {
            this.filteredproductInventory.push(inventory);
            continue;
          }
        }
        if (inventory.ItemDim1Code) {
          if (this.tr2enLowercase(inventory.ItemDim1Code).search(regex) >= 0) {
            this.filteredproductInventory.push(inventory);
          }
        }
      }
    }
    else {
      this.filteredproductInventory = this.productInventory;
    }
    this.loading.dismiss();
  }

  handleError(error: any) {
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string[]) => {
      this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
    if (this.loading)
      this.loading.dismiss();
  }

  public showAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  public   presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  sendMessageWithEnterKey(event: any) {
    event.preventDefault();
    this.doFilter();
  }

}
