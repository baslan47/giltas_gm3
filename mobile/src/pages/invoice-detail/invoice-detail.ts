import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, LoadingController, Loading, ViewController } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'page-invoice-detail',
  templateUrl: 'invoice-detail.html',
})
export class InvoiceDetailPage {

  loading: Loading;
  extredetail: any;  
  type: any;  
  convertedDate: any;
  paymentInfo: any;

  userPaymentInfo ={
    "ProcName": "G3_GetCustomerPaymentInformation",
    "Parameters": [{ "Name": "DocumentNo", "Value": "" }]
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public serverService: ServerService,
    public alertCtrl: AlertController, public loadingCtrl: LoadingController, public viewCtrl: ViewController, public translateService: TranslateService) {
    this.extredetail = this.navParams.get('extredetail');
    this.type = this.navParams.get('type');
    this.convertedDate = this.serverService.convertDate(this.extredetail[0].DocumentDate);
    this.getPaymentInfo();
    console.log(this.convertedDate)
    console.log(this.extredetail);
    console.log(this.type);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvoiceDetailPage');
  }
  getPaymentInfo(){
    
    this.userPaymentInfo.Parameters[0].Value = this.extredetail[0].RefNumber;
    this.serverService.getAny(this.userPaymentInfo)
      .then(res =>{
        this.paymentInfo = res;
        console.log(res)
      })
  }

  closeModalCancel(){
    this.viewCtrl.dismiss({result: false});
  }  
}

