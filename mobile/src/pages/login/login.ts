import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, ModalController, Loading } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { ServerService } from "../../classes/server.service";
import { Storage } from '@ionic/storage';
import { SettingsPage } from "../settings/settings";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { Settings, Integrator, G3Settings, V3Settings } from '../../models/models';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loading: Loading;
  login: FormGroup;
  success: string = 'Connection Created Successfully';
  loginData = { "ProcName": "G3_Login", "Parameters": [{ "Name": "Username", "Value": "" }] };
  settings: Settings = new Settings();
  account: { username: string, password: string } = {
    username: '',
    password: ''
  };

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public loadingCtrl: LoadingController,
    public serverService: ServerService, public storage: Storage, public modalCtrl: ModalController,
    public translateService: TranslateService) {
    this.login = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
    //this.getSettings();
  }

  getSettings() {
    this.storage.get('settings').then(data => {//yapılacak*****(isterlerse)
      if (data)
        this.serverService.Settings = data[0];
      else if (!this.serverService.Settings) {
        this.goToSettingPage();
      }
    }, error => console.error(error));
  }

  goToSettingPage() {
    const modal = this.modalCtrl.create(SettingsPage);
    modal.present();
    modal.onDidDismiss(data => {
      this.getSettings();
    });
  }

  Login_2() {
    console.log('Login With entgrator');
    if (this.login.valid) {
      this.presentLoading();
      this.serverService.connect(this.serverService.Settings.Integrator.Url, this.serverService.Settings.Integrator).then((res: any) => {
        this.dismissLoading();
        if (res.Status == this.success) {
          this.loginData.Parameters[0].Value = this.login.controls["username"].value;
          this.serverService.getAny(this.loginData).then(res => {
            console.log(res);
            this.dismissLoading();
            if (res.length > 0) {
              if (this.checkExpiryDate(res[0].ExpiryDate)) {
                if (this.login.controls["password"].value == atob(res[0].Password)) {
                  //this.setToStorage();
                  this.navCtrl.push(TabsPage);
                } else {
                  this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'LOGIN_ALERT_ERROR_MESSAGE_INVALID_USERNAME_OR_PASSWORD']).subscribe((value: string[]) => {
                    this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], value['LOGIN_ALERT_ERROR_MESSAGE_INVALID_USERNAME_OR_PASSWORD']);
                  });
                }
              } else {
                this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'LOGIN_ALERT_ERROR_MESSAGE_EXIRED_DATE']).subscribe((value: string[]) => {
                  this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], value['LOGIN_ALERT_ERROR_MESSAGE_EXIRED_DATE']);
                });
              }
            } else {
              this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'LOGIN_ALERT_ERROR_MESSAGE_INVALID_USERNAME']).subscribe((value: string[]) => {
                this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], value['LOGIN_ALERT_ERROR_MESSAGE_INVALID_USERNAME']);
              });
            }
          }).catch(this.handleError);
        } else {
          this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'LOGIN_ALERT_ERROR_MESSAGE_INVALID_INTEGRATOR_USERNAME_OR_PASSWORD']).subscribe((value: string[]) => {
            this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], value['LOGIN_ALERT_ERROR_MESSAGE_INVALID_INTEGRATOR_USERNAME_OR_PASSWORD']);
          });
        }
      }, (error) => {
        console.log(error);
        this.dismissLoading();
        this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'LOGIN_ALERT_ERROR_MESSAGE_CHECK_INTEGRATOR_CONNECTION_INFORMATIONS']).subscribe((value: string[]) => {
          this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], value['LOGIN_ALERT_ERROR_MESSAGE_CHECK_INTEGRATOR_CONNECTION_INFORMATIONS']);
        });
      }).catch(this.handleError);
      //}
    }
  }
  //login olacajk
  //setting olmayacak -- gizle
  Login() {
    console.log('Login With Giltaş');
    this.presentLoading();
    this.account.username = this.login.controls['username'].value;
    this.account.password = this.login.controls['password'].value;

    this.serverService.login(this.account).then((res: any) => {
      console.log("res0", res);
      if (res.success) {
        this.serverService.Settings = (res.Settings);
        console.log("Settings", this.serverService.Settings);
        this.serverService.Settings.V3Settings.SalespersonName = res['personel']['name'];
        this.serverService.Settings.V3Settings.SalespersonNameCode = res['personel']['username'];
        this.serverService.Settings.V3Settings.SalespersonPassword = res['personel']['password'];
        this.serverService.Settings.V3Settings.SalespersonEmail = res['personel']['email'];
        console.log(this.serverService.Settings.V3Settings.SalespersonName);
        console.log(this.serverService.Settings.V3Settings.SalespersonNameCode);
        this.serverService.connect(this.serverService.Settings.Integrator.Url, this.serverService.Settings.Integrator).then((res: any) => {
          console.log('serverConnect',res);
          this.dismissLoading();
          if (res.Status == this.success) {

            this.navCtrl.push(TabsPage);
          } else {
            this.dismissLoading();
            this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'LOGIN_ALERT_ERROR_MESSAGE_INVALID_INTEGRATOR_USERNAME_OR_PASSWORD']).subscribe((value: string[]) => {
              this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], value['LOGIN_ALERT_ERROR_MESSAGE_INVALID_INTEGRATOR_USERNAME_OR_PASSWORD']);
            });
          }
        }, (error) => {
          // console.log(error);
          this.dismissLoading();
          this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'LOGIN_ALERT_ERROR_MESSAGE_CHECK_INTEGRATOR_CONNECTION_INFORMATIONS']).subscribe((value: string[]) => {
            this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], value['LOGIN_ALERT_ERROR_MESSAGE_CHECK_INTEGRATOR_CONNECTION_INFORMATIONS']);
          });
        }).catch(this.handleError);
        //}

      }
      else {this.dismissLoading();
        this.translateService.get(['ALERT_ERROR_TITLE_TEXT']).subscribe((value: string[]) => {
          this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], res.error);
        });
        //this.dismissLoading();
      }
      // BK Continue convert josn
    }, (error) => {
      console.log(error);
      this.dismissLoading();
    });
    //this.dismissLoading();
  }

  checkExpiryDate(expiryDate: string) {
    // return true;
    // date: 30.07.2018 = unix: 1532908800
    // base64 utf-8 : MTUzMjkwODgwMA==

    /* */
    let now: Date = new Date();
    console.log('now: ' + now);

    let ds: string = atob(expiryDate);
    let exDate: Date = new Date(Number(ds) * 1000);
    console.log('expiryDate: ' + exDate);
    if (now > exDate)
      return false;
    else
      return true;

  }

  presentAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  autoLogin() {
    this.login.controls["username"].setValue("gm1");
    this.login.controls["password"].setValue("1");
    this.Login();
  }

  handleError(error: any) {
    this.dismissLoading();
    console.error('An error occurred', error);
    this.translateService.get('ALERT_ERROR_TITLE_TEXT').subscribe((value: string) => {
      this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], error.ExceptionMessage);
    });
  }
}
