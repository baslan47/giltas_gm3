import { Component } from '@angular/core';
import {
  NavController,
  NavParams,
  ViewController,
  Loading,
  LoadingController,
  AlertController
} from 'ionic-angular';
import { TranslateService } from "@ngx-translate/core";
import { ServerService } from "../../classes/server.service";
import 'rxjs/Rx';

@Component({
  selector: 'page-modal-addaddress',
  templateUrl: 'modal-addaddress.html',
})
export class ModalAddaddressPage {

  loading: Loading;

  detail_world: any = 'TR'
  detail_Address: any;
  detail_AddressTypeCode: any;
  detail_CountryCode: any;
  detail_CountryCodenew: any;
  detail_StateCode: any = 'TR.EG';
  detail_CityCode: any = 'TR.35';
  detail_DistrictCode: any;
  checkdatabase: number = 0;
  detail_BuildingNum: number = 0;
  detail_FloorNum: number = 0;
  detail_DoorNum: number = 0;
  stateList: any[] = [];
  cityList: any[] = [];
  districtList: any[] = [];
  worldList: any[] = [];
  placeholder: string = 'TR';

  addressDescriptions: any[] = [
    { "Code": "1", "Value": "Ev Adresi" },
    { "Code": "2", "Value": "İş Adresi" },
    { "Code": "3", "Value": "Merkez Ofis" },
    { "Code": "4", "Value": "Eski Ev Adresi" }
  ];
  addrData = { "ProcName": "", "Parameters": [{ "Name": "Parent", "Value": "" }, { "Name": "Language", "Value": "TR" }] };
  GetData = { "ProcName": "G3_GetWorldCountries"};

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public loadingCtrl: LoadingController, public serverService: ServerService,
    public translateService: TranslateService, public alertCtrl: AlertController) {
      //JS if(this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2'){
      if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
        this.checkdatabase = 1;
      }  
  }

  //G3_GetWorldCountries
  ngOnInit() {
    if(this.checkdatabase == 1){
      this.placeholder = '';
      this.detail_world = '';
      this.getWorldCountriese();
    }    
    this.getStates();
    this.getCities(this.detail_StateCode);
    this.getDistricts(this.detail_CityCode);
    this.checkDistricts(this.serverService.Settings.V3Settings.OfficeCode);
  }

  portChange(event) {
    this.detail_world = event.value['Code'];    
    console.log('value:', event.value['Code']);
    console.log(this.detail_world);
    if(this.detail_world == 'TR') this.getStates();
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalAddaddressPage');
  }

  closeModalCancel() {
    this.viewCtrl.dismiss({ result: false });
  }


  getWorldCountriese(){
    this.GetData.ProcName = "G3_GetWorldCountries";
    this.serverService.getAny(this.GetData).then(res => {     
      console.log(res);
      this.worldList = res;
    })
  }

  returnAddressData() {  
    /* TODO JS
    if(this.detail_world != 'TR'){
      if (!this.detail_AddressTypeCode || !this.detail_CountryCodenew) {
        this.presentAlert("Uyarı", "Lütfen Zorunlu Alanları Doldurunuz.");
        return;
      }
    }else{
      if (!this.detail_AddressTypeCode || !this.detail_StateCode || !this.detail_CityCode || !this.detail_DistrictCode) {
        this.presentAlert("Uyarı", "Lütfen Zorunlu Alanları Doldurunuz.");
        return;
      }
    }
    */
       
    if (!this.detail_Address) this.detail_Address = '';

    this.detail_CountryCode = this.detail_world;

    if(this.detail_world != 'TR'){
      this.viewCtrl.dismiss({
        txtDAddress: this.detail_Address,
        txtDaddtype: this.detail_AddressTypeCode,
        txtDCoCode: this.detail_CountryCode,  
        txtDSCode: this.detail_world,
        txtDCiCode: this.detail_world,
        txtDDCode: this.detail_world,
        txtDBNum: this.detail_BuildingNum,
        txtDFNum: this.detail_FloorNum,
        txtDDNum: this.detail_DoorNum
      })
    }else{
      this.viewCtrl.dismiss({
        txtDAddress: this.detail_Address,
        txtDaddtype: this.detail_AddressTypeCode,
        txtDCoCode: this.detail_CountryCode,  
        txtDSCode: this.detail_StateCode,
        txtDCiCode: this.detail_CityCode,
        txtDDCode: this.detail_DistrictCode,
        txtDBNum: this.detail_BuildingNum,
        txtDFNum: this.detail_FloorNum,
        txtDDNum: this.detail_DoorNum
      })
    }
    
  }

  getStates() {
    this.addrData.ProcName = "G3_GetStates";
    this.addrData.Parameters[0].Value = 'TR';
    this.serverService.getAny(this.addrData)
      .then(res => {
        this.stateList = res;
        // this.cityList = [];
        // this.districtList = [];
        console.log(res);
      }).catch(this.handleError);
  }
  getCities(code) {
    this.addrData.ProcName = "G3_GetCities";
    this.addrData.Parameters[0].Value = code
    this.serverService.getAny(this.addrData)
      .then(res => {
        this.cityList = res;

        // this.districtList = [];
        console.log(res);
      })
      .catch(this.handleError);
  }
  getDistricts(code) {
    this.addrData.ProcName = "G3_GetDistricts";
    this.addrData.Parameters[0].Value = code;
    this.serverService.getAny(this.addrData)
      .then(res => {
        this.districtList = res;
        console.log(res);
      })
      .catch(this.handleError);
  }
  checkDistricts(officeCode) {
    if (officeCode == 's01') this.detail_DistrictCode = 'TR.03504'
    else if (officeCode == 's02') this.detail_DistrictCode = 'TR.03504'//bornova
    else if (officeCode == 's03') this.detail_DistrictCode = 'TR.03501'//karşıyaka
    else if (officeCode == 's04') this.detail_DistrictCode = 'TR.03520'//üçkuyular
    else if (officeCode == 's05') this.detail_DistrictCode = 'TR.03520'//şirinyer
    else if (officeCode == 's06') this.detail_DistrictCode = 'TR.03500'//manisa
    else if (officeCode == 's07') this.detail_DistrictCode = 'TR.03500'//nokta
    else if (officeCode == 's08') this.detail_DistrictCode = 'TR.03500'//bozyaka
    else if (officeCode == 's11') this.detail_DistrictCode = 'TR.03520'//forbes
    else if (officeCode == 's12') this.detail_DistrictCode = 'TR.03500'//bayraklı
    else if (officeCode == 's13') this.detail_DistrictCode = 'TR.03526'//gaziemir
    else if (officeCode == 's14') this.detail_DistrictCode = 'TR.03527'//narlıdere
    else if (officeCode == 's15') this.detail_DistrictCode = 'TR.03500'//turgutlu
    else if (officeCode == 's16') this.detail_DistrictCode = 'TR.03517'//torbalı
    else if (officeCode == 's17') this.detail_DistrictCode = 'TR.03525'//çiğli
    else if (officeCode == 's18') this.detail_DistrictCode = 'TR.03513'//ödemiş
    else if (officeCode == 's19') this.detail_DistrictCode = 'TR.03500'//aydın
    else if (officeCode == 's20') this.detail_DistrictCode = 'TR.03500'//denizli
    else if (officeCode == 's21') this.detail_DistrictCode = 'TR.03500'//salihli
    else if (officeCode == 's22') this.detail_DistrictCode = 'TR.03500'//çamdibi
    else if (officeCode == 's24') this.detail_DistrictCode = 'TR.03500'//balıkesir

    console.log(officeCode, this.detail_DistrictCode)
  }

  onStateChange() {
    let code = this.detail_StateCode;
    this.getCities(code);

  }
  onCityChange() {
    let code = this.detail_CityCode;
    this.getDistricts(code)
  }

  handleError(error: any) {
    console.error('An error occurred', error);
    this.dismissLoading();
  }
  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }
  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
  presentAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

}
