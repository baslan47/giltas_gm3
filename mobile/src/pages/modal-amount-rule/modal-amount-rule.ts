import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-modal-amount-rule',
  templateUrl: 'modal-amount-rule.html',
})
export class ModalAmountRulePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalAmountRulePage');
  }
  closeModal(){
    this.viewCtrl.dismiss({result: false});
  }

}
