import { Component } from '@angular/core';
import { NavController, Loading, LoadingController, NavParams, ViewController, AlertController } from 'ionic-angular';
import {ServerService} from "../../classes/server.service";
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'page-modal-attribute-type17',
  templateUrl: 'modal-attribute-type17.html',
})
export class ModalAttributeType17Page {

  jobName: string;
  productcode: string;  
  productattributeCode: string;
  loading: Loading;  
  attributeType17List = [];
  public myInput: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
              public loadingCtrl: LoadingController, public serverService: ServerService, public alertCtrl: AlertController,
              public translateService: TranslateService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalAttributeType17Page');
  }

  closeModal(){
    this.viewCtrl.dismiss({txtPN: this.jobName});
  }

  closeModalCancel(){
    this.viewCtrl.dismiss({result: false});
  }
  ngOnInit() {
    //this.presentLoading();
    //this.getAttributeType17();
    
  }

  onInput(event: any) {
    //this.MarkaListTmp = this.MarkaList.filter( item => {return item.AttributeTypeDescription.toLowerCase().includes(this.myInput);});  
    this.getAttributeType17();
    return this.attributeType17List;
  }

  onCancel(){
    // this.MarkaListTmp = this.MarkaList;
    console.log('cancel')
  }
  getAttributeType17(){
    this.presentLoading();
    let addData = { "ProcName": "G3_GetAttributeType17", "Parameters": [{ "Name": "Parent", "Value": "" }, { "Name": "Code", "Value": "" }] }
    addData.Parameters[0].Value = this.myInput;
    addData.Parameters[1].Value = ""
    this.serverService.getAny(addData)
        .then(res => {
            this.attributeType17List = res;
            this.dismissLoading();
            console.log('getAttributeType17',this.attributeType17List);
        })
}
  

  goToCustomerPage(att: any) {
    this.viewCtrl.dismiss({
      txtAttCode17: att.AttributeCode,
      txtAttDesc17: att.AttributeDescription
    })
  }


  handleError(error: any) {
    this.dismissLoading();
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string[]) => {
      this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  showAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

}
