import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-modal-campaign-group',
  templateUrl: 'modal-campaign-group.html',
})
export class ModalCampaignGroupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalCampaignGroupPage');
  }
  closeModal(){
    this.viewCtrl.dismiss({result: false});
  }

}
