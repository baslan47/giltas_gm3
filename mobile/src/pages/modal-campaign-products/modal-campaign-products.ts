import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";

@Component({
  selector: 'page-modal-campaign-products',
  templateUrl: 'modal-campaign-products.html',
})
export class ModalCampaignProductsPage {

  itemData = { "ProcName": "" };
  deleteItems = {
    "ProcName": "G3AI_DeleteItems", "Parameters": [
      { "Name": "ItemCode", "Value": "" },
      { "Name": "ColorCode", "Value": "" }, { "Name": "ItemDim1Code", "Value": "" },
     ]
  };

  itemArray: any[] = [];

 

  constructor(public navCtrl: NavController, public navParams: NavParams,  public viewCtrl: ViewController,
    public serverService: ServerService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalCampaignProductsPage');
  }
  closeModal(){
    this.viewCtrl.dismiss({result: false});
  }

  ngOnInit(){
    this.getOfferItems();
  }

  getOfferItems() {
    this.itemData.ProcName = 'G3AI_GetOfferItems';
    this.serverService.getAny(this.itemData).then(res => {
      console.log('G3AI_GetOfferItems', res);
      this.itemArray = res;
    })
  }

  deleteItem(index){
    //console.log(this.itemArray[index])
     this.deleteItems.Parameters[0].Value = this.itemArray[index].ItemCode;
     this.deleteItems.Parameters[1].Value = this.itemArray[index].ColorCode? this.itemArray[index].ColorCode: '';
     this.deleteItems.Parameters[2].Value = this.itemArray[index].ItemDim1Code? this.itemArray[index].ItemDim1Code: '';
     this.serverService.getAny(this.deleteItems);
     this.itemArray.splice(index,1);
    console.log(this.itemArray);
  }

  saveItems(){
    this.viewCtrl.dismiss({
      newItemArray: [] = this.itemArray
    })
  }

}
