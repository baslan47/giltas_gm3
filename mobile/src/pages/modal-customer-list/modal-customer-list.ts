import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-modal-customer-list',
  templateUrl: 'modal-customer-list.html',
})
export class ModalCustomerListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalCustomerListPage');
  }
  closeModal(){
    this.viewCtrl.dismiss({result: false});
  }

}
