import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ServerService } from '../../classes/server.service';

@Component({
  selector: 'page-modal-customers',
  templateUrl: 'modal-customers.html',
})
export class ModalCustomersPage {

  customername: string;
  customerlastname: string;
  customertc: string;  
  customertel: string;
  customercode: string;  
  customerfirstlastname: string;  
  checkdatabase: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public serverService: ServerService) {
      //JS if(this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2'){
      if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
        this.checkdatabase = 1;
      } 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalCustomersPage');
  }

  closeModal() {
    this.viewCtrl.dismiss({
      txtCN: this.customername, txtCLN: this.customerlastname,
      txtCTC: this.customertc, txtCT: this.customertel, 
      txtCC: this.customercode, txtCFLN: this.customerfirstlastname      
    });
  }

  closeModalCancel() {
    this.viewCtrl.dismiss({ result: false });
  }
}
