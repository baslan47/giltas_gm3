import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';



@Component({
  selector: 'page-modal-filter-order',
  templateUrl: 'modal-filter-order.html',
})
export class ModalFilterOrderPage {

  startDate: string;  
  endDate: string;
  orderNumber: string;
  customerName: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalFilterOrderPage');
  }

  closeModal(){
    this.viewCtrl.dismiss({txtSD: this.startDate, txtED: this.endDate, txtON: this.orderNumber, txtCS: this.customerName});
  }

  closeModalCancel(){
    this.viewCtrl.dismiss({result: false});
  }

}
