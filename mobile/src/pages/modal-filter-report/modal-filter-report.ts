import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-modal-filter-report',
  templateUrl: 'modal-filter-report.html',
})
export class ModalFilterReportPage {

  startDate: string;  
  endDate: string;  
  minTotal: number;
  maxTotal: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalFilterReportPage');
  }

  closeModal(){
    this.viewCtrl.dismiss({txtSD: this.startDate, txtED: this.endDate, txtMinT: this.minTotal, txtMaxT: this.maxTotal});
  }

  closeModalCancel(){
    this.viewCtrl.dismiss({result: false});
  }

}
