import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-modal-product-list',
  templateUrl: 'modal-product-list.html',
})
export class ModalProductListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalProductListPage');
  }
  closeModal(){
    this.viewCtrl.dismiss({result: false});
  }

}
