import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';


@Component({
  selector: 'page-modal-productfilter',
  templateUrl: 'modal-productfilter.html',
})
export class ModalProductfilterPage {

  productname: string;
  productcode: string;  
  productattributeCode: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalProductfilterPage');
  }

  closeModal(){
    this.viewCtrl.dismiss({txtPN: this.productname, txtPC: this.productcode});
  }

  closeModalCancel(){
    this.viewCtrl.dismiss({result: false});
  }

}
