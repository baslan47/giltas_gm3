import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-modal-time-period',
  templateUrl: 'modal-time-period.html',
})

export class ModalTimePeriodPage {

  startDate: string = "";
  endDate: string = "";
  timePeriodCode: string = "";
  timePeriodName: string = "";

  timePeriodData = {
    "ProcName": "G3AI_InsertTimePeriod", "Parameters": [
      { "Name": "TimePeriodCode", "Value": "" }, { "Name": "TimePeriodDescription", "Value": "" },
      { "Name": "StartDate", "Value": "" }, { "Name": "EndDate", "Value": "" },
      { "Name": "LangCode", "Value": "" }
    ]
  }


  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public serverService: ServerService,
    public alertCtrl: AlertController, public translateService: TranslateService) {
    var startDate_to_parse = new Date();
    var year = startDate_to_parse.getFullYear().toString();
    var month = (startDate_to_parse.getMonth() + 1).toString();
    var day = startDate_to_parse.getDate().toString();
    this.startDate = day + '/' + month + '/' + year;
    this.parseStartDate();

    var endDate_to_parse = new Date();
    var year = endDate_to_parse.getFullYear().toString();
    var month = (endDate_to_parse.getMonth() + 1).toString();
    var day = endDate_to_parse.getDate().toString();
    this.endDate = day + '/' + month + '/' + year;
    this.parseEndDate();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalTimePeriodPage');
  }
  parseStartDate() {
    var firstStape, secondStape;
    if (this.startDate.substring(5, 6) === "0") {
      firstStape = this.startDate.substring(0, 5);
      if (this.startDate.substring(8, 9) === "0") {
        firstStape = firstStape + this.startDate.substring(6, 8);
        secondStape = this.startDate.substring(9);
      }
      else {
        secondStape = this.startDate.substring(6);
      }
      this.startDate = firstStape + secondStape;
    }
    this.startDate = this.startDate.replace('-', '/');
  }
  parseEndDate() {
    var firstStape, secondStape;
    if (this.endDate.substring(5, 6) === "0") {
      firstStape = this.endDate.substring(0, 5);
      if (this.endDate.substring(8, 9) === "0") {
        firstStape = firstStape + this.endDate.substring(6, 8);
        secondStape = this.endDate.substring(9);
      }
      else {
        secondStape = this.endDate.substring(6);
      }
      this.endDate = firstStape + secondStape;
    }
    this.endDate = this.endDate.replace('-', '/');
  }

  addTimePeriod() {
    if (this.timePeriodCode == "" || this.timePeriodName == "" || this.startDate == "" || this.endDate == "") {
      this.presentAlert("Uyarı", "Lütfen Tüm Alanları Doldurunuz.");
      return 0;
    }else{
      this.timePeriodData.Parameters[0].Value = this.timePeriodCode;
      this.timePeriodData.Parameters[1].Value = this.timePeriodName;
      this.timePeriodData.Parameters[2].Value = this.startDate;
      this.timePeriodData.Parameters[3].Value = this.endDate;
      this.timePeriodData.Parameters[4].Value = 'TR';
      console.log(this.timePeriodData);
      this.serverService.getAny(this.timePeriodData);
      this.viewCtrl.dismiss({ result: true });
    }
  }

  presentAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }


  closeModal() {
    this.viewCtrl.dismiss({ result: false });
  }

}
