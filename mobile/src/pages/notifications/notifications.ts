import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OpenOrdersPage } from '../open-orders/open-orders';
import { ShipmentsPage } from '../shipments/shipments';
import { ServerService } from '../../classes/server.service';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public serverService: ServerService) {
  }

  ionViewDidLoad() {
    console.log(this.serverService);
    console.log('ionViewDidLoad NotificationsPage');
  }

  goToOpenOrders() {
    this.navCtrl.push(OpenOrdersPage);  
  }

  goToShipments(){
    this.navCtrl.push(ShipmentsPage);
  }

}
