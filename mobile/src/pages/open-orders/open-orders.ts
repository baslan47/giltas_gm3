import { Component } from '@angular/core';
import { NavController, NavParams, Loading, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { ServerService } from '../../classes/server.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-open-orders',
  templateUrl: 'open-orders.html',
})
export class OpenOrdersPage {

  userData = {
    "ProcName": "G3_GetOpenOrders",
    "Parameters": [{ "Name": "StoreCode", "Value": "" }]
  };
  loading: Loading;
  list: any;

  transferData = {
    "ModelType": 59,
    "CompanyCode": "1",
    "IsCompleted": true,
    "IsLocked": false,
    "IsReturn": false,
    "IsOrderBase": true,
    "IsTransferApproved": false,
    "ShippingPostalAddressID": "",
    "OfficeCode": "M02",
    "StoreCode": "M02",
    "WarehouseCode": "M02",
    "ToStoreCode": "M01",
    "ToWarehouseCode": "M01",
    "Series": "A",
    "SeriesNumber": 99999,
    "Description": "GM Üzerinden Onaylama",
    "StoreTransferType": 0,
    "Lines": []
  }

  line = {
    "OrderLineID": "",
    "UsedBarcode": "",
    "Qty1": "1"
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public serverService: ServerService, public alertCtrl: AlertController,
    public translateService: TranslateService, public loadingCtrl: LoadingController, public toastCtrl: ToastController, ) {
  }

  ionViewDidLoad() {
    this.getOpenOrders()
    console.log('ionViewDidLoad OpenOrdersPage');
  }

  getOpenOrders() {
    this.presentLoading();
    this.userData.Parameters[0].Value = this.serverService.Settings.V3Settings.StoreCode;
    this.serverService.getAny(this.userData).then(res => {
      console.log(res);
      this.list = res;
      if (Array.isArray(res)) this.serverService.openOrdersCount = res.length;
      this.dismissLoading();
    }).catch(this.handleError);
  }

  handleError(error: any) {
    this.dismissLoading();
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string[]) => {
      this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  showAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  private presentConfirmAlert(item) {
    let alert = this.alertCtrl.create({
      title: 'Ürün Transferi',
      subTitle: item.ItemCode + ' nolu ürün kodu, ' + item.ToStoreCode + ' mağazasına transfer işlemi Onaylıyor musunuz?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'İptal',
          role: 'cancel',
        },
        {
          text: 'Tamam',
          handler: () => {
            this.acceptTransfer(item);
          }
        },
      ]
    });
    alert.present();
  }

  acceptTransfer(item) {
    this.line.OrderLineID = item.OrderLineID;
    this.line.UsedBarcode = item.Barcode;

    //this.transferData.OfficeCode = item.OfficeCode;
    //this.transferData.StoreCode = item.StoreCode;
    //this.transferData.WarehouseCode = item.WarehouseCode;
        
    this.transferData.ToStoreCode = item.ToStoreCode;
    this.transferData.ToWarehouseCode = item.ToWarehouseCode;
    this.transferData.ShippingPostalAddressID = item.PostalAddressID;

    this.transferData.Lines = [];
    this.transferData.Lines.push(this.line);
    console.log(this.transferData);
    this.presentLoading();

    this.serverService.postData(this.transferData).then(res => {
      this.dismissLoading();
      console.log(res)
      if (res && res.ModelType == 59) {
        this.presentToast('Transfer talep işlemi onaylandi.');
        this.getOpenOrders();
      } else if (res && res.ModelType == 0) {
        this.presentToast(res.ExceptionMessage);
      }
    }).catch(this.handleError);
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'X'
    });
    toast.present();
  }

}
