import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html',
})
export class OrderDetailPage implements OnInit {

  loading: Loading;
  orderNumber: any;
  details: any;
  checkdatabase: number = 0;
  userData = { "ProcName": "G3_GetOrderDetails", "Parameters": [{ "Name": "OrderNumber", "Value": "" }] };
  total: number = 0;
  buttonClicked: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public serverService: ServerService,
    public alertCtrl: AlertController, public loadingCtrl: LoadingController, public translateService: TranslateService) {
    this.orderNumber = this.navParams.data.orderNumber;
    console.log(this.orderNumber);
    //JS if(this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2'){
    if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
      this.checkdatabase = 1;
    } 
  }

  onButtonClick() {
    this.buttonClicked = !this.buttonClicked;
    if (this.buttonClicked == true) {
      document.getElementById('headerStyle').style.display = 'none';
    } else {
      document.getElementById('headerStyle').style.display = 'flex';
    }
  }

  ngOnInit() {
    this.presentLoading();
    console.log(new Date());
    this.userData.Parameters[0].Value = this.orderNumber;
    this.serverService.getAny(this.userData)
      .then(res => {
        console.log(new Date())
        console.log(res);
        this.details = res;
        this.calculateTotal();
        this.dismissLoading();
      })
      .catch(this.handleError);
  }

  calculateTotal() {
    for (let item of this.details) {
      if(this.checkdatabase == 1){
        var tempprice = item.Doc_Price - (item.LDisRate1*item.Doc_Price/100);        
        this.total = Number((this.total + item.Qty1 * tempprice).toFixed(2));
      }else{
        this.total = Number((this.total + item.Doc_Amount).toFixed(2));
      }
    }
    console.log(this.total);
  }

  handleError(error: any) {
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string) => {
      this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
  }

  presentAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

}
