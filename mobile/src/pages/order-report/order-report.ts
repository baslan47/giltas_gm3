import {Component} from '@angular/core';
import {Loading, LoadingController, ModalController, NavController, NavParams, ViewController, AlertController} from 'ionic-angular';
//import {Customer} from "../../classes/customer";
import {ServerService} from "../../classes/server.service";
import {TranslateService} from "@ngx-translate/core";
import {OrderDetailPage} from "../order-detail/order-detail";
import { ModalFilterOrderPage } from '../modal-filter-order/modal-filter-order';

@Component({
  selector: 'page-order-report',
  templateUrl: 'order-report.html',
})
export class OrderReportPage {
  
  nextPage: string;
  total: number = 0;  
  public items: any;
  checkdatabase: number = 0;
  // and orderdate between '2019-01-31' and '2019-03-31'
  userData = {"ProcName": "", "Parameters": [{"Name": "Count", "Value": ""}, 
    {"Name": "WhereStr", "Value": ""}]};
  loading: Loading;
  buttonClicked: boolean = false;  
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
              public serverService: ServerService, public viewCtrl: ViewController, public loadingCtrl: LoadingController,
              public translateService: TranslateService, public alertCtrl: AlertController,) {                
    this.nextPage = this.navParams.get('nextPage');
    //JS if(this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2'){
    if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
      this.checkdatabase = 1;
    }     
    this.doFilter();    
  }   

  filterModal() {
    let modal;
    modal = this.modalCtrl.create(ModalFilterOrderPage);
    modal.present();
    modal.onDidDismiss(data => {      
      console.log(data);
      if(data){
        if(data.txtSD && data.txtED){
          const whrStr: string = " and o.OrderDate between '" + data.txtSD + "' and '" + data.txtED + "'";                         
          this.userData.Parameters[1].Value = whrStr;
         
        }else  if(data.txtON){
          const whrStr: string = " and o.OrderNumber LIKE '" + data.txtON + "%'";                         
          this.userData.Parameters[1].Value = whrStr;
        }
        else  if(data.txtCS){
          const whrStr: string = " and cr.CurrAccDescription LIKE '" + data.txtCS + "%'";                         
          this.userData.Parameters[1].Value = whrStr;
        }
      }else{
        
      }     
      this.doFilter(); 
    });
  }
  

  filterReport(){
    this.userData.Parameters[1].Value = '';    
    let prompt = this.alertCtrl.create({
      cssClass: 'alertCustomCss',
      title: 'FİLTRELE',          
      inputs: [
        {
          name: 'customer_name',
          placeholder: 'Müşteri Adı',
          type: 'text'
        },
        {
          name: 'order_number',
          placeholder: 'Sipariş Numarası',          
        },        
        {
          name: 'start_date',          
          type: 'date'
        },
        {
          name: 'end_date',          
          type: 'date'
        },        
      ],
      buttons: [                
        {
          text: 'Sonuçları Getir',
          handler: data => {
            console.log(data);   
            if(data.order_number == "" && data.start_date != ""){
              //const whrStr: string = " and ordernumber between '" + data.order_number + "' and '" + data.order_number + "'";                         
              const whrStr: string = " and orderdate between '" + data.start_date + "' and '" + data.end_date + "'";                         
              this.userData.Parameters[1].Value = whrStr;
              
            }else if(data.order_number == "" && data.start_date != ""){

            }    
            this.doFilter();        
          }
        },
        {
          text: 'İptal',
          handler: data => {
            
          }
        }, 
      ]
    });
    prompt.present();
  }

  onButtonClick() {
    this.buttonClicked = !this.buttonClicked;
    if(this.buttonClicked == true){
      document.getElementById('headerStyle').style.display = 'none';
    }else{
      document.getElementById('headerStyle').style.display = 'flex';
    } 
  }

   doFilter() {
    var temp = this.serverService.Settings.V3Settings.SalesType;
    this.presentLoading();
    console.log(this.serverService.Settings.V3Settings.SalesType);
    if(this.serverService.Settings.V3Settings.SalesType==1 || this.serverService.Settings.V3Settings.SalesType==4) this.userData.ProcName = "G3_WholesaleOrders";
    else if(temp==3 || temp==5 || temp==6) this.userData.ProcName = "G3_WholesaleRetailOrders";
    const count: number = this.serverService.Settings.G3Settings.OrderCount ? this.serverService.Settings.G3Settings.OrderCount: 10;    
    this.userData.Parameters[0].Value = count.toString();
    this.serverService.getAny(this.userData).then(res => {
      this.items = res;
      console.log(res);      
      if (Array.isArray(res)) this.items = res;
      else this.items = [];
      this.dismissLoading();    
    }).catch(this.handleError);
  }

  refreshFilter(){
    this.userData.Parameters[1].Value = "";
    this.doFilter();
  }

  handleError(error: any) {
    console.error('An error occurred', error);
  }

  goTo(order: any){    
    switch (this.nextPage) {
      case 'proposal':
        this.selectOrder(order.OrderNumber);
        break;
      case 'orderdetail':
        this.goToOrderDetails(order.OrderNumber);
        break;
    }
  }

  selectOrder(number) {
    this.viewCtrl.dismiss(number);
  }

  goToOrderDetails(number) {
    this.navCtrl.push(OrderDetailPage, {orderNumber: number});
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  back(){
    this.viewCtrl.dismiss();
  }
  
}