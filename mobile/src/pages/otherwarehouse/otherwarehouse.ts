import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, ToastController, AlertController } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";

@Component({
  selector: 'page-otherwarehouse',
  templateUrl: 'otherwarehouse.html',
})
export class OtherwarehousePage {

  loading: Loading;
  warehouseproducts: any;
  storeData = { "ProcName": "", "Parameters": [{ "Name": "ItemCode", "Value": "" }] };
  ProductWarehouse: any;

  transferData = {
    "ModelType": "18",
    "CompanyCode": 1, 
    "OfficeCode": "", 
    "StoreCode": "", 
    "WarehouseCode": "", 
    "ToStoreCode": this.serverService.Settings.V3Settings.StoreCode,
    "ToWarehouseCode": this.serverService.Settings.V3Settings.WarehouseCode,
    "InternalDescription": "GM Ürün Transfer Talebi", 
    "IsCompleted": true, "IsCreditableConfirmed": true, "IsCreditSale": true, 
    "StoreOrderRequestTypes": 1,
    "Lines": []
  }

  line = {
     "ItemTypeCode": 1, 
     "ItemCode": "",
     "ColorCode": "",
     "ItemDim1Code": "",
     "Qty1": "1",
     "UsedBarcode": ""
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public serverService: ServerService, public toastCtrl: ToastController, public alertCtrl: AlertController) {
    this.warehouseproducts = this.navParams.data.warehouseproducts;
  }

  ngOnInit() {
    this.presentLoading();
    this.storeData.ProcName = "G3_GetAllProductWarehouse";
    this.storeData.Parameters[0].Value = this.warehouseproducts;
    this.serverService.getAny(this.storeData).then(res => {
      console.log('G3_GetAllProductWarehouse');
      console.log(res);
      console.log(this.serverService.Settings.V3Settings.StoreCode);
      this.ProductWarehouse = res;
      this.dismissLoading();

    }).catch(error => this.handleError(error));
  }

  public presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Lütfen bekleyiniz...",
    });
    this.loading.present();
  }

  private handleError(error: any) {
    this.dismissLoading();
    console.error('An error occurred', error);
  }

  public dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtherwarehousePage');
  }

  transfer(item) {
    if (item.Inventory > 0 && item.Barcode != '') {
      this.line.UsedBarcode = item.Barcode;

      this.transferData.OfficeCode = item.OfficeCode;
      this.transferData.StoreCode = item.StoreCode;
      this.transferData.WarehouseCode = item.WarehouseCode;

      this.transferData.Lines.push(this.line);
      console.log(this.transferData);
      this.presentLoading();
      this.serverService.postData(this.transferData).then(res => {
        this.dismissLoading();
        console.log(res)
        if (res && res.ModelType == 18) {
          this.presentToast('Transfer talep işlemi gönderildi.');
        }else if (res && res.ModelType == 0) {
          this.presentToast(res.ExceptionMessage);
        }
      }).catch(this.handleError);
    }
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'X'
    });
    toast.present();
  }

  private presentAlert(item) {
    if (item.Inventory > 0 && item.Barcode != '') {
      let alert = this.alertCtrl.create({
        title: 'Ürün Transferi',
        subTitle: item.Barcode + ' barkod nolu ürün ' + item.WarehouseDescription + ' dan transfer işlemi başlatılacak, Onaylıyor musunuz?',
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'İptal',
            role: 'cancel',
          },
          {
            text: 'Tamam',
            handler: () => {
              this.transfer(item);
            }
          },
        ]
      });
      alert.present();
    }
  }

}
