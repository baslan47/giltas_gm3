import { Component } from '@angular/core';
import { AlertController, Loading, LoadingController, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: 'page-produc-inventory-report',
    templateUrl: 'produc-inventory-report.html',
})
export class ProducInventoryReportPage {

    loading: Loading;
    warehouseproducts: any;
    storeData = {"ProcName": "", "Parameters": [{"Name": "ItemCode", "Value": ""}]};
    ProductWarehouse: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
        public serverService: ServerService,
        public alertCtrl: AlertController, public toastCtrl: ToastController,
        public translateService: TranslateService, public loadingCtrl: LoadingController, ) {
        this.warehouseproducts = this.navParams.data.warehouseproducts;
        console.log(this.warehouseproducts);   
    }

    ngOnInit() {
        this.presentLoading();
        this.storeData.ProcName = "G3_GetAllProductWarehouse";
        this.storeData.Parameters[0].Value = this.warehouseproducts;
        this.serverService.getAny(this.storeData).then(res => {
          console.log('G3_GetAllProductWarehouse');
          console.log(res);
          this.ProductWarehouse = res;
          this.dismissLoading();
    
      }).catch(error => this.handleError(error));
    }
    
    public presentLoading() {
      this.loading = this.loadingCtrl.create({
          content: "Lütfen bekleyiniz...",
      });
      this.loading.present();
    }
    
    private handleError(error: any) {
      this.dismissLoading();
      console.error('An error occurred', error);
    }
    
    public dismissLoading() {
      if (this.loading) {
          this.loading.dismiss();
          this.loading = null;
      }
    }
}
