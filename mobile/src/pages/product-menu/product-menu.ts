import { Component } from '@angular/core';
import { AlertController, Loading, LoadingController, NavController, NavParams, ModalController } from 'ionic-angular';
import { AttributeTypesPage } from "../attribute-types/attribute-types";
import { ProductsPage } from "../products/products";
import { ProductPage } from "../product/product";
import { ServerService } from "../../classes/server.service";
import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { TranslateService } from "@ngx-translate/core";
import { BarcodeTestPage } from "../barcode-test/barcode-test";
import { ModalProductfilterPage } from '../modal-productfilter/modal-productfilter';

@Component({
  selector: 'page-product-menu',
  templateUrl: 'product-menu.html',
})
export class ProductMenuPage {
  public myInputOld: string = '';
  public myInputText: string = '';


  loading: Loading;
  getItemCodeData = { "ProcName": "G3_GetProductItemCode", "Parameters": [{ "Name": "Barcode", "Value": "" }] };

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public serverService: ServerService, public alertCtrl: AlertController,
    private barcodeScanner: BarcodeScanner, public translateService: TranslateService,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductMenuPage');
  }

  goToAttributeTypesPage() {
    this.navCtrl.push(AttributeTypesPage);
  }

  scanBarcode2() {
    var barcode = '2230000001396'; // 86933001';
    this.getItemCodeData.Parameters[0].Value = barcode;
    this.serverService.getAny(this.getItemCodeData).then(res => {
      if (res)
        this.navCtrl.push(ProductPage, { product: res[0], barcode: barcode });
    }).catch(this.handleError);
  }

  scanBarcode() {
    //8698536442375    
    this.serverService.isOpenCamera = true;
    console.log(this.serverService.isOpenCamera)

    this.barcodeScanner.scan().then((barcodeData) => {
      this.serverService.isOpenCamera = false;
      console.log(barcodeData.format)
      if (barcodeData.format == "EAN_8" || barcodeData.format == "EAN_13" || barcodeData.format == "CODE_128" || barcodeData.format == "UPC_A") {
        this.getItemCodeData.Parameters[0].Value = barcodeData.text;
        this.serverService.getAny(this.getItemCodeData).then(res => {
          if (res) {
            if (res.length > 0) {
              this.navCtrl.push(ProductPage, { product: res[0], barcode: barcodeData.text });
              // this.serverService.isOpenCamera = false;
            }
            else {
              this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'PROPOSAL_PRODUCT_NOT_FOUND_TEXT']).subscribe((value: string) => {
                this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['PROPOSAL_PRODUCT_NOT_FOUND_TEXT']);
              });
              // this.serverService.isOpenCamera = false;
            }
          }
        }).catch(this.handleError);
      } else {
        // this.serverService.isOpenCamera = false;
        this.showAlert("Geçersiz Barcode", "Lütfen Geçerli Formatta Barcode Okutunuz");
      }
    }, (err) => {
      console.log(err);
    });
  }

  filterModal() {
    let modal;
    modal = this.modalCtrl.create(ModalProductfilterPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data.result == false) {
        console.log(data.result);
      } else {
        if (data.txtPN) {
          this.navCtrl.push(ProductsPage, { myInputOld: data.txtPN, myInputText: 'PN' });
        }
        else if (data.txtPC) {
          this.navCtrl.push(ProductsPage, { myInputOld: data.txtPC, myInputText: 'PC' });
        }
      }
    });
  }

  private handleError(error: any) {
    this.dismissLoading();
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string) => {
      this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
  }

  public presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  public dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  public showAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  goTobarcodeTestPage() {
    this.navCtrl.push(BarcodeTestPage);
  }
}
