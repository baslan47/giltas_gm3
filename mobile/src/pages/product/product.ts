import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, Loading, ToastController, ModalController } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import 'rxjs/Rx';
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { InventoryPage } from "../inventory/inventory";
import { Product, ProductDetails } from "../../models/models";
import { WarehousePage } from "../warehouse/warehouse";
import { ImgLoaderComponent } from 'ionic-image-loader';
import { trigger, style, animate, transition } from '@angular/animations';
import { OtherwarehousePage } from '../otherwarehouse/otherwarehouse';

@Component({
  selector: 'page-product',
  animations: [
    trigger(
      'enterAnimation', [
      transition(':enter', [
        style({ transform: 'opacity 1s ease-out;', opacity: 0 }),
        animate('500ms', style({ transition: 'opacity 1s ease-out;', opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'opacity 1s ease-out;', opacity: 1 }),
        animate('500ms', style({ transform: 'opacity 1s ease-out;', opacity: 0 }))
      ])
    ]
    )
  ],
  templateUrl: 'product.html',
})
export class ProductPage implements OnInit {
  product: Product;
  productInventory: any;
  userData = { "ProcName": "", "Parameters": [{ "Name": "ItemCode", "Value": "" }] };
  pricesData = {
    "ProcName": "G3_GetProductPrices",
    "Parameters": [{ "Name": "ItemCode", "Value": "" }, { "Name": "PriceGroupCode", "Value": "" }]
  };
  storeData = { "ProcName": "", "Parameters": [{ "Name": "ItemCode", "Value": "" }, { "Name": "WarehouseCode", "Value": "" }] };
  barcodeData = {
    "ProcName": "G3_GetProductPriceByBarcode",
    "Parameters": [{ "Name": "Barcode", "Value": "" }, { "Name": "PriceGroupCode", "Value": "" }]
  };
  getBarcodeData = {
    "ProcName": "G3_GetProductBarcode", "Parameters": [{ "Name": "ItemCode", "Value": "" },
    { "Name": "ColorCode", "Value": "" }, { "Name": "ItemDim1Code", "Value": "" }]
  };

  getAvailableInventoryV3 = {
    "ProcName": "G3_GetAvailableInventory", "Parameters": [{ "Name": "ItemCode", "Value": "" },
    { "Name": "ColorCode", "Value": "" }]
  };

  PaymentPlanProduct = {
    "ProcName": "G3_GetPaymentPlanByItemCode",
    "Parameters": [
      { "Name": "ItemCode", "Value": "" },
      { "Name": "PriceGroupCode", "Value": "" },
      { "Name": "Language", "Value": "" }
    ]
  };

  //G3_GetAvailableInventory
  //12h647600700



  loading: Loading;
  productDetails: ProductDetails;
  productPrices: any[] = [];
  productPricesIF: any[] = [];
  productColorSizeMatrix: any[] = [];
  productColorSizeMatrixNew: any[] = [];
  productColors: any[] = [];
  tempItemDim: any[] = [];
  colHeaders: any;
  imageURL: any;
  color: string;
  tempMaxQty: number = 0;
  checkdatabase: number = 0;
  ModuleLicense: string;
  selectedSize: string;
  alternatives: any[] = [];
  sizes: any;
  barcode: string;
  quantity: number;
  quantities: number[] = [];
  advancePrice: any;
  PaymentPlanProductList: any[] = [];
  tempPaymentPlanProductList: any[] = [];
  temp2PaymentPlanProductList: any[] = [];
  PaymentPlanDescriptions: any[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public serverService: ServerService, public alertCtrl: AlertController, private photoViewer: PhotoViewer,
    public toastCtrl: ToastController, public modalCtrl: ModalController) {
    //JS if (this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2') {
    if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
      this.checkdatabase = 1;
    }
    this.ModuleLicense = this.serverService.Settings.G3License.Modules;
    this.product = this.navParams.data.product;
    this.barcode = this.navParams.data.barcode;
    this.imageURL = this.serverService.Settings.G3Settings.ImageUrl + '/Home/GetImage?itemcode=' + this.product.ItemCode;
  }



  ngOnInit() {
    this.getProductPricesWithItemCode();
    if (this.serverService.Settings.Integrator.DatabaseName == 'YALISPOR_V3') {
      this.getProductPricesWithItemCodeIF();
    }
    console.log(this.product);
    console.log(this.barcode);
    this.presentLoading();
    this.getProductPricesWithBarcode();
    this.getProductDetails();
    this.getProductInventory();
    this.getProductColorSizeMatrix();
    this.getItemDim1s();
    this.getAlternativeProducts();


    // this.pricesData.Parameters[0].Value = this.product.ItemCode;
    // this.pricesData.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
    // this.serverService.getProductPrices(this.pricesData).then(res => {
    //   if (res) {
    //     this.productPrices = res;
    //     console.log('G3_GetProductPrices');
    //     console.log(this.productPrices);
    //     console.log(this.productPrices[0])
    //     for (let i = 0; i < this.productPrices.length; i++) {
    //       if (this.productPrices[i].PaymentPlanCode == "") {
    //         this.advancePrice = this.productPrices[i].Price
    //       }
    //     }
    //     console.log(this.advancePrice);
    //   }
    // }).catch(error => this.handleError(error));


    // this.PaymentPlanProduct.Parameters[0].Value = this.product.ItemCode;
    // this.PaymentPlanProduct.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
    // this.PaymentPlanProduct.Parameters[2].Value = "tr";
    // this.serverService.getAny(this.PaymentPlanProduct)
    //   .then(res => {
    //     console.log('PaymentPlanProduct', res)
    //     this.PaymentPlanProductList = res;
    //     this.product.PaymentPlanProduct = res;
    //   })


  }
  // sortPrices() {
  //   function heapSort(arr) {
  //     var len = arr.length,
  //       end = len - 1;

  //     heapify(arr, len);

  //     while (end > 0) {
  //       swap(arr, end--, 0);
  //       siftDown(arr, 0, end);
  //     }
  //     return arr;
  //   }
  //   function heapify(arr, len) {
  //     // break the array into root + two sides, to create tree (heap)
  //     var mid = Math.floor((len - 2) / 2);
  //     while (mid >= 0) {
  //       siftDown(arr, mid--, len - 1);
  //     }
  //   }
  //   function siftDown(arr, start, end) {
  //     var root = start,
  //       child = root * 2 + 1,
  //       toSwap = root;
  //     while (child <= end) {
  //       if (arr[toSwap] < arr[child]) {
  //         swap(arr, toSwap, child);
  //       }
  //       if (child + 1 <= end && arr[toSwap] < arr[child + 1]) {
  //         swap(arr, toSwap, child + 1)
  //       }
  //       if (toSwap != root) {
  //         swap(arr, root, toSwap);
  //         root = toSwap;
  //       }
  //       else {
  //         return;
  //       }
  //       toSwap = root;
  //       child = root * 2 + 1
  //     }
  //   }
  //   function swap(arr, i, j) {
  //     var temp = arr[i];
  //     arr[i] = arr[j];
  //     arr[j] = temp;
  //   }
  // }

  getProductPricesWithItemCode() {
    this.pricesData.Parameters[0].Value = this.product.ItemCode;
    this.pricesData.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
    this.serverService.getProductPrices(this.pricesData).then(res => {
      if (res) {
        this.productPrices = res;
        console.log('G3_GetProductPrices', this.productPrices);
      }
    }).catch(error => this.handleError(error));
  }

  getProductPricesWithItemCodeIF() {
    this.pricesData.Parameters[0].Value = this.product.ItemCode;
    this.pricesData.Parameters[1].Value = 'IF';
    this.serverService.getProductPrices(this.pricesData).then(res => {
      if (res) {
        this.productPricesIF = res;
        console.log('G3_GetProductPricesIF', this.productPricesIF);
      }
    }).catch(error => this.handleError(error));
  }

  getAvailableInventory(val1) {
    this.getAvailableInventoryV3.Parameters[0].Value = this.product.ItemCode;
    this.getAvailableInventoryV3.Parameters[1].Value = val1;
    this.serverService.getAny(this.getAvailableInventoryV3).then(res => {
      console.log('G3_getAvailableInventory');
      this.productColorSizeMatrixNew = res;
      console.log(this.productColorSizeMatrixNew);
    }).catch(error => this.handleError(error));
  }

  getProductDetails() {

    this.storeData.ProcName = "G3_GetProductDetails";
    this.storeData.Parameters[0].Value = this.product.ItemCode;
    this.storeData.Parameters[1].Value = (this.serverService.Settings.V3Settings.WarehouseCode) ? this.serverService.Settings.V3Settings.WarehouseCode : "";
    this.serverService.getAny(this.storeData).then(res => {
      console.log('G3_GetProductDetails');
      console.log(res);
      this.productDetails = res[0];
    }).catch(error => this.handleError(error));
  }

  getAlternativeProducts() {
    this.userData.ProcName = "G3_GetAlternativeProducts";
    this.userData.Parameters[0].Value = this.product.ItemCode;
    this.serverService.getAny(this.userData).then(res => {
      this.alternatives = res;
      console.log('Alternatives');
      console.log(this.alternatives);
    }).catch(error => this.handleError(error));
  }

  getItemDim1s() {
    this.userData.ProcName = "G3_GetItemDim1s";
    this.userData.Parameters[0].Value = this.product.ItemCode;
    this.serverService.getAny(this.userData).then(res => {
      console.log('G3_GetItemDim1s');
      console.log(res);
      if (res) this.colHeaders = res;
    }).catch(error => this.handleError(error));
  }

  getProductColorSizeMatrix() {
    this.storeData.ProcName = "G3_GetProductColorSizeMatrix3";
    this.storeData.Parameters[0].Value = this.product.ItemCode;
    this.storeData.Parameters[1].Value = (this.serverService.Settings.V3Settings.WarehouseCode) ? this.serverService.Settings.V3Settings.WarehouseCode : "";
    this.serverService.getAny(this.storeData).then(res => {
      console.log('G3_GetProductColorSizeMatrix3');
      console.log(res);
      this.dismissLoading();
      console.log(this.product.ItemCode, this.serverService.Settings.V3Settings.WarehouseCode)
      if (res.length > 0 && JSON.stringify(res).toString().indexOf('Exception') == -1) {
        this.productColorSizeMatrix = res;
        // if(this.checkdatabase == 1){
        //   for(let i = 0; i < this.productColorSizeMatrix.length; i++){
        //     this.tempItemDim = [];
        //     this.getAvailableInventoryV3.Parameters[0].Value = this.product.ItemCode;
        //     this.getAvailableInventoryV3.Parameters[1].Value = this.productColorSizeMatrix[i]['R/B'];    
        //     this.serverService.getAny(this.getAvailableInventoryV3).then(res => {
        //       console.log(res);   
        //       this.tempItemDim = res;                                                       
        //       for(let k = 0; k < this.tempItemDim.length; k++){
        //         this.productColorSizeMatrix[i][this.tempItemDim[k]['ItemDim1Code']] = this.tempItemDim[k]['AvailableInventoryQty1'];
        //       }
        //     }).catch(error => this.handleError(error));                        
        //   }
        // }
        if (!this.color && this.productColorSizeMatrix[0]['R/B']) this.color = this.productColorSizeMatrix[0]['R/B'];
        if (this.color) this.sizes = this.productColorSizeMatrix.find(x => x['R/B'] == this.color);
        if (this.checkdatabase == 1) {
          this.getAvailableInventory(this.color);
        }
        console.log('sizes');
        console.log(this.sizes);
      } else {// sadece renk
        this.storeData.ProcName = "G3_GetProductColors";
        this.storeData.Parameters[0].Value = this.product.ItemCode;
        this.storeData.Parameters[1].Value = (this.serverService.Settings.V3Settings.WarehouseCode) ? this.serverService.Settings.V3Settings.WarehouseCode : "";
        this.serverService.getAny(this.storeData).then(res => {
          console.log(res);
          if (res.length > 0 && this.productColorSizeMatrix.length == 0 && ((res.length == 1 || res.length == 2) && res[0].ColorCode != "")) {
            this.productColors = res;
            console.log(this.serverService.getColorDescription(res[0].ColorCode))
          }
        })
      }
    }).catch(error => this.handleError(error));
  }

  getProductInventory() {
    this.userData.ProcName = "G3_GetProductInventory";
    console.log(this.userData);
    this.serverService.getAny(this.userData).then(res => {
      if (res) {
        console.log('Inventory');
        console.log(res);
        this.productInventory = res;
      }

    }).catch(error => this.handleError(error));
  }

  getProductPricesWithBarcode() {
    this.getBarcodeData.Parameters[0].Value = this.product.ItemCode;
    this.getBarcodeData.Parameters[1].Value = this.color ? this.color : '';
    this.getBarcodeData.Parameters[2].Value = this.selectedSize ? this.selectedSize : '';
    console.log(this.getBarcodeData);
    this.serverService.getAny(this.getBarcodeData).then(res => {
      console.log(res);
      this.product.UsedBarcode = res[0].Barcode;
      this.barcodeData.Parameters[0].Value = res[0].Barcode;
      this.barcodeData.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
      this.serverService.getProductPrice(this.barcodeData).then(res => {
        console.log('getProductPricesWithBarcode', res);
        //this.getAvailableInventory(res.ColorCode);
        this.PaymentPlanProductList = res.PriceList;
        for (let n = 0; n < this.PaymentPlanProductList.length; n++) {
          if (this.PaymentPlanProductList[n].PaymentPlanCode == "X03" || this.PaymentPlanProductList[n].PaymentPlanCode == "X06" || this.PaymentPlanProductList[n].PaymentPlanCode == "X12") {
            this.PaymentPlanProductList.splice(n, 1);
            n--;
          }
        }
        this.getPaymentPlanDescription();
        //this.product.PaymentPlanProduct = res.PriceList;2015+Veritas

      }).catch(error => this.handleError(error));
    }).catch(error => this.handleError(error));
  }

  getPaymentPlanDescription() {
    let planData = { "ProcName": "G3_GetPaymentPlanDesc", "Parameters": [{ "Name": "Language", "Value": "TR" }] };
    this.serverService.getAny(planData).then(res => {
      //this.PaymentPlanDescriptions = res;
      console.log(res);
      if (this.checkdatabase == 1) {
        for (let x of this.PaymentPlanProductList) {
          if (this.tempPaymentPlanProductList.indexOf(x.PaymentPlanCode) < 0) {
            this.tempPaymentPlanProductList.push(x.PaymentPlanCode);
            this.temp2PaymentPlanProductList.push(x);
          }
        }
        console.log(this.temp2PaymentPlanProductList);
        this.PaymentPlanProductList = this.temp2PaymentPlanProductList;
      }
      for (let i = 0; i < this.PaymentPlanProductList.length; i++) {
        for (let n = 0; n < res.length; n++) {
          if (this.PaymentPlanProductList[i].PaymentPlanCode == res[n].PaymentPlanCode) {
            if (res[n].PaymentPlanCode == "") res[n].PaymentPlanDescription = "Peşin";
            this.PaymentPlanDescriptions.push(res[n].PaymentPlanDescription)
          }
        }
      }
      if (this.checkdatabase == 1) {
        this.gotoWarehousePage();
      }
    })
  }

  getPriceGroup(item: string) {
    var tempPriceGroupCode: string;
    if (item == null) return;

    else if (item.charAt(0) == "€") {
      tempPriceGroupCode = '€'
      return tempPriceGroupCode;
    } else if (item.charAt(0) == "$") {
      tempPriceGroupCode = '$'
      return tempPriceGroupCode;
    } else {
      tempPriceGroupCode = '₺'
      return tempPriceGroupCode;
    }
  }

  private handleError(error: any) {
    this.dismissLoading();
    console.error('An error occurred', error);
    this.showAlert('Hata', 'Sunucuya bağlanırken hata oldu.' + error);
  }

  public presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Lütfen bekleyiniz...",
    });
    this.loading.present();
  }

  public dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  public showAlert(title: string, subTitle: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: ['OK']
    });
    alert.present();
  }

  public setSizes() {
    if (this.color) {
      this.selectedSize = null;
      this.quantity = null;
      this.quantities = [];
      this.sizes = this.productColorSizeMatrix.find(x => x['R/B'] == this.color);
      //this.getAvailableInventory(this.color);         
      if (this.checkdatabase == 1) {
        this.getAvailableInventoryV3.Parameters[0].Value = this.product.ItemCode;
        this.getAvailableInventoryV3.Parameters[1].Value = this.color;
        this.serverService.getAny(this.getAvailableInventoryV3).then(res => {
          console.log('G3_getAvailableInventory');
          //console.log(res);      
          this.productColorSizeMatrixNew = res;
          console.log(this.productColorSizeMatrixNew)
        }).catch(error => this.handleError(error));
      }
    }
    console.log('sizes');
    console.log(this.sizes);
  }

  setSelectedSize(size: any) {
    console.log(size);

    if (this.checkdatabase == 1) {
      if (size.AvailableInventoryQty1 > 0) {
        this.tempMaxQty = size.AvailableInventoryQty1;
        this.selectedSize = size.ItemDim1Code;
        this.quantities = [];
        for (let i = 0; i < size.AvailableInventoryQty1; i++) this.quantities.push(i + 1);
        this.quantity = 1;
      }
    } else {
      if (this.sizes[size.ItemDim1Code] > 0) {
        this.tempMaxQty = this.sizes[size.ItemDim1Code];
        this.selectedSize = size.ItemDim1Code;
        this.quantities = [];
        for (let i = 0; i < this.sizes[size.ItemDim1Code]; i++) this.quantities.push(i + 1);
        this.quantity = 1;
      }
    }
  }

  showImage() {
    let url = this.imageURL;
    url += (this.color) ? ('&colorcode=' + this.color) : '';
    this.photoViewer.show(url);
  }

  addToBasket() {
    if (this.ModuleLicense.charAt(2) != "0") {
      if (this.productColorSizeMatrix && this.sizes) {
        if (!this.color) {
          this.showAlert('Bilgi', 'Lütfen renk seçeniz.');
          return;
        }
        if (!this.selectedSize && !this.product.ItemDim1Code) {
          this.showAlert('Bilgi', 'Lütfen beden seçeniz.');
          return;
        }
      }
      this.addProduct();
    } else {
      this.presentAccountAlert();
    }
  }

  presentAccountAlert() {
    const alert = this.alertCtrl.create({
      title: 'UYARI',
      message: 'Lisansınız bu işlem için aktif değildir.',
      buttons: ['OK']
    });
    alert.present();
  }

  addProduct() {
    this.presentLoading();
    this.getBarcodeData.Parameters[0].Value = this.product.ItemCode;
    this.getBarcodeData.Parameters[1].Value = this.color ? this.color : '';
    this.getBarcodeData.Parameters[2].Value = this.selectedSize ? this.selectedSize : '';
    console.log(this.getBarcodeData);
    this.serverService.getAny(this.getBarcodeData).then(res => {
      console.log(res);
      this.product.UsedBarcode = res[0].Barcode;
      if (this.checkdatabase == 1) {
        this.product.MesureCode = "AD"
      } else {
        this.product.MesureCode = res[0].MesureCode
      }

      this.barcodeData.Parameters[0].Value = res[0].Barcode;
      this.barcodeData.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
      this.serverService.getProductPrice(this.barcodeData).then(res => {
        console.log(res);

        this.product.PriceList = res.PriceList;
        this.product.BarcodeType = res.BarcodeType;
        this.product.LDisRate1 = 0;
        this.product.MaxQty = this.tempMaxQty;
        if (this.checkdatabase == 1) {
          this.product.ColorCode = this.productColorSizeMatrixNew[0]['ColorCode'];
          this.product.ItemDim1Code = this.selectedSize;
        } else {
          this.product.ColorCode = this.color;
          this.product.ItemDim1Code = this.selectedSize;
        }
        this.pushProduct();
        this.loading.dismiss();
      }).catch(error => this.handleError(error));
    }).catch(error => this.handleError(error));
  }

  pushProduct() {
    this.product.Qty1 = this.quantity ? this.quantity : 1;
    for (let item of this.serverService.Items) {
      if (item.ItemCode == this.product.ItemCode && item.UsedBarcode == this.product.UsedBarcode) {
        item.Qty1 += this.product.Qty1;
        return
      }
    }
    this.product.Price = this.productDetails.RetailSalePrice;
    this.serverService.Items.push(this.product);
    this.selectedSize = null;
    this.quantity = null;
    this.quantities = [];
  }

  gotoAllProductWarehousePage() {
    this.navCtrl.push(OtherwarehousePage, { warehouseproducts: this.product.ItemCode });
  }

  gotoInventoryPage() {
    if (this.color && this.sizes && this.colHeaders) {
      let sizesZero: any[] = [];
      for (let size of this.colHeaders) {
        if (size.ItemDim1Code != 'R/B' && this.sizes[size.ItemDim1Code] < 1)
          sizesZero.push(size.ItemDim1Code);
      }
      let inventoryList: any[] = [];
      for (let size of sizesZero) {
        for (let inventory of this.productInventory) {
          if (inventory.ColorCode == this.color && inventory.ItemDim1Code == size && inventory.StoreCode != this.serverService.Settings.V3Settings.StoreCode)
            inventoryList.push(inventory);
        }
      }
      if (inventoryList.length > 0) {
        this.navCtrl.push(InventoryPage, { productInventory: inventoryList });
      }
      else {
        this.presentAlert("Bilgi", "Mağaza stoklarınızda olmayan ürünler diğer mağaza stoklarında da yok...");
      }
    }
  }

  gotoWarehousePage() {
    this.navCtrl.push(WarehousePage, {
      product: this.product,
      productPrices: this.PaymentPlanProductList,
      colHeaders: this.colHeaders,
      productColorSizeMatrix: this.productColorSizeMatrix,
      PaymentPlanDescriptions: this.PaymentPlanDescriptions,
    });
  }

  goToAlternativeProductDetail(product: Product) {
    this.navCtrl.push(ProductPage, { product: product });
  }

  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'X'
    });
    toast.present();
  }

  presentAlert(title: string, message: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }
}
