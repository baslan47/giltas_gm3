import {Component, OnInit} from '@angular/core';
import {NavController, AlertController, Loading, LoadingController, NavParams, ModalController} from 'ionic-angular';
import {ServerService} from "../../classes/server.service";
import {ProductPage} from '../product/product';
import {TranslateService} from "@ngx-translate/core";
import {Product} from "../../models/models";
import { ProducInventoryReportPage } from '../produc-inventory-report/produc-inventory-report';
import { ModalProductfilterPage } from '../modal-productfilter/modal-productfilter';

@Component({
  selector: 'page-products',
  templateUrl: 'products.html'
})
export class ProductsPage implements OnInit {
  public myInput: string = '';
  public myInputOld: string = '';
  public myInputText: string = '';
  public items: Array<Product>;
  userData = {
    "ProcName": "G3_ProductListNEW",
    "Parameters": [{"Name": "ItemDescription", "Value": ""}, {"Name": "ItemCode", "Value": ""}, {"Name": "AttributeCode", "Value": ""},
    {"Name": "AttributeType", "Value": ""}]
  };
  loading: Loading;
  attributeCode: any;
  attributeDescription: any;
  attributeTypeCode: any;
  nextPage: string;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams,
              public loadingCtrl: LoadingController, public serverService: ServerService,
              public translateService: TranslateService, public modalCtrl: ModalController) {
    this.myInput = this.navParams.data.myInput;
    this.myInputOld = this.navParams.data.myInputOld;
    this.myInputText = this.navParams.data.myInputText;
    this.attributeCode = this.navParams.data.attributeCode;
    this.attributeTypeCode = this.navParams.data.attributeTypeCode;
    this.myInput = this.attributeDescription;
    console.log(this.myInputOld);    
    console.log(this.myInputText);  
    console.log(this.attributeCode);    
    console.log(this.attributeTypeCode);       
    this.nextPage = this.navParams.get('nextPage');
  }

  ngOnInit() {
    console.log(this.serverService.Settings.G3Settings.ImageUrl)
    if(this.attributeTypeCode){
      this.userData.Parameters[2].Value = this.attributeCode
      this.userData.Parameters[3].Value = this.attributeTypeCode
    }else{
      if(this.myInputText == 'PN'){
        this.userData.Parameters[0].Value = this.myInputOld
      }else if(this.myInputText == 'PC'){
        this.userData.Parameters[1].Value = this.myInputOld
      }
    }    
    this.doFilter();
  }

  ionViewDidLoad() {
    if(this.nextPage == null){
      return this.nextPage = 'productdetail'
    }
    //this.loader.dismiss();
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string[]) => {
      this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
    if (this.loading)
      this.loading.dismiss();
  }

  filterModal() {    
    this.refreshFilter();
    let modal;
    modal = this.modalCtrl.create(ModalProductfilterPage);
    modal.present();
    modal.onDidDismiss(data => { 
      if(data.result == false){
        console.log(data.result);
      }else{       
        if(data.txtPN){
          this.userData.Parameters[0].Value = data.txtPN;
        }
        else if(data.txtPC){
          this.userData.Parameters[1].Value = data.txtPC;
        }              
        this.doFilter();
      }        
    });
  }   

  refreshFilter(){
    this.userData.Parameters[0].Value = "";
    this.userData.Parameters[1].Value = "";    
    this.userData.Parameters[2].Value = ""; 
    this.userData.Parameters[3].Value = "";      
    //this.doFilter();
  }

  public presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  public showAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }
  
  goTo(product: Product){
    //look at ionViewDidLoad()
    switch (this.nextPage) {
      case 'productdetail':
        this.goToProductDetail(product);
        break;
      case 'productinventoryreport':
        this.goToProductInventoryReport(product);
        break;    
    }
  }

  goToProductDetail(product: Product) {
    this.navCtrl.push(ProductPage, {product: product});
  }

  goToProductInventoryReport(product: Product) {
    this.navCtrl.push(ProducInventoryReportPage, {warehouseproducts: product.ItemCode});
  }

  onInput(event: any) {
    if (this.myInput.length >= 3) {
      //TODO setToStorage it in settings
    }
  }

  onCancel(event: any) {
    this.doFilter();
  }

  doFilter() {
    this.presentLoading();          
    this.serverService.getProducts(this.userData)
      .then(res => {
        this.items = res;
        console.log(res)
        this.loading.dismiss();
      })
      .catch(this.handleError);
  }

  sendMessageWithEnterKey(event: any) {
    event.preventDefault();
    this.doFilter();
  }
}
