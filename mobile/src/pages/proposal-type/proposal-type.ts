import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ProposalPage } from '../proposal/proposal';
import { ServerService } from '../../classes/server.service';

@Component({
  selector: 'page-proposal-type',
  templateUrl: 'proposal-type.html',
})
export class ProposalTypePage {

  changesaletype: string;
  SaleTypeLicense: string;
  SaleTypeLicense0: boolean;
  SaleTypeLicense1: boolean;
  SaleTypeLicense2: boolean;
  SaleTypeLicense3: boolean;
  SaleTypeLicense4: boolean;
  SaleTypeLicense5: boolean;
  text: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public serverService: ServerService) {
    this.text = this.navParams.get('text');
    this.SaleTypeLicense = this.serverService.Settings.G3License.SalesTypes;
    if(this.SaleTypeLicense.charAt(0) != '0'){
      this.SaleTypeLicense0 = true;
    }
    if(this.SaleTypeLicense.charAt(1) != '0'){
      this.SaleTypeLicense1 = true;
    }
    if(this.SaleTypeLicense.charAt(2) != '0'){
      this.SaleTypeLicense2 = true;
    }
    if(this.SaleTypeLicense.charAt(3) != '0'){
      this.SaleTypeLicense3 = true;
    }
    if(this.SaleTypeLicense.charAt(4) != '0'){
      this.SaleTypeLicense4 = true;
    }
    if(this.SaleTypeLicense.charAt(5) != '0'){
      this.SaleTypeLicense5 = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProposalTypePage');
  }

  goToProposalPage() {
    console.log("sadsa")
    this.viewCtrl.dismiss({type: this.changesaletype});
  }

  closeModal(){
    this.viewCtrl.dismiss({type: false});
  }

}
