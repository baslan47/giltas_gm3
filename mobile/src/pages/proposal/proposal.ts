import { Component, OnInit, ɵConsole } from '@angular/core';
import {
  NavController,
  AlertController,
  LoadingController,
  NavParams,
  ModalController,
  ActionSheetController, Loading, ToastController
} from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ServerService } from "../../classes/server.service";
import { SelectCustomerPage } from '../select-customer/select-customer'
import { Order } from "../../classes/order";
import { Exception } from "../../classes/exception";
import { CustomerPage } from "../customer/customer";
import { Customer } from "../../classes/customer";
import { TranslateService } from "@ngx-translate/core";
import { SelectProductPage } from "../select-product/select-product";
import { IntegratorLotProduct, IntegratorProduct, PaymentPlan, Product, ProductDetails } from "../../models/models";
import { OrderReportPage } from "../order-report/order-report";
import { ProposalTypePage } from '../proposal-type/proposal-type';
import { ProposalReceiptPage } from '../proposal-receipt/proposal-receipt';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { isArray } from 'util';


@Component({
  selector: 'page-proposal',
  templateUrl: 'proposal.html',
})
export class ProposalPage implements OnInit {

  //settings: Settings = new Settings();
  stringDataType = "2";

  // açıklama
  productDetails: ProductDetails;
  CustomerNameRececipt: any;
  CustomerCodeRececipt: any;
  place: number = 0;
  CustomerRefNRececipt: any;
  CustomerDipRececipt: any;
  CustomerTaxTRececipt: any[] = [];
  CustomerProdRececipt: any = [];
  CustomerInfoRececipt: string;
  CustomerPaidRececipt: number = 0;
  productColorSizeMatrix: any[] = [];
  PriceGroupCode: any;
  infoText: any;
  sizes: any;
  plans: PaymentPlan[] = new Array();
  taxPlans: any[] = new Array();
  monthly: number = 0;
  total: number = 0;
  realQty: number = 0;
  data: boolean;
  paid: number = 0;
  checkdatabase: number = 0;
  remain: number = 0;
  orderTypeName: string;
  imageText: string;
  base64Image: string;
  loading: Loading;
  currentPlanCode: any = "";
  currentPlanCodeArrayCount: number = 0;
  currentPlanCodeArray: any = [];
  currentUsedBarcodeArray: any = [];
  installementCount: number = 1;
  checkbutton: boolean = false;
  firstcurrent: boolean = true;
  customerMoneyPoint: string = '';



  paymentPlanGrCodeList: any[] = new Array();

  barcodeData = {
    "ProcName": "G3_GetProductPriceByBarcode",
    "Parameters": [{ "Name": "Barcode", "Value": "" }, { "Name": "PriceGroupCode", "Value": "" }]
  };
  storeData = { "ProcName": "", "Parameters": [{ "Name": "ItemCode", "Value": "" }, { "Name": "WarehouseCode", "Value": "" }] };
  planData = { "ProcName": "G3_GetPaymentPlan", "Parameters": [{ "Name": "Language", "Value": "tr" }, { "Name": "ForCreditCardPlan", "Value": "" }] };
  getOrderData = { "ProcName": "G3_GetOrderDetails", "Parameters": [{ "Name": "OrderNumber", "Value": "" }] };
  addData = { "ProcName": "G3_AddUserWarning", "Parameters": [{ "Name": "CurrAccCode", "Value": "" }, { "Name": "CurrAccPaid", "Value": "" }] };
  updateOrderData = { "ProcName": "G3_SetOrderIsCompleted", "Parameters": [{ "Name": "HeaderID", "Value": "" }] };
  updateInvoiceData = { "ProcName": "G3_SetInvoiceIsCompleted", "Parameters": [{ "Name": "HeaderID", "Value": "" }] };
  updateProposalData = { "ProcName": "G3_SetProposalData", "Parameters": [{ "Name": "HeaderID", "Value": "" }, { "Name": "Code", "Value": "" }] };
  updateOrderIsCompleted = { "ProcName": "G3_SetIsCreditTableConfirmed", "Parameters": [{ "Name": "HeaderID", "Value": "" }] };
  updatesetIsCreditSaleAndIsCompleted = { "ProcName": "G3_SetIsCreditSaleAndIsCompleted", "Parameters": [{ "Name": "HeaderID", "Value": "" }] };
  deleteTrOrderPaymentPlan = { "ProcName": "G3_DeleteTrOrderPaymentPlan", "Parameters": [{ "Name": "HeaderID", "Value": "" }] };
  /*updatesetCompanyCode = { "ProcName": "G3_SetCompanyCode", "Parameters": [{ "Name": "HeaderID", "Value": "" }] };*/
  updateSalesPerson = { "ProcName": "", "Parameters": [{ "Name": "HeaderID", "Value": "" }, { "Name": "Salespersoncode", "Value": "" }] };

  /*updateOrderNumber= { "ProcName": "sp_LastRefNumProcess", "Parameters": [{ "Name": "CompanyCode", "Value": "2" }, { "Name": "ProcessCode", "Value": "WS" }, { "Name": "ProcessFlowCode", "Value": "2" }] };*/

  paymentPlanGrCode = {
    "ProcName": "G3_GetPaymentPlanByItemCode",
    "Parameters": [{ "Name": "Language", "Value": "" }, { "Name": "PriceGroupCode", "Value": "" }, { "Name": "ItemCode", "Value": "" }]
  };
  addProposalReport = {
    "ProcName": "G3_AddProposalReport",
    "Parameters":
      [
        { "Name": "HeaderID", "Value": "" },
        { "Name": "LineID", "Value": "" },
        { "Name": "StoreCode", "Value": "" },
        { "Name": "ProcessCode", "Value": "" },
        { "Name": "ApplicationCode", "Value": "" },
        { "Name": "CurrAccCode", "Value": "" },
        { "Name": "Number", "Value": "" },
        { "Name": "Date", "Value": "" },
        { "Name": "Time", "Value": "" },
        { "Name": "ItemCode", "Value": "" },
        { "Name": "ColorCode", "Value": "" },
        { "Name": "ItemDim1Code", "Value": "" },
        { "Name": "Qty1", "Value": {} },
        { "Name": "SalespersonCode", "Value": "" },
        { "Name": "Price", "Value": "" },
        { "Name": "CreatedUserName", "Value": "" },
        { "Name": "@IsCompleted", "Value": false }
      ]
  };

  orderData = {
    "ModelType": 6,
    "CustomerCode": "",
    "OfficeCode": "",
    "StoreCode": "",
    "PosTerminalID": "0", // TODO add it to settings : OK
    "IsCompleted": false,
    "IsSuspended": false,
    "IsCreditSale": false,
    "Lines": [],
    "SumLines": [],
    "PaymentTerm": 0,
    "TaxTypeCode": 0,
    "TDisRate1": 0,
    "StoreWareHouseCode": "",
    "WareHouseCode": ""
  };

  invoiceData = {
    "ModelType": 8,
    "CustomerCode": "",
    "PosTerminalID": "",
    "InvoiceDate": "",
    "Description": "GM",
    "ShipmentMethodCode": 2,
    "CompanyCode": "2",
    "OfficeCode": "",
    "WareHouseCode": "",
    "StoreCode": "",
    "Lines": [],
    "SuppressItemDiscount": false,
    "IsCompleted": false,
    "IsSuspended": true,
    "IsSalesViaInternet": false,
    "ApplyCampaign": true,
  }

  //"SalespersonCode":"", // TODO add it to settings

  paymentsData = [{
    "PaymentType": "1",  //TODO Move it to setting
    "Code": "",  //TODO Move it to setting  : OK
    "CreditCardTypeCode": "",
    "InstallmentCount": "",
    "CurrencyCode": "TRY",
    "DownPayment": "0",
    "Amount": "0"
  }];

  discountData = [{
    "DiscountTypeCode": "",
    "Value": 0,
    "DiscountReasonCode": "",
    "IsPercentage": false
  }];

  getAvailableInventoryV3 = {
    "ProcName": "G3_GetAvailableInventory", "Parameters": [{ "Name": "ItemCode", "Value": "" },
    { "Name": "ColorCode", "Value": "" }]
  };

  AddSerialInvoice = { "ProcName": "G3_AddSerialInvoice", "Parameters": [{ "Name": "InvoiceLineID", "Value": "" }, { "Name": "SerialNumber", "Value": "" }] };
  AddItemSerial = { "ProcName": "G3_AddItemSerial", "Parameters": [{ "Name": "SerialNumber", "Value": "" }] };

  customerPointData = {
    "ProcName": "G3_GetCustomerPoint", "Parameters": [
      { "Name": "CurrAccTypeCode", "Value": "" }, { "Name": "CurrAccCode", "Value": "" },
      { "Name": "OperationDate", "Value": "" }, { "Name": "OperationTime", "Value": "" },
      { "Name": "ProcessCode", "Value": "" }, { "Name": "OfficeCode", "Value": "" },
      { "Name": "StoreCode", "Value": "" }, { "Name": "LangCode", "Value": "TR" }]
  }

  customer: Customer;

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    private barcodeScanner: BarcodeScanner, public serverService: ServerService, public navParams: NavParams,
    public modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController,
    public translateService: TranslateService, public toastCtrl: ToastController,
    private camera: Camera) {
    console.log(this.serverService.Settings.V3Settings.SalespersonPassword);
    this.taxPlans = [
      {
        "name": "Standart",
        "val": 0
      },
      {
        "name": "Vergisiz",
        "val": 4
      },
    ]
    // this.settings.V3Settings = new V3Settings();customer
    this.data = this.navParams.data.res;
    this.customer = this.navParams.data.customer;
    //console.log(this.customer);
    //JS if (this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2') {
    if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
      this.checkdatabase = 1;
    }
  }
  ionViewWillEnter() {
    var temp = 0;
    for (let i = 0; i < this.serverService.Settings.G3License.SalesTypes.length; i++) {
      if (this.serverService.Settings.G3License.SalesTypes.charAt(i) == "1") {
        temp++;
      }
    }
    if (temp > 1) {
      this.checkbutton = true;
      this.goToProposalTypePage();
    }
    if (this.data == true) {
      this.CustomerNameRececipt = this.customer.FirstName + " " + this.customer.LastName;
    }
  }

  ngOnInit() {

    this.presentLoading();
    console.log("Çalıştı");
    this.getOrderTypeBySaleType();

    if (this.serverService.Settings.V3Settings.IsProductPriceByGrCode && this.serverService.Settings.V3Settings.SalesType == 3) {
      this.stringDataType = '3';
    }
  }

  ionViewDidLoad() {
    this.dismissLoading();
  }

  handleError(error: any) {
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string) => {
      this.presentAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
  }

  getOrderTypeBySaleType() {
    if (this.checkdatabase == 1) {
      if (this.serverService.Settings.V3Settings.SalesType == 1) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Toptan Satış";
      else if (this.serverService.Settings.V3Settings.SalesType == 2) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Perakende Satış";
      else if (this.serverService.Settings.V3Settings.SalesType == 3) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Taksitli Satış";
      else if (this.serverService.Settings.V3Settings.SalesType == 4) this.planData.Parameters[1].Value = '0', this.orderTypeName = "İhracat Satış";
      else if (this.serverService.Settings.V3Settings.SalesType == 5) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Peşin Satış (Hemen Teslim)";
      else if (this.serverService.Settings.V3Settings.SalesType == 6) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Peşin Satış (Sonra Teslim)";
    } else {
      if (this.serverService.Settings.V3Settings.SalesType == 1) this.planData.Parameters[1].Value = '1', this.orderTypeName = "Toptan Satış";
      else if (this.serverService.Settings.V3Settings.SalesType == 2) this.planData.Parameters[1].Value = '1', this.orderTypeName = "Perakende Satış";
      else if (this.serverService.Settings.V3Settings.SalesType == 3) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Taksitli Satış";
      else if (this.serverService.Settings.V3Settings.SalesType == 4) this.planData.Parameters[1].Value = '1', this.orderTypeName = "İhracat Satış";
      else if (this.serverService.Settings.V3Settings.SalesType == 5) this.planData.Parameters[1].Value = '1', this.orderTypeName = "Peşin Satış (Hemen Teslim)";
      else if (this.serverService.Settings.V3Settings.SalesType == 6) this.planData.Parameters[1].Value = '1', this.orderTypeName = "Peşin Satış (Sonra Teslim)";
    }
    this.serverService.getPaymentPlan(this.planData).then(res => {
      console.log(res);
      if (this.checkdatabase == 1) {
        this.plans = res.filter(x => x.PaymentPlanCode == "€CT" || x.PaymentPlanCode == "€DP" || x.PaymentPlanCode == "$CT" || x.PaymentPlanCode == "$DP" || x.PaymentPlanCode == "$FT");
      } else {
        this.plans = res;
      }
    }).catch(this.handleError);
  }

  getPriceGroup(item: string) {
    if(this.serverService.Settings.G3Settings.Currency != ''){
      this.PriceGroupCode = '';
      return this.serverService.Settings.G3Settings.Currency;
    }
    var tempPriceGroupCode: string;
    if (item == null) {
      tempPriceGroupCode = '€'
      this.PriceGroupCode = 'EUR'
      return tempPriceGroupCode;
    } else {
      if (item.charAt(0) == "€") {
        tempPriceGroupCode = '€'
        this.PriceGroupCode = 'EUR'
        return tempPriceGroupCode;
      } else if (item.charAt(0) == "$") {
        tempPriceGroupCode = '$'
        this.PriceGroupCode = 'USD'
        return tempPriceGroupCode;
      } else {
        tempPriceGroupCode = '₺'
        this.PriceGroupCode = ''
        return tempPriceGroupCode;
      }
    }
  }

  scanBarcode() {
    //8680434455120
    //8698536442375
    //8698536470880
    //this.addProduct("3330000289134");  

    this.serverService.isOpenCamera = true;
    try {
      this.barcodeScanner.scan().then(barcodeData => {
        console.log('Barcode data', barcodeData);
        this.addProduct(barcodeData.text);
        this.serverService.isOpenCamera = false;

      }).catch(err => {
        console.log('Error', err);
        alert(JSON.stringify(err));
        this.serverService.isOpenCamera = false;
      });
    }
    catch (err) {
      console.log('Barcode err: ', err);
      alert(JSON.stringify(err));
      this.serverService.isOpenCamera = false;
    }
  }

  scanSerialNumber(product: Product) {
    this.serverService.isOpenCamera = true;
    try {
      this.barcodeScanner.scan().then(barcodeData => {
        this.serverService.isOpenCamera = false;
        product.seriNumber = barcodeData.text;
      }).catch(err => {
        console.log('Error', err);
        this.serverService.isOpenCamera = false;
      });
    }
    catch (err) {
      this.serverService.isOpenCamera = false;
      console.log('Serial err: ', err);
    }
  }

  // recognizeImage(product: Product){
  //   Tesseract.recognize(this.base64Image)
  //   .then(result => {
  //     this.imageText = result.text;
  //     product.seriNumber = this.imageText;      
  //     this.base64Image = "";
  //     console.log(product.seriNumber);
  //   })
  //   .catch(err => {
  //     console.error(err);
  //   })
  // }
  getCustomerPoint(customer) {
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    console.log(date + ' ' + time)
    this.customerPointData.Parameters[0].Value = customer.CurrAccTypeCode;
    this.customerPointData.Parameters[1].Value = customer.CurrAccCode;
    this.customerPointData.Parameters[2].Value = date + ' ' + time;
    this.customerPointData.Parameters[3].Value = time;
    this.customerPointData.Parameters[4].Value = 'R'
    this.customerPointData.Parameters[5].Value = this.serverService.Settings.V3Settings.OfficeCode;
    this.customerPointData.Parameters[6].Value = this.serverService.Settings.V3Settings.StoreCode;
    this.serverService.getAny(this.customerPointData)
      .then(res => {
        console.log('remaining point', res)
        if(isArray(res) && res.length > 0){
          let alertSubTitle = 'Birikmiş Puan:' + ' ' + res[0].RemainingPoint + ' ' + '₺';
          this.customerMoneyPoint = '(' + res[0].RemainingPoint + ' ' + '₺' + ')';
          this.presentAlert('Bilgi', alertSubTitle);
        }        
      })

  }

  selectProduct() {
    let modal;
    modal = this.modalCtrl.create(SelectProductPage);
    modal.present();
    modal.onDidDismiss(data => {
      console.log(data)
      if (data.result) {
        if (this.serverService.Settings.V3Settings.SalesType != 3) {
          this.onPlanChanged(event);
        } else {
          this.onPlanChangedByProduct(Product, event);

        }
      }
    });
  }

  goToProposalTypePage() {
    let modal;
    modal = this.modalCtrl.create(ProposalTypePage, { text: 'Sipariş Kategori Tipi Seçiniz' });
    modal.present();
    modal.onDidDismiss(data => {
      // console.log(data)
      if (data.type == null) {
        data.type = this.serverService.Settings.V3Settings.SalesType;
        // console.log(data.type)
      }
      if (data.type != false) {
        //this.presentLoading();
        this.serverService.Settings.V3Settings.SalesType = data.type;
        if (this.checkdatabase == 1) {
          if (data.type == 1) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Toptan Satış";
          else if (data.type == 2) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Perakende Satış";
          else if (data.type == 3) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Taksitli Satış";
          else if (data.type == 4) this.planData.Parameters[1].Value = '0', this.orderTypeName = "İhracat Satış";
          else if (data.type == 5) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Peşin Satış (Hemen Teslim)";
          else if (data.type == 6) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Peşin Satış (Sonra Teslim)";
        } else {
          if (data.type == 1) this.planData.Parameters[1].Value = '1', this.orderTypeName = "Toptan Satış";
          else if (data.type == 2) this.planData.Parameters[1].Value = '1', this.orderTypeName = "Perakende Satış";
          else if (data.type == 3) this.planData.Parameters[1].Value = '0', this.orderTypeName = "Taksitli Satış";
          else if (data.type == 4) this.planData.Parameters[1].Value = '1', this.orderTypeName = "İhracat Satış";
          else if (data.type == 5) this.planData.Parameters[1].Value = '1', this.orderTypeName = "Peşin Satış (Hemen Teslim)";
          else if (data.type == 6) this.planData.Parameters[1].Value = '1', this.orderTypeName = "Peşin Satış (Sonra Teslim)";
        }
        console.log(data.type)
        this.stringDataType = data.type;
        this.onPlanChanged(event);
        // this.serverService.getPaymentPlan(this.planData).then(res => {    //GEREK YOK 
        //  

        //   console.log(res);
        //   this.plans = res;
        //   this.onPlanChanged(event);
        //   this.dismissLoading();
        // }).catch(this.handleError);
      }
    });
  }

  //G3_GetProductsInventory (@ItemCode AS VARCHAR(50), @ColorCode AS VARCHAR(50), @ItemDim1Code AS VARCHAR(50), @WarehouseCode AS VARCHAR(50))
  addProduct(barcode) {
    this.barcodeData.Parameters[0].Value = barcode;
    this.barcodeData.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
    console.log(this.barcodeData);
    this.presentLoading();
    this.serverService.getProductPrice(this.barcodeData).then(res => {
      console.log(res);
      if (res == null) {
        this.translateService.get(['PROPOSAL_PRODUCT_NOT_FOUND_TEXT']).subscribe((value: string) => {
          this.presentToast(value['PROPOSAL_PRODUCT_NOT_FOUND_TEXT']);
        });
      } else {

        let product = new Product();
        product = res;
        product.Price = this.getCurrentPrice(product.PriceList);
        //product.UseSerialNumber = res.UseSerialNumber;
        product.UsedBarcode = barcode;
        product.LDisRate1 = 0;
        product.ColorCode = res.ColorCode;
        product.ItemDim1Code = res.ItemDim1Code;
        //let max: number = this.sizes[col.ItemDim1Code];
        //product.MaxQty = max;             
        console.log(product)
        var temp: any = [];
        if (this.checkdatabase == 1) {
          this.currentPlanCode = product.PaymentPlanCode;
          temp = product.PriceList.filter(x => x.PaymentPlanCode == this.currentPlanCode);
          console.log(temp);
          product.Price = temp[0]['Price'];
          this.firstcurrent = false;
        }
        this.pushProduct(product);
        this.calculate();
      }
      this.dismissLoading();
    }).catch(this.handleError);
    console.log('current plan code', this.currentPlanCode);
  }

  pushProduct(product: Product) {
    for (let item of this.serverService.Items) {
      if (item.ItemCode == product.ItemCode && item.UsedBarcode == product.UsedBarcode) {
        item.Qty1++;
        return
      }
    }

    console.log(product.ItemCode);
    this.storeData.ProcName = "G3_GetProductDetails";
    this.storeData.Parameters[0].Value = product.ItemCode;
    this.storeData.Parameters[1].Value = (this.serverService.Settings.V3Settings.WarehouseCode) ? this.serverService.Settings.V3Settings.WarehouseCode : "";
    this.serverService.getAny(this.storeData).then(res => {
      console.log('G3_GetProductDetails');
      console.log(res);
      this.productDetails = res[0];
      product.UseSerialNumber = res[0].UseSerialNumber;
      if (this.checkdatabase == 1) {
        this.storeData.ProcName = "G3_GetProductColorSizeMatrix3";
        this.storeData.Parameters[0].Value = product.ItemCode;
        this.storeData.Parameters[1].Value = (this.serverService.Settings.V3Settings.WarehouseCode) ? this.serverService.Settings.V3Settings.WarehouseCode : "";
        this.serverService.getAny(this.storeData).then(res => {
          //console.log(res);       
          this.productColorSizeMatrix = res;
          this.sizes = this.productColorSizeMatrix.find(x => x['R/B'] == product.ColorCode);
          product.Content = this.productDetails.ProductAtt08Desc ? this.productDetails.ProductAtt08Desc : '';
          /* JS Kaldirldi.
          if (this.checkdatabase == 1) {
            this.getAvailableInventoryV3.Parameters[0].Value = product.ItemCode;
            this.getAvailableInventoryV3.Parameters[1].Value = product.ColorCode;
            this.serverService.getAny(this.getAvailableInventoryV3).then(res => {
              console.log('G3_getAvailableInventory');
              console.log(res);              
              product.MaxQty = res[0]['AvailableInventoryQty1'];                       
            }).catch(error => this.handleError(error));
          } else {
            product.MaxQty = this.sizes[product.ItemDim1Code];
          } */
          product.MaxQty = this.sizes[product.ItemDim1Code];
          console.log(this.sizes);
        })
        product.MesureCode = this.productDetails.MesureCode;
      } else {
        product.MesureCode = res[0].MesureCode;
      }
      //product.UseSerialNumber = this.productDetails.UseSerialNumber;
      //product.MesureCode = this.productDetails.UnitOfMeasureCode1;
    }).catch(error => this.handleError(error));



    this.serverService.Items.push(product);
  }

  removeProduct(index: number) {
    this.serverService.Items.splice(index, 1);
    this.calculate();
  }

  editProduct(index: number, data: any) {
    console.log(data);

    if (this.serverService.Settings.V3Settings.DiscountType == 0) {      
      this.presentToast('Ayarlarinizi kontrol ediniz, bu islem yapamazsiniz!')
      return;
    }

    this.serverService.Items[index].Qty1 = this.serverService.Items[index].Qty1;
    // JS
    //if (this.serverService.Settings.V3Settings.DiscountType == 0) {
    //  this.serverService.Items[index].Price = data.price;
    //} else {
      if (data.price > 0) {
        //product.LDisRate1*product.Price/100 
        //product.Price - product.LDisRate1*product.Price/100
        let discount: number = data.price / this.serverService.Items[index].Price * 100;
        console.log('discount : ' + discount);
        this.serverService.Items[index].LDisRate1 = discount;
        this.serverService.Items[index].RatePrice = data.price;
      } else {
        this.serverService.Items[index].LDisRate1 = data.pprice;
        this.serverService.Items[index].RatePrice = data.pprice * this.serverService.Items[index].Price / 100;
        console.log(this.serverService.Items);
      }
    //}
    this.calculate();
  }

  //12H647606725 80 50 30 
  //product.LDisRate1*product.Price/100

  editProductNew(index: number, data: any) {
    if (this.serverService.Settings.V3Settings.DiscountType == 1) {      
      this.presentToast('Ayarlarinizi kontrol ediniz, bu islem yapamazsiniz!')
      return;
    }

    this.serverService.Items[index].Price = data.price;
    this.calculate();
  }

  selectOrder() {
    let modal;
    modal = this.modalCtrl.create(OrderReportPage, { nextPage: 'proposal' });
    modal.present();
    modal.onDidDismiss(data => {
      if (data) {
        console.log(data);
        this.presentToast('Bu aşamadan sonra siparş oluşturursanız, yeni bir sipariş oluşturur.');
        this.getOrderData.Parameters[0].Value = data;
        this.serverService.getAny(this.getOrderData)
          .then(res => {
            console.log(res);
            if (res) {              
              this.serverService.Items = [];
              for (let item of res) {
                let product = new Product();
                product = item;
                this.pushProduct(product);
                this.orderData.TDisRate1 = item.TDisRate1;
              }
              this.setCurrentPriceAll();
              this.calculate();
            }
          })
          .catch(this.handleError);
      }
    });
  }

  setDiscount(data: any) {/*
        this.discountData[0].DiscountTypeCode = this.serverService.Settings.V3Settings.DiscountTypeCode;
        this.discountData[0].Value = data.discount;
        this.discountData[0].DiscountReasonCode = this.serverService.Settings.V3Settings.DiscountReasonCode;
        this.discountData[0].IsPercentage = this.serverService.Settings.V3Settings.IsPercentage;*/
    this.orderData.TDisRate1 = data.discount;
    console.log(this.discountData);
  }

  getCurrentPrice(list) {
    // console.log(this.customer)
    // if (this.customer) {
    //   for (let pp of list) {
    //     if (this.customer.WholesalePriceGroupCode == pp.PriceGroupCode)
    //       console.log(pp.Price)
    //     return pp.Price;
    //   }
    // }

    for (let pp of list) {
      if (pp.PaymentPlanCode == this.currentPlanCode)
        return pp.Price;
      console.log(pp.price)
    }

  }

  setCurrentPriceAll() {
    if (this.serverService.Settings.V3Settings.IsProductPriceByGrCode == false || this.serverService.Settings.V3Settings.IsProductPriceByGrCode == true && this.serverService.Settings.V3Settings.SalesType != 3) {
      for (let product of this.serverService.Items) {
        if (this.customer) {
          for (let pp of product.PriceList) {
            if (this.customer.WholesalePriceGroupCode == pp.PriceGroupCode) {
              product.Price = pp.Price;
              console.log(product.Price)
              return;
            }
          }
        }
        for (let pp of product.PriceList) {
          if (pp.PaymentPlanCode == this.currentPlanCode) {
            product.Price = pp.Price;
            console.log(product.Price)
            return;
          }
        }
      }
    }
  }

  calculate() {
    this.calculateTotal();
    this.calculateInstallment();
    this.calculateRemain(this.paid);
  }

  calculateTotal() {
    this.total = 0;
    for (let p of this.serverService.Items) {
      this.total += (p.Price - p.LDisRate1 * p.Price / 100) * p.Qty1;
    }
    this.total = this.calculateFloatTotal(this.total);
    if (this.orderData.TDisRate1 > 0) {
      this.total = this.total - (this.total * this.orderData.TDisRate1 / 100);
    }
    if(this.total == undefined || this.total == Number.NaN) this.total = 0;
  }

  calculateFloatTotal(test: number) {
    var tempstring = test.toString();
    var indexDot = tempstring.indexOf(".");
    if (indexDot != -1) {
      return test = parseFloat(tempstring.substring(0, indexDot + 2));
    } else {
      return test;
    }
  }

  calculateInstallment() {

    for (let plan of this.plans) {
      if (plan.PaymentPlanCode == this.currentPlanCode) {
        this.installementCount = plan.InstallmentCount;

        break;
      }
    }

    //this.monthly = (this.total - this.paid) / this.installementCount;
    // console.log('total', this.total);
    // console.log('paid', this.paid);
    // console.log('installementCount', this.installementCount)
    var tempMonthly = ((this.total - this.paid) / this.installementCount).toString();
    var indexDot = tempMonthly.indexOf(".");
    // console.log('tempMonthly', tempMonthly);
    // console.log('indexDot', indexDot);
    if (indexDot == -1) {
      this.monthly = (this.total - this.paid) / this.installementCount;
    } else {
      this.monthly = parseFloat(tempMonthly.substring(0, indexDot + 2));
    }

  }

  calculateRemain(paid: number) {
    if (this.paid > this.total) {
      this.paid = this.total
    }
    this.remain = Number.parseFloat((this.total - this.paid).toFixed(1));
    this.calculateInstallment();
  }

  getTotal() {
    this.calculate();
    return this.total;
  }

  setCustomerOperations() {
    if (!this.customer)
      this.selectCustomer();
    else
      this.presenCustomertActionSheet();
  }

  selectCustomer() {
    const modal = this.modalCtrl.create(SelectCustomerPage);
    modal.present();
    modal.onDidDismiss(
      data => {
        if (data.customer) {
          this.getCustomerPoint(data.customer);
          this.customer = data.customer;
          //CustomerName
          this.CustomerNameRececipt = this.customer.CustomerName ? this.customer.CustomerName : this.customer.FirstName + ' ' + this.customer.LastName;
          console.log(this.CustomerNameRececipt);
          this.CustomerCodeRececipt = this.customer.CurrAccCode;
          if (this.checkdatabase == 0) {
            this.setCurrentPriceAll();
            this.onPlanChangedByProduct(Product, event);
          }
        }
      }
    );
  }

  showCustomerDetail() {
    const modal = this.modalCtrl.create(CustomerPage, { customer: this.customer, showBack: true });
    modal.present();
    modal.onDidDismiss(
      data => {
        console.log('proposal.showCustomerDetails:');
      }
    );
  }

  goToReceiptPage() {
    this.serverService.isOpenCamera = true;
    console.log("info: " + this.infoText);
    this.CustomerInfoRececipt = this.infoText;
    let modal;
    modal = this.modalCtrl.create(ProposalReceiptPage, {
      customer: this.CustomerNameRececipt,
      customercode: this.CustomerCodeRececipt, refCode: this.CustomerRefNRececipt,
      products: this.CustomerProdRececipt, info: this.CustomerInfoRececipt, paid: this.CustomerPaidRececipt,
      priceGroupCode: this.PriceGroupCode, tax: this.CustomerTaxTRececipt, diptax: this.CustomerDipRececipt
    });
    modal.present();
    modal.onDidDismiss(data => {
      this.serverService.isOpenCamera = false;
    });
  }

  alertReceipt() {
    let alert = this.alertCtrl.create({
      title: "Siparişiniz başarılı bir şekilde oluşturulmuştur",
      buttons: [
        {
          text: 'Tamam',
          handler: data => {
            this.goToReceiptPage()
          }
        },
      ]
    });
    alert.present();
  }

  makeOrder() {
    //askıda sipariş

    if (this.serverService.Settings.V3Settings.IsSuspended) {
      this.orderData.IsSuspended = true;
    } else {
      this.orderData.IsSuspended = false
    }

    this.orderData.ModelType = this.getModelType()
    console.log(this.orderData.ModelType);
    if (this.orderData.ModelType == 5 || this.orderData.ModelType == 14) {

      delete this.orderData.StoreWareHouseCode;//
      delete this.orderData.StoreCode;//

      this.orderData.IsCreditSale = false;
      this.orderData.OfficeCode = this.serverService.Settings.V3Settings.OfficeCode;//
      //this.orderData.StoreCode = '';      
      this.orderData.PaymentTerm = this.serverService.Settings.V3Settings.PaymentTerm;
      this.orderData.WareHouseCode = this.serverService.Settings.V3Settings.WarehouseCode;
    } else if (this.orderData.ModelType == 6) {// Peşin Sonradan Teslim

      delete this.orderData.WareHouseCode;

      this.orderData.IsCreditSale = false;
      //this.orderData.SalespersonCode = this.serverService.Settings.SalespersonCode;
      this.orderData.OfficeCode = this.serverService.Settings.V3Settings.OfficeCode;
      this.orderData.StoreCode = this.serverService.Settings.V3Settings.StoreCode;
      this.orderData.StoreWareHouseCode = this.serverService.Settings.V3Settings.WarehouseCode;
      this.orderData.PosTerminalID = this.serverService.Settings.V3Settings.POSTerminalID;

    } else if (this.orderData.ModelType == 8) {//Peşin Hemen Teslim (FATURA)

      //this.orderData.SalespersonCode = this.serverService.Settings.SalespersonCode;
      this.invoiceData.OfficeCode = this.serverService.Settings.V3Settings.OfficeCode;
      this.invoiceData.StoreCode = this.serverService.Settings.V3Settings.StoreCode;
      this.invoiceData.WareHouseCode = this.serverService.Settings.V3Settings.WarehouseCode;
      this.invoiceData.PosTerminalID = this.serverService.Settings.V3Settings.POSTerminalID;

      if (this.serverService.Settings.Integrator.DatabaseName == "Demo_v3") this.invoiceData.CompanyCode = "1"

    } else if (this.orderData.ModelType == 16) { // taksitli

      //şüpheli müşteri
      if (this.customer.DebtStatusTypeCode == 0) {
        // Sipariş Atabilir
      } else {
        if (this.customer.DebtStatusTypeCode == 1 || this.customer.DebtStatusTypeCode == 2 || this.customer.DebtStatusTypeCode == 3 || this.customer.DebtStatusTypeCode == 4) {
          if (this.customer.BadDebtReasonCode == '01' || this.customer.BadDebtReasonCode == '04' || this.customer.BadDebtReasonCode == '06') {
            console.log(this.customer.DebtStatusTypeCode);
            console.log(this.customer.BadDebtReasonCode);
            if (this.serverService.Settings.V3Settings.SalesType == 3) { // taksitli
              this.presentAlert('Uyarı', 'Müşteri Şüpheli Durumda,  Sadece Peşin Satış Yapılabilir.');
              return;
            } else {
              this.presentToast('Müşteri Şüpheli Durumda,  Sadece Peşin Satış Yapılabilir.');
            }
          }
        }
      }

      delete this.orderData.WareHouseCode;

      this.orderData.IsCreditSale = true;
      //this.orderData.SalespersonCode = this.serverService.Settings.SalespersonCode;
      this.orderData.OfficeCode = this.serverService.Settings.V3Settings.OfficeCode;
      this.orderData.StoreCode = this.serverService.Settings.V3Settings.StoreCode;
      this.orderData.StoreWareHouseCode = this.serverService.Settings.V3Settings.WarehouseCode;
      this.orderData.PosTerminalID = this.serverService.Settings.V3Settings.POSTerminalID;
      this.paymentsData[0].InstallmentCount = this.installementCount.toString();
    }
    if (this.orderData.ModelType == 8) {
      // tarih ayarı
      let myday: string = "";
      let mymonth: string = "";
      let myyear: string = "";
      let numberMonth: number = 0;
      let fullDate: string = "";
      myyear = new Date().getFullYear().toString();
      myday = new Date().getDate().toString();
      numberMonth = new Date().getMonth();
      numberMonth++;
      if (numberMonth < 10) {
        mymonth = "0" + numberMonth.toString();
      } else {
        mymonth = numberMonth.toString();
      }
      fullDate = myyear + '/' + mymonth + '/' + myday;
      //fullDate ='2019/07/03'
      //console.log("fullDate", fullDate);

      this.invoiceData.InvoiceDate = fullDate;
      // this.invoiceData.SalesViaInternetInfo.PaymentDate = fullDate;
      // this.invoiceData.SalesViaInternetInfo.SendDate = fullDate;
      this.invoiceData.CustomerCode = this.customer.CurrAccCode;
      this.invoiceData.Lines = [];
      for (let p of this.serverService.Items) {
        console.log(p.seriNumber);
        console.log(p.BarcodeType);
        switch (p.BarcodeType) {
          // normal barcode
          case 1:
            let ip: IntegratorProduct = new IntegratorProduct();
            ip.UsedBarcode = p.UsedBarcode;
            ip.ItemTypeCode = 1;
            ip.ProductSerialNumber = p.seriNumber;
            console.log(ip.ProductSerialNumber);

            // İhracat ise VI = VD olmasın
            if (this.serverService.Settings.V3Settings.SalesType == 4) ip.Price = p.Price;
            else ip.PriceVI = p.Price;

            ip.Qty1 = p.Qty1;


            if (this.stringDataType != '3') {
              ip.PaymentPlanCode = this.currentPlanCode;
            } else {
              for (var i = 0; i <= this.currentUsedBarcodeArray.length; i++) {
                if (this.currentUsedBarcodeArray[i] == ip.UsedBarcode) {
                  ip.PaymentPlanCode = this.currentPlanCodeArray[i];
                }
              }
            }

            ip.LDisRate1 = p.LDisRate1;
            this.invoiceData.Lines.push(ip);
            break;
          // lot barcode
          case 2:
            let ilp: IntegratorLotProduct = new IntegratorLotProduct();
            ilp.ItemTypeCode = 1;
            ilp.LotBarcode = p.UsedBarcode;
            ilp.Qty1 = p.Qty1;
            this.orderData.SumLines.push(ilp);
            break;
        }
      }
      if (this.discountData[0].Value > 0) {
        this.orderData['Discounts'] = this.discountData;
      }

      this.presentLoading();

      this.serverService.makeOrder(this.invoiceData)
        .then(res => {

          //this.addItemSerial('SR001693582');
          //this.addSerialInvoice(res['Lines'][0]['LineID'],'SR001693582')

          console.log(JSON.stringify(res));
          console.log(res)

          this.CustomerRefNRececipt = res['InvoiceNumber'];

          console.log(this.CustomerRefNRececipt);

          this.dismissLoading();
          let result: Order = Object.assign(new Order(), res);
          console.log(result.ModelType);
          if (result.ModelType == this.getModelType()) {
            if (this.serverService.Settings.V3Settings.SalesType == 5) {
              this.addCurrAccUserWarning();
              this.addProposalReportInvoice(result);
            }
            this.goToReceiptPage()
            this.setProposalData(result.HeaderID, result.OfficeCode);

            this.setSalesPersonData(result.HeaderID, this.serverService.Settings.V3Settings.SalespersonCode);

            this.setSalesPersonDataInvoice(result.HeaderID, this.serverService.Settings.V3Settings.SalespersonCode);
            this.reset();
          } else {
            let result: Exception = Object.assign(new Exception(), res);
            console.log("HATA");
            this.translateService.get('ALERT_ERROR_TITLE_TEXT').subscribe((value: string) => {
              this.presentAlert(value, this.serverService.getReadableMessage(result.ExceptionMessage));
            });
          }
        }).catch(this.handleError);
    }
    else {
      this.orderData.CustomerCode = this.customer.CurrAccCode;

      this.orderData.Lines = [];
      this.orderData.SumLines = [];
      for (let p of this.serverService.Items) {
        console.log(p.BarcodeType);
        switch (p.BarcodeType) {
          // normal barcode
          case 1:
            let ip: IntegratorProduct = new IntegratorProduct();
            ip.UsedBarcode = p.UsedBarcode;
            ip.ItemTypeCode = 1;

            if (this.checkdatabase == 1) {
              if (this.serverService.Settings.V3Settings.SalesType == 4) ip.Price = p.Price;
              else ip.Price = p.Price;
            } else {
              if (this.serverService.Settings.V3Settings.SalesType == 4) ip.Price = p.Price;
              else ip.PriceVI = p.Price;
            }

            ip.Qty1 = p.Qty1;


            if (this.stringDataType != '3') {
              ip.PaymentPlanCode = this.currentPlanCode;
            } else {
              for (let i = 0; i <= this.currentUsedBarcodeArray.length; i++) {
                if (this.currentUsedBarcodeArray[i] == ip.UsedBarcode) {
                  ip.PaymentPlanCode = this.currentPlanCodeArray[i];
                }
              }
            }

            ip.LDisRate1 = p.LDisRate1;
            console.log(ip);
            this.orderData.Lines.push(ip);
            console.log(this.orderData.Lines)
            break;
          // lot barcode
          case 2:
            let ilp: IntegratorLotProduct = new IntegratorLotProduct();
            ilp.ItemTypeCode = 1;
            ilp.LotBarcode = p.UsedBarcode;
            ilp.Qty1 = p.Qty1;
            console.log(ilp)
            this.orderData.SumLines.push(ilp);
            break;
        }
      }

      if (this.paid > 0) {
        /*
        this.orderData.IsCompleted = false;
        this.paymentsData[0].Amount = this.paid.toString();
        //this.paymentsData[0].DownPayment = "1";
        this.paymentsData[0].Code = this.serverService.Settings.V3Settings.PaymentCode;
        this.orderData['Payments'] = this.paymentsData;
        */
      }

      if (this.discountData[0].Value > 0) {
        this.orderData['Discounts'] = this.discountData;
      }
      console.log(this.orderData)

      for (var n = 0; n < this.orderData.Lines.length; n++) {
        console.log(this.orderData.Lines[n].Qty1);
        //this.orderData.Lines[n].Qty1 = this.orderData.Lines[n].Qty1.toString().replace('.', ','); //replace(/\./g, ',');
        this.addProposalReport.Parameters[12].Value = this.orderData.Lines[n].Qty1;
        console.log(this.orderData.Lines[n].Qty1);
      }

      this.presentLoading();
      this.serverService.makeOrder(this.orderData)
        .then(res => {

          console.log(JSON.stringify(res));
          console.log(res);

          this.CustomerRefNRececipt = res['OrderNumber'];
          this.CustomerTaxTRececipt = res['Lines'];
          this.CustomerDipRececipt = res['TDisRate1'];
          console.log(this.CustomerTaxTRececipt);
          console.log(this.CustomerRefNRececipt);
          console.log(this.CustomerDipRececipt);

          this.dismissLoading();
          let result: Order = Object.assign(new Order(), res);
          if (result.ModelType == this.getModelType()) {
            if (this.serverService.Settings.V3Settings.SalesType == 3 || this.serverService.Settings.V3Settings.SalesType == 6) {
              this.addCurrAccUserWarning();
              this.setOrderIsCompleted(result.HeaderID);
              this.addProposalReportOrder(result);
              this.setIsCreditableConfirmed(result.HeaderID);//taksitli satış istihbarat              
            }
            if (this.serverService.Settings.V3Settings.SalesType == 1 || this.serverService.Settings.V3Settings.SalesType == 4) {
              if (this.checkdatabase == 1) {
                this.setIsCreditSaleAndIsCompleted(result.HeaderID);
              }
            }

            if (this.serverService.Settings.V3Settings.SalesType != 3) {
              this.goToReceiptPage();
            } else {
              this.translateService.get(['ALERT_INFORMATION_TITLE_TEXT', 'PROPOSAL_ALERT_PROPOSAL_CREATED_SUCCESSFULLY_MESSAGE_TEXT']).subscribe((value: string[]) => {
                this.presentAlert(value['ALERT_INFORMATION_TITLE_TEXT'], value['PROPOSAL_ALERT_PROPOSAL_CREATED_SUCCESSFULLY_MESSAGE_TEXT']);
              });
            }
            this.setTrOrderPaymentPlan(result.HeaderID);
            this.setProposalData(result.HeaderID, result.OfficeCode);
            this.setSalesPersonData(result.HeaderID, this.serverService.Settings.V3Settings.SalespersonCode);
            this.setSalesPersonDataOthers(result.HeaderID, this.serverService.Settings.V3Settings.SalespersonCode);
            console.log(result.HeaderID + "\n" + this.serverService.Settings.V3Settings.SalespersonCode);
            this.reset();
          } else {
            let result: Exception = Object.assign(new Exception(), res);
            this.translateService.get('ALERT_ERROR_TITLE_TEXT').subscribe((value: string) => {
              this.presentAlert(value, this.serverService.getReadableMessage(result.ExceptionMessage));
            });
          }
        }).catch(this.handleError);

      console.log('saletype-ordertype', this.getOrderTypeBySaleType())

    }


  }



  addProposalReportOrder(result) {
    //askıda sipariş raporu START
    let ProcessCode;
    let orderNumber = result.OrderNumber
    for (let x = 0; x < orderNumber.length; x++) {
      if (orderNumber[x] == 'R') {
        if (orderNumber[x + 1] == 'I') {
          ProcessCode = orderNumber[x] + orderNumber[x + 1];
        } else {
          ProcessCode = orderNumber[x];
        }
      }
    }

    for (var n = 0; n < result.Lines.length; n++) {
      this.addProposalReport.Parameters[0].Value = result.HeaderID;
      this.addProposalReport.Parameters[1].Value = result.Lines[n].LineID;
      this.addProposalReport.Parameters[2].Value = result.StoreCode;
      this.addProposalReport.Parameters[3].Value = ProcessCode;
      this.addProposalReport.Parameters[4].Value = result.ApplicationCode;
      this.addProposalReport.Parameters[5].Value = result.CustomerCode;
      this.addProposalReport.Parameters[6].Value = result.OrderNumber;
      // this.addProposalReport.Parameters[7].Value = result.OrderDate;
      // this.addProposalReport.Parameters[8].Value = result.OrderTime;
      this.addProposalReport.Parameters[9].Value = result.Lines[n].ItemCode;
      this.addProposalReport.Parameters[10].Value = result.Lines[n].ColorCode;
      this.addProposalReport.Parameters[11].Value = result.Lines[n].ItemDim1Code;
      this.addProposalReport.Parameters[12].Value = result.Lines[n].Qty1;
      this.addProposalReport.Parameters[13].Value = result.Lines[n].SalespersonCode;
      this.addProposalReport.Parameters[14].Value = result.Lines[n].ActualPrice;
      this.addProposalReport.Parameters[15].Value = this.serverService.Settings.V3Settings.SalespersonCode;
      this.addProposalReport.Parameters[16].Value = false;

      this.serverService.getAny(this.addProposalReport);
    }
    // askıda sipariş raporu END
  }
  addProposalReportInvoice(result) {
    //askıda fatura raporu START
    let ProcessCode;
    let invoiceNumber = result.InvoiceNumber
    for (let x = 0; x < invoiceNumber.length; x++) {
      if (invoiceNumber[x] == 'R') {
        if (invoiceNumber[x + 1] == 'I') {
          ProcessCode = invoiceNumber[x] + invoiceNumber[x + 1];
        } else {
          ProcessCode = invoiceNumber[x];
        }
      }
    }

    for (var n = 0; n < result.Lines.length; n++) {
      this.addProposalReport.Parameters[0].Value = result.HeaderID;
      this.addProposalReport.Parameters[1].Value = result.Lines[n].LineID;
      this.addProposalReport.Parameters[2].Value = result.StoreCode;
      this.addProposalReport.Parameters[3].Value = ProcessCode;
      this.addProposalReport.Parameters[4].Value = result.ApplicationCode;
      this.addProposalReport.Parameters[5].Value = result.CustomerCode;
      this.addProposalReport.Parameters[6].Value = result.InvoiceNumber;
      // this.addProposalReport.Parameters[7].Value = result.OrderDate;
      // this.addProposalReport.Parameters[8].Value = result.OrderTime;
      this.addProposalReport.Parameters[9].Value = result.Lines[n].ItemCode;
      this.addProposalReport.Parameters[10].Value = result.Lines[n].ColorCode;
      this.addProposalReport.Parameters[11].Value = result.Lines[n].ItemDim1Code;
      this.addProposalReport.Parameters[12].Value = result.Lines[n].Qty1;
      this.addProposalReport.Parameters[13].Value = result.Lines[n].SalespersonCode;
      this.addProposalReport.Parameters[14].Value = result.Lines[n].ActualPrice;
      this.addProposalReport.Parameters[15].Value = this.serverService.Settings.V3Settings.SalespersonCode;
      this.addProposalReport.Parameters[16].Value = false;

      this.serverService.getAny(this.addProposalReport);
    }
    // askıda fatura raporu END
  }

  /*setOrderNumber(){
    //sp_LastRefNumProcess      
    this.serverService.getAny(this.updateOrderNumber).then(res => {
      console.log("sp_LastRefNumProcess");
      console.log(res);
    })
    .catch(this.handleError);
  }*/

  setProposalData(headerId, code) {
    this.updateProposalData.Parameters[0].Value = headerId;
    this.updateProposalData.Parameters[1].Value = code;
    this.serverService.getAny(this.updateProposalData);
  }

  setSalesPersonData(headerId, code) {
    this.updateSalesPerson.ProcName = "G3_SetSalesPersonData";
    this.updateSalesPerson.Parameters[0].Value = headerId;
    this.updateSalesPerson.Parameters[1].Value = code;
    this.serverService.getAny(this.updateSalesPerson);
  }
  setSalesPersonDataInvoice(headerId, code) {
    this.updateSalesPerson.ProcName = "G3_SetSalesPersonDataInvoice"
    this.updateSalesPerson.Parameters[0].Value = headerId;
    this.updateSalesPerson.Parameters[1].Value = code;
    this.serverService.getAny(this.updateSalesPerson);
  }

  setSalesPersonDataOthers(headerId, code) {
    this.updateSalesPerson.ProcName = "G3_SetSalesPersonData"
    this.updateSalesPerson.Parameters[0].Value = headerId;
    this.updateSalesPerson.Parameters[1].Value = code;
    this.serverService.getAny(this.updateSalesPerson);
  }

  setIsCreditableConfirmed(headerId) {
    this.updateOrderIsCompleted.Parameters[0].Value = headerId;
    this.serverService.getAny(this.updateOrderIsCompleted);
  }

  setIsCreditSaleAndIsCompleted(headerId) {
    this.updatesetIsCreditSaleAndIsCompleted.Parameters[0].Value = headerId;
    this.serverService.getAny(this.updatesetIsCreditSaleAndIsCompleted);
  }

  setOrderIsCompleted(headerId) {
    this.updateOrderData.Parameters[0].Value = headerId;
    this.serverService.getAny(this.updateOrderData);
  }

  setTrOrderPaymentPlan(headerId) {
    this.deleteTrOrderPaymentPlan.Parameters[0].Value = headerId;
    this.serverService.getAny(this.deleteTrOrderPaymentPlan);
  }

  /*setCompanyCode(headerId){         
    this.updatesetCompanyCode.Parameters[0].Value = headerId;    
    this.serverService.getAny(this.updatesetCompanyCode);
  }*/

  addSerialInvoice(InvoiceLineID, SerialNumber) {
    this.AddSerialInvoice.Parameters[0].Value = InvoiceLineID;
    this.AddSerialInvoice.Parameters[1].Value = SerialNumber;
    this.serverService.getAny(this.AddSerialInvoice);
  }

  addItemSerial(SerialNumber) {
    this.AddItemSerial.Parameters[0].Value = SerialNumber;
    this.serverService.getAny(this.AddItemSerial);
  }

  //AddItemSerial

  setInvoiceIsCompleted(headerId) {
    this.updateInvoiceData.Parameters[0].Value = headerId;
    this.serverService.getAny(this.updateInvoiceData);
  }

  addCurrAccUserWarning() {
    this.CustomerInfoRececipt = this.infoText;
    this.CustomerPaidRececipt = this.paid;
    if (this.infoText != null && this.paid > 0) {
      this.addData.Parameters[0].Value = this.orderData.CustomerCode;
      this.addData.Parameters[1].Value = 'Açıklama :' + this.infoText + ' ' + 'Pesinat : ' + this.paid;
      this.serverService.getAny(this.addData)
        .then(res => {
          console.log(res);

        })
        .catch(this.handleError);
      console.log(this.addData.Parameters[1].Value);
    }
    else if (this.infoText != null && (this.paid == 0 || this.paid == null)) {
      this.addData.Parameters[0].Value = this.orderData.CustomerCode;
      this.addData.Parameters[1].Value = 'Açıklama :' + this.infoText;
      this.serverService.getAny(this.addData)
        .then(res => {
          console.log(res);

        })
        .catch(this.handleError);
      console.log(this.addData.Parameters[1].Value);
    }
    else if (this.infoText == null && this.paid > 0) {
      this.addData.Parameters[0].Value = this.orderData.CustomerCode;
      this.addData.Parameters[1].Value = 'Pesinat : ' + this.paid;
      this.serverService.getAny(this.addData)
        .then(res => {
          console.log(res);

        })
        .catch(this.handleError);
      console.log(this.addData.Parameters[1].Value);
    }
    else if (this.infoText == null && (this.paid == 0 || this.paid == null)) {
      return;
    }
  }

  reset() {
    this.infoText = '';
    this.orderTypeName = '';
    this.currentPlanCodeArrayCount = 0;
    this.currentUsedBarcodeArray = [];
    this.currentPlanCodeArray = [];
    this.serverService.Items = [];
    this.orderData.TDisRate1 = 0;
    this.customer = null;
    this.currentPlanCode = "";
    this.monthly = 0;
    this.total = 0;
    this.remain = 0;
    this.paid = 0;
    this.getOrderTypeBySaleType();
  }

  /*
        None = 0,
        Login = 1,
        Customer = 2,
        RetailCustomer = 3,
        Product = 4,
        OrderWS = 5,
        OrderR = 6,
        InvoiceWS = 7,
        InvoiceR = 8,
        OrderBP = 9,
        OrderCP = 10,
        OrderCS = 11,
        OrderDS = 12,
        OrderEP = 13,
        OrderES = 14,
        OrderIP = 15,
        OrderRI = 16,
        OrderSS = 17,
        OrderST = 18,
        InvoiceBP = 19,
        InvoiceCP = 20,
        InvoiceCS = 21,
        InvoiceDS = 22,
        InvoiceEP = 23,
        InvoiceES = 24,
        InvoiceIP = 25,
        InvoiceRI = 26,
        InvoiceSS = 27,
   */

  getModelType() {
    /***
     * 1 toptan
     * 2 perakende
     * 3 taksit
     * 4 ihracat
     * 5 Peşin satış (hemen teslim)
     * 6 Peşin satış (sonra teslim)
     * 
     */
    if (this.serverService.Settings.V3Settings.SalesType == 1)
      return 5;
    else if (this.serverService.Settings.V3Settings.SalesType == 2)
      return 6;
    else if (this.serverService.Settings.V3Settings.SalesType == 3)
      return 16;
    else if (this.serverService.Settings.V3Settings.SalesType == 4)
      return 14;
    else if (this.serverService.Settings.V3Settings.SalesType == 5)
      return 8;
    else if (this.serverService.Settings.V3Settings.SalesType == 6)
      return 6;
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  presentAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'X'
    });
    toast.present();
  }

  presenCustomertActionSheet() {
    this.translateService.get(['PROPOSAL_ACTION_SHEET_BUTTON_CHANGE_CUSTOMER_TEXT', 'PROPOSAL_ACTION_SHEET_BUTTON_CUSTOMER_DETAIL_TEXT', 'PROPOSAL_ACTION_SHEET_BUTTON_ADD_GUARANTOR_TEXT', 'ALERT_INFORMATION_TITLE_TEXT', 'PROPOSAL_ACTION_SHEET_BUTTON_ADD_GUARANTOR_MESSAGE_TEXT', 'PROPOSAL_ACTION_SHEET_BUTTON_CANCEL_TEXT', 'PROPOSAL_ACTION_SHEET_TITLE_TEXT']).subscribe((value: string[]) => {
      let actionSheetButtons = [];

      actionSheetButtons.push({
        text: value['PROPOSAL_ACTION_SHEET_BUTTON_CHANGE_CUSTOMER_TEXT'],
        handler: () => {
          this.selectCustomer();
        }
      });

      actionSheetButtons.push({
        text: value['PROPOSAL_ACTION_SHEET_BUTTON_CUSTOMER_DETAIL_TEXT'],
        handler: () => {
          this.showCustomerDetail();
        }
      });

      if (this.serverService.Settings.V3Settings.IsGuarantor) {
        actionSheetButtons.push({
          text: value['PROPOSAL_ACTION_SHEET_BUTTON_ADD_GUARANTOR_TEXT'],
          handler: () => {
            this.presentAlert(value['ALERT_INFORMATION_TITLE_TEXT'], value['PROPOSAL_ACTION_SHEET_BUTTON_ADD_GUARANTOR_MESSAGE_TEXT']);
          }
        });
      }

      actionSheetButtons.push({
        text: value['PROPOSAL_ACTION_SHEET_BUTTON_CANCEL_TEXT'],
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      });

      let actionSheet = this.actionSheetCtrl.create({
        title: value['PROPOSAL_ACTION_SHEET_TITLE_TEXT'],
        buttons: actionSheetButtons
      });
      actionSheet.present();
    });
  }

  presentOrderActionSheet() {
    this.translateService.get(['SELECT-ORDER_TITLE_TEXT', 'ADD_DISCOUNT_TEXT', 'SELECT_CANCEL_TEXT', 'PROPOSAL_ORDER_ACTION_SHEET_TITLE_TEXT']).subscribe((value: string[]) => {
      let actionSheetButtons = [];

      actionSheetButtons.push({
        text: value['SELECT-ORDER_TITLE_TEXT'],
        handler: () => {
          this.selectOrder();
        }
      });

      if (this.checkdatabase == 1) {
        actionSheetButtons.push({
          text: value['ADD_DISCOUNT_TEXT'],
          handler: () => {
            this.showDiscountPrompt();
          }
        });
      }

      actionSheetButtons.push({
        text: value['SELECT_CANCEL_TEXT'],
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      });

      let actionSheet = this.actionSheetCtrl.create({
        title: value['PROPOSAL_ORDER_ACTION_SHEET_TITLE_TEXT'],
        buttons: actionSheetButtons
      });
      actionSheet.present();
    });
  }

  showPromptEditProduct(index: number, product: Product, slidingItem: any) {
    if (slidingItem)
      slidingItem.close();

    let alert = this.alertCtrl.create({
      title: 'Ürün Güncelleme',
      inputs: [
        {
          type: 'radio',
          label: 'Yeni Özel Fiyat',
          value: 'Yeni Özel Fiyat',

        },
        {
          type: 'radio',
          label: 'Tutarlı İskonto',
          value: 'Tutarlı İskonto',

        },
        {
          type: 'radio',
          label: 'Yüzdeli İskonto',
          value: 'Yüzdeli İskonto',

        },
      ],
      buttons: [
        {
          text: 'İptal',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'İleri',
          handler: (data: string) => {
            if (data) {
              console.log(data);
              this.showPrompt(index, product, data);
            } else {
              this.alertWarning();
            }
          }
        }
      ]
    });
    alert.present();
  }

  showPrompt(index: number, product: Product, data) {
    if (data == "Yeni Özel Fiyat") {
      if (this.checkdatabase == 1){
        product.Price = 0;
      }
      let prompt = this.alertCtrl.create({
        title: 'Özel Fiyat',
        message: 'Yeni Özel Fiyat Giriniz!',
        inputs: [
          {
            name: 'price',
            value: product.Price.toString(),
            type: 'number',
            min: 0,
            placeholder: 'Yeni Özel Fiyat Giriniz',
          },
        ],
        buttons: [
          {
            text: 'İptal',
            handler: data => {

            }
          },
          {
            text: 'Kaydet',
            handler: data => {
              this.editProductNew(index, data);
            }
          }
        ]
      });
      prompt.present();
    } else if (data == "Tutarlı İskonto") {
      this.translateService.get(['PROPOSAL_ALERT_EDIT_TITLE_TEXT', 'PROPOSAL_ALERT_EDIT_MESSAGE_TEXT', 'PROPOSAL_ALERT_EDIT_INPUT_PRICE_PLACEHOLDER_TEXT', 'ALERT_BUTTON_CANCEL_TEXT', 'ALERT_BUTTON_SAVE_TEXT', 'PROPOSAL_ALERT_EDIT_INPUT_QUANTITY_PLACEHOLDER_TEXT']).subscribe((value: string[]) => {
        let prompt = this.alertCtrl.create({
          title: value['PROPOSAL_ALERT_EDIT_TITLE_TEXT'],
          message: "Tutarlı İskonto Yap",
          inputs: [
            {
              name: 'price',
              value: product.Price.toString(),
              type: 'number',
              min: 0,
              placeholder: value['PROPOSAL_ALERT_EDIT_INPUT_PRICE_PLACEHOLDER_TEXT'],
            },
          ],
          buttons: [
            {
              text: value['ALERT_BUTTON_CANCEL_TEXT'],
              handler: data => {

              }
            },
            {
              text: value['ALERT_BUTTON_SAVE_TEXT'],
              handler: data => {
                this.editProduct(index, data);
              }
            }
          ]
        });
        prompt.present();
      });
    } else {
      this.translateService.get(['PROPOSAL_ALERT_EDIT_TITLE_TEXT', 'PROPOSAL_ALERT_EDIT_MESSAGE_TEXT', 'PROPOSAL_ALERT_EDIT_INPUT_PRICE_PLACEHOLDER_TEXT', 'ALERT_BUTTON_CANCEL_TEXT', 'ALERT_BUTTON_SAVE_TEXT', 'PROPOSAL_ALERT_EDIT_INPUT_QUANTITY_PLACEHOLDER_TEXT']).subscribe((value: string[]) => {
        let prompt = this.alertCtrl.create({
          title: value['PROPOSAL_ALERT_EDIT_TITLE_TEXT'],
          message: "Yüzdeli İskonto Yap",
          inputs: [
            {
              name: 'pprice',
              type: 'number',
              min: 0,
              placeholder: "Yüzdelik oran giriniz..",
            },
          ],
          buttons: [
            {
              text: value['ALERT_BUTTON_CANCEL_TEXT'],
              handler: data => {

              }
            },
            {
              text: value['ALERT_BUTTON_SAVE_TEXT'],
              handler: data => {
                this.editProduct(index, data);
              }
            }
          ]
        });
        prompt.present();
      });
    }
  }
  //12H647606725

  showPromtBarcodeQty(product: Product, slidingItem: any) {
    if (slidingItem)
      slidingItem.close();

    let alert = this.alertCtrl.create({
      title: 'Adet Bilgisi',
      inputs: [
        {
          name: 'adet',
          placeholder: 'Ürün adeti giriniz',
        }
      ],
      buttons: [
        {
          text: 'İptal',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Kaydet',
          handler: data => {
            console.log(data);
            product.Qty1 = data.adet;
            console.log(product)
          }
        }
      ]
    });
    alert.present();
  }

  showPromptPaymentPlan(product: Product, slidingItem: any) {
    if (slidingItem)
      slidingItem.close();

    this.paymentPlanGrCode.Parameters[0].Value = 'TR';
    this.paymentPlanGrCode.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
    this.paymentPlanGrCode.Parameters[2].Value = product.ItemCode;
    this.serverService.getAny(this.paymentPlanGrCode)
      .then(res => {

        this.paymentPlanGrCodeList = res
        console.log(this.paymentPlanGrCodeList);
        if (!Array.isArray(res)) return;

        this.translateService.get(['PROPOSAL_ALERT_EDIT_TITLE_TEXT', 'PROPOSAL_ALERT_EDIT_MESSAGE_TEXT', 'PROPOSAL_ALERT_EDIT_INPUT_PRICE_PLACEHOLDER_TEXT', 'ALERT_BUTTON_CANCEL_TEXT', 'ALERT_BUTTON_SAVE_TEXT', 'PROPOSAL_ALERT_EDIT_INPUT_QUANTITY_PLACEHOLDER_TEXT']).subscribe((value: string[]) => {
          let prompt = this.alertCtrl.create({
            title: 'Ödeme Planı',
            buttons: [
              {
                text: value['ALERT_BUTTON_CANCEL_TEXT'],
                handler: data => {
                  slidingItem.close();
                }
              },
              {
                text: value['ALERT_BUTTON_SAVE_TEXT'],
                handler: data => {
                  console.log(data)
                  this.installementCount = parseInt(data);
                  console.log(this.installementCount);
                  this.onPlanChangedByProduct(product, data);

                }
              }
            ]
          });


          for (let plan of res) {

            prompt.addInput({
              type: 'radio',
              label: plan.PaymentPlanDescription,
              value: plan.PaymentPlanCode
            });
          }
          prompt.present();
        });

      })
      .catch(this.handleError);
  }

  showPromptSerialNumber(product: Product, slidingItem: any) {
    if (slidingItem)
      slidingItem.close();

    let alert = this.alertCtrl.create({
      title: 'Ürün Seri Numarası Girilme Şekli',
      inputs: [
        {
          type: 'radio',
          label: 'Elle Giriş',
          value: 'Elle Giriş',

        },
        {
          type: 'radio',
          label: 'Okutularak Giriş',
          value: 'Okutularak Giriş',

        },
      ],
      buttons: [
        {
          text: 'İptal',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'İleri',
          handler: (data: string) => {
            if (data) {
              console.log(data);
              this.showpromptEnterSerialNumber(product, data);
            } else {
              this.alertWarning();
            }
          }
        }
      ]
    });
    alert.present();
  }

  alertWarning() {
    let alert = this.alertCtrl.create({
      title: 'Lütfen Bir Seçim Yapınız',
      buttons: [{
        text: 'Tamam',
        handler: data => {

        }
      }]
    });
    alert.present();
  }

  showpromptEnterSerialNumber(product: Product, data: any) {
    if (data == "Elle Giriş") {
      let alert = this.alertCtrl.create({
        title: 'Ürün Seri Numarası',
        inputs: [
          {
            name: 'serino',
            placeholder: 'Seri Numarası...'
          }
        ],
        buttons: [
          {
            text: 'İptal',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Kaydet',
            handler: data => {
              console.log(data);
              product.seriNumber = data.serino;
              console.log(product)
            }
          }
        ]
      });
      alert.present();
    } else {
      this.scanSerialNumber(product);
    }
  }

  onPlanChangedByProduct(product, event) {
    console.log('onPlanChangedByProduct', event)
    for (let plan of this.paymentPlanGrCodeList) {
      if (plan.PaymentPlanCode == event && plan.ItemCode == product.ItemCode) {
        product.Price = plan.Price;
        this.currentUsedBarcodeArray[this.currentPlanCodeArrayCount] = product.UsedBarcode;
        this.currentPlanCodeArray[this.currentPlanCodeArrayCount] = event
        this.currentPlanCodeArrayCount++;
      }
    }
    this.CustomerProdRececipt = this.serverService.Items;
    console.log(this.CustomerProdRececipt);
    this.calculate();
  }

  optionsFn() {
    console.log(this.place);
    this.orderData.TaxTypeCode = this.place;
  }

  onPlanChanged(event) {
    if (this.checkdatabase == 1) {
      if (this.serverService.Items.length > 0 && this.firstcurrent) {
        this.firstcurrent = false;
        this.currentPlanCode = this.serverService.Items[0].PriceList[0].PaymentPlanCode;
        //this.PriceGroupCode = this.serverService.Items[0].PriceList.filter(x => x.PaymentPlanCode == this.currentPlanCode);
        //console.log(this.PriceGroupCode[0]['PriceGroupCode']);
      }
    }
    console.log(this.currentPlanCode);
    for (let p of this.serverService.Items) {
      p.Price = this.getCurrentPrice(p.PriceList);

    }
    this.CustomerProdRececipt = this.serverService.Items;
    console.log(this.CustomerProdRececipt);
    this.calculate();
  }

  showDiscountPrompt() {
    this.translateService.get(['ORDER-DETAIL_DISCOUNT_RATE_TEXT', 'ADD_DISCOUNT_TEXT', 'ORDER-DETAIL_DISCOUNT_RATE_TEXT', 'ALERT_BUTTON_CANCEL_TEXT', 'ALERT_BUTTON_SAVE_TEXT']).subscribe((value: string[]) => {
      let prompt = this.alertCtrl.create({
        title: value['ADD_DISCOUNT_TEXT'],
        message: value['ADD_DISCOUNT_TEXT'],
        inputs: [
          {
            name: 'discount',
            type: 'number',
            min: 1,
            placeholder: "Yüzdelik oran giriniz.."
          }
        ],
        buttons: [
          {
            text: value['ALERT_BUTTON_CANCEL_TEXT'],
            handler: data => {

            }
          },
          {
            text: value['ALERT_BUTTON_SAVE_TEXT'],
            handler: data => {
              this.setDiscount(data);
            }
          }
        ]
      });
      prompt.present();
    });
  }

  clearItems() {
    this.firstcurrent = true;
    this.translateService.get(['PROPOSAL_ALERT_CLEAR_TITLE_TEXT', 'PROPOSAL_ALERT_CLEAR_MESSAGE_TEXT', 'ALERT_BUTTON_CANCEL_TEXT', 'ALERT_BUTTON_CLEAR_TEXT']).subscribe((value: string[]) => {
      let prompt = this.alertCtrl.create({
        title: value['PROPOSAL_ALERT_CLEAR_TITLE_TEXT'],
        message: value['PROPOSAL_ALERT_CLEAR_MESSAGE_TEXT'],
        buttons: [
          {
            text: value['ALERT_BUTTON_CANCEL_TEXT'],
            handler: data => {

            }
          },
          {
            text: value['ALERT_BUTTON_CLEAR_TEXT'],
            handler: data => {
              this.serverService.Items = [];
              this.reset();
            }
          }
        ]
      });
      prompt.present();
    });
  }

  increasedecreaseNumber(index: number, type: number) {
    switch (type) {
      case 1:
        this.serverService.Items[index].Qty1++;
        break;
      case 2:
        if (this.serverService.Items[index].Qty1 > 1) this.serverService.Items[index].Qty1--;
        break;
      case 3:
        this.serverService.Items[index].Qty1 = (parseFloat(this.serverService.Items[index].Qty1) + 1).toFixed(1);
        break;
      case 4:
        if (this.serverService.Items[index].Qty1 > 1) this.serverService.Items[index].Qty1 = (parseFloat(this.serverService.Items[index].Qty1) - 1).toFixed(1);
        break;
      case 5:
        this.serverService.Items[index].Qty1 = (parseFloat(this.serverService.Items[index].Qty1) + 0.1).toFixed(1);
        break;
      case 6:
        if (this.serverService.Items[index].Qty1 > 0.1) this.serverService.Items[index].Qty1 = (parseFloat(this.serverService.Items[index].Qty1) - 0.1).toFixed(1);
        break;
    }
    if (this.serverService.Settings.V3Settings.IsProductPriceByGrCode == false || this.serverService.Settings.V3Settings.IsProductPriceByGrCode == true && this.serverService.Settings.V3Settings.SalesType != 3) {
      this.onPlanChanged(event);
    } else {
      this.onPlanChangedByProduct(Product, event);
    }
  }
  setQuantity(index: number) {
    this.serverService.Items[index].Qty1


    let prompt = this.alertCtrl.create({
      title: 'Uzunluk Giriniz!',
      cssClass: 'alertDanger',
      //message: value['PROPOSAL_ALERT_EDIT_MESSAGE_TEXT'],
      inputs: [
        {
          name: 'first',
          // value: '00',
          type: 'number',
          id: 'input1',
          placeholder: '00'
        },
        {
          name: 'second',
          // value: '00',
          type: 'number',
          id: 'input2',
          placeholder: '00'
        }
      ],
      buttons: [
        {
          text: 'Geri',
          handler: data => {
          }
        },
        {
          text: 'Tamam',
          handler: data => {
            console.log(data)
            if (data.first == "") data.first = 0;
            if (data.second == "") data.second = 0;
            this.serverService.Items[index].Qty1 = data.first + '.' + data.second;
            if (this.serverService.Settings.V3Settings.IsProductPriceByGrCode == false || this.serverService.Settings.V3Settings.IsProductPriceByGrCode == true && this.serverService.Settings.V3Settings.SalesType != 3) {
              this.onPlanChanged(event);
            } else {
              this.onPlanChangedByProduct(Product, event);
            }
          }
        }
      ]
    });
    prompt.present();
  }

}
