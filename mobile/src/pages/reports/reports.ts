import {Component} from '@angular/core';
import {NavController, ModalController} from 'ionic-angular';
import {CustomersPage} from "../customers/customers";
import {OrderReportPage} from "../order-report/order-report";
import { ProductsPage } from '../products/products';
import { ProposalTypePage } from '../proposal-type/proposal-type';
import { ServerService } from '../../classes/server.service';

@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html'
})
export class ReportsPage {

  checkdatabase: number = 0;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public serverService: ServerService) {
    //JS if(this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2'){
    if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
      this.checkdatabase = 1;
    } 
  }  

  goToOrderReport() {

    this.navCtrl.push(OrderReportPage, {nextPage: 'orderdetail'});
    // let modal;
    // modal = this.modalCtrl.create(ProposalTypePage, {text: 'Müşteri Sipariş Raporu Tipi Seçiniz'});
    // modal.present();
    // modal.onDidDismiss(data => {                
    //       if(data.type != false){
    //         this.serverService.Settings.V3Settings.SalesType = data.type;
    //         this.navCtrl.push(OrderReportPage, {nextPage: 'orderdetail'});
    //       }else{

    //       }          
    // });
    
  }  

  goToProducts(){
    this.navCtrl.push(ProductsPage, {nextPage: 'productinventoryreport'})
  }

  goToReportTypePage(){ 
    
    this.navCtrl.push(CustomersPage, {nextPage: 'extrareport'});
    // let modal;
    // modal = this.modalCtrl.create(ProposalTypePage, {text: 'Müşteri Ekstresi Tipi Seçiniz'});
    // modal.present();
    // modal.onDidDismiss(data => {                
    //       if(data.type != false){
    //         this.serverService.Settings.V3Settings.SalesType = data.type;
    //         this.navCtrl.push(CustomersPage, {nextPage: 'extrareport'});
    //       }else{

    //       }          
    // });
  }
}
