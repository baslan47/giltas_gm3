import { Component, ViewChild } from '@angular/core';
import { ModalController, NavController, NavParams, ViewController, LoadingController, Loading, InfiniteScroll } from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import { Customer } from "../../classes/customer";
import { CustomerPage } from "../customer/customer";
import { TranslateService } from "@ngx-translate/core";
import { ModalCustomersPage } from '../modal-customers/modal-customers';

@Component({
  selector: 'page-select-customer',
  templateUrl: 'select-customer.html',
})
export class SelectCustomerPage {

  loading: Loading;
  public myInput: string = "";
  pagefetch: number = 0;
  checkdatabase: number = 0;
  pagefetchcount: number = 0;
  checklog: boolean = false;
  public items: Array<Customer>;
  public dataList: any[];
  userData = {
    "ProcName": "", "Parameters": [{ "Name": "FirstName", "Value": "" }, { "Name": "LastName", "Value": "" },
    { "Name": "IdentityNumber", "Value": "" }, { "Name": "PhoneNumber", "Value": "" }, { "Name": "Code", "Value": "" },
    { "Name": "FetchPage", "Value": "0" }, { "Name": "FirstLastName", "Value": "" }
    , { "Name": "CustomerType", "Value": "0" }]
  };
  userDataFetch = {
    "ProcName": "", "Parameters": [{ "Name": "FirstName", "Value": "" }, { "Name": "LastName", "Value": "" },
    { "Name": "IdentityNumber", "Value": "" }, { "Name": "PhoneNumber", "Value": "" }, { "Name": "Code", "Value": "" },
    { "Name": "FirstLastName", "Value": "" }]
  };

  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public serverService: ServerService, public viewCtrl: ViewController,
    public loadingCtrl: LoadingController, public translateService: TranslateService) {
    this.dataList = [];
    //JS if(this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2'){
    if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
      this.checkdatabase = 1;
    } 
  }

  ionViewWillEnter() {
    if (this.checklog == false) {
      if (this.serverService.Settings.V3Settings.SalesType) {
        console.log(this.serverService.Settings.V3Settings.SalesType);
        switch (this.serverService.Settings.V3Settings.SalesType.toString()) {
          case '1':
            this.userData.ProcName = "G3_CustomerListWholePaging";            
            this.userData.Parameters[7].Value = "0";
            this.userDataFetch.ProcName = "G3_CustomerListWholefetchCount";  
            break;
          case '2':
            this.userData.ProcName = "G3_CustomerListRetailPaging";
            this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
            break;
          case '3':
            this.userData.ProcName = "G3_CustomerListRetailPaging";
            this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
            break;
          case '4':
            this.userData.ProcName = "G3_CustomerListWholePaging";            
            this.userData.Parameters[7].Value = "3";
            this.userDataFetch.ProcName = "G3_CustomerListWholefetchCount";
            break;
          case '5':
            this.userData.ProcName = "G3_CustomerListRetailPaging";
            this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
            break;
          case '6':
            this.userData.ProcName = "G3_CustomerListRetailPaging";
            this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
            break;
          default:
            this.userData.ProcName = "G3_CustomerListRetailPaging";
            this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
            break;
        }
      }
      else {
        this.userData.ProcName = "G3_CustomerListRetailPaging";
        this.userDataFetch.ProcName = "G3_CustomerListRetailfetchCount";
      }
      console.log(this.userData);
      this.doFilter();
    }
  }

  refreshFilter() {
    this.infiniteScroll.enable(true);
    this.dataList = [];
    this.pagefetch = 0;
    this.userData.Parameters[0].Value = "";
    this.userData.Parameters[1].Value = "";
    this.userData.Parameters[2].Value = "";
    this.userData.Parameters[3].Value = "";
    this.userData.Parameters[4].Value = "";
    this.userData.Parameters[5].Value = "0";
    this.userData.Parameters[6].Value = "";
    this.userData.Parameters[7].Value = "0";
    this.userDataFetch.Parameters[0].Value = "";
    this.userDataFetch.Parameters[1].Value = "";
    this.userDataFetch.Parameters[2].Value = "";
    this.userDataFetch.Parameters[3].Value = "";
    this.userDataFetch.Parameters[4].Value = "";
    this.userDataFetch.Parameters[5].Value = "";
    this.checklog = true;
  }

  loadData(infiniteScroll?) {
    this.pagefetch += 10;
    this.userData.Parameters[5].Value = this.pagefetch.toString();
    this.serverService.getCustomers(this.userData)
      .then(res => {
        console.log(res)
        this.items = res;
        for (let i = 0; i < 10; i++) {
          this.dataList.push(this.items[i]);
          if (this.dataList.length == this.pagefetchcount) {
            console.log("Done");
            infiniteScroll.enable(false);
            break;
          } else {
  
          }
        }
        infiniteScroll.complete();
        console.log(this.dataList);
      })
      .catch(this.handleError);          
  }

  filterModal() {
    this.refreshFilter()
    let modal;
    modal = this.modalCtrl.create(ModalCustomersPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data.txtCN) {
        this.userData.Parameters[0].Value = data.txtCN;
        this.userDataFetch.Parameters[0].Value = data.txtCN;
      }
      if (data.txtCLN) {
        this.userData.Parameters[1].Value = data.txtCLN;
        this.userDataFetch.Parameters[1].Value = data.txtCLN;
      }
      else if (data.txtCFLN) {
        this.userData.Parameters[6].Value = data.txtCFLN;
        this.userDataFetch.Parameters[5].Value = data.txtCFLN;
      }
      else if (data.txtCTC) {
        this.userData.Parameters[2].Value = data.txtCTC;
        this.userDataFetch.Parameters[2].Value = data.txtCTC;
      }
      else if (data.txtCT) {
        this.userData.Parameters[3].Value = data.txtCT;
        this.userDataFetch.Parameters[3].Value = data.txtCT;
      }
      else if (data.txtCC) {
        this.userData.Parameters[4].Value = data.txtCC;
        this.userDataFetch.Parameters[4].Value = data.txtCC;
      }
      this.doFilter();
    });
  }

  onInput(event: any) {
    if (this.myInput.length >= 3) {
      // TODO
    }
  }

  onCancel(event: any) {
    this.doFilter();
  }

  doFilter() {
    this.presentLoading();
    this.serverService.getCustomers(this.userData)
      .then(res => {
        this.items = res;
        console.log(res);
        this.dismissLoading();
        if (this.items.length > 0) {
          for (let i = 0; i < 10; i++) {
            this.dataList.push(res[i]);
            if (this.dataList.length == this.items.length) {
              if (this.dataList.length < 10) {
                this.infiniteScroll.enable(false);
              }
              break;
            }
          }
        } else {
          console.log("Aranan Kayıtlar Yok");
        }
        this.serverService.getCustomers(this.userDataFetch)
          .then(res => {
            this.pagefetchcount = res[0]["Total"];
            console.log(this.pagefetchcount);
          })
          .catch(this.handleError);
      })
      .catch(this.handleError);
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
  }

  selectCustomer(customer: Customer) {
    this.viewCtrl.dismiss({ customer: customer });
  }

  addCustomer() {
    this.checklog = true;
    const modal = this.modalCtrl.create(CustomerPage, { showBack: true });
    modal.present();
    modal.onDidDismiss(
      data => {
        if (data) {
          this.selectCustomer(data.customer);
        }
      }
    );
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }
}
