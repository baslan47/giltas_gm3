import { Component } from '@angular/core';
import {
  AlertController,
  Loading,
  LoadingController,
  NavController,
  NavParams,
  ViewController,
  ToastController,
  ModalController
} from 'ionic-angular';
import { ServerService } from "../../classes/server.service";
import { TranslateService } from "@ngx-translate/core";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { Product, ProductDetails } from "../../models/models";
import { ModalProductfilterPage } from '../modal-productfilter/modal-productfilter';

@Component({
  selector: 'page-select-product',
  templateUrl: 'select-product.html',
})
export class SelectProductPage {
  public myInput: string = "";
  public attributeCode: string = "";
  loading: Loading;  
  imageURL: any;
  checklog: boolean = false;
  selectedQuantityP: string = "1";
  selectedSize: string;
  public items: Product[] = new Array();
  userData = {
    "ProcName": "G3_ProductListNEW",
    "Parameters": [{ "Name": "ItemDescription", "Value": "" }, { "Name": "ItemCode", "Value": "" }, { "Name": "AttributeCode", "Value": "" }
      , { "Name": "AttributeType", "Value": "" }]
  };
  productData = { "ProcName": "", "Parameters": [{ "Name": "ItemCode", "Value": "" }] };
  storeData = { "ProcName": "", "Parameters": [{ "Name": "ItemCode", "Value": "" }, { "Name": "WarehouseCode", "Value": "" }] };
  barcodeData = {
    "ProcName": "G3_GetProductPriceByBarcode",
    "Parameters": [{ "Name": "Barcode", "Value": "" }, { "Name": "PriceGroupCode", "Value": "" }]
  };
  getBarcodeData = {
    "ProcName": "G3_GetProductBarcodeSelectPage", "Parameters": [{ "Name": "ItemCode", "Value": "" },
    { "Name": "ColorCode", "Value": "" }, { "Name": "ItemDim1Code", "Value": "" }]
  };  

  getAvailableInventoryV3 = {
    "ProcName": "G3_GetAvailableInventory", "Parameters": [{ "Name": "ItemCode", "Value": "" },
    { "Name": "ColorCode", "Value": "" }]
  };

  //[]

  product: Product;
  productDetails: ProductDetails;
  productColorSizeMatrix: any[] = [];
  productColorSizeMatrixNew: any[] = [];
  productColors: any[] = [];
  colHeaders: any;
  checkdatabase: number = 0;
  color: string;
  sizes: any;
  productSelectedQuantityMatrix = [{ "Size": "", "MaxQuantity": <number>{}, "SelectedQuantity": "" }];
  productSelectedQuantityMatrix2 = [{ "Size": "", "MaxQuantity": <number>{}, "SelectedQuantity": "" }];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public serverService: ServerService, public viewCtrl: ViewController,
    public translateService: TranslateService, public alertCtrl: AlertController,
    public photoViewer: PhotoViewer, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public modalCtrl: ModalController) {
      //JS if(this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2'){
      if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
        this.checkdatabase = 1;
      } 
  }


  ionViewWillEnter() {
    // this.presentLoading();
    if (!this.checklog) {
      this.filterModal();
    } 
   }

  filterModal() {
    this.refreshFilter();
    this.checklog = true;
    let modal;
    modal = this.modalCtrl.create(ModalProductfilterPage);
    modal.present();
    modal.onDidDismiss(data => {
      if (data.result == false) {
        console.log(data.result);
      } else {
        if (data.txtPN) {
          this.userData.Parameters[0].Value = data.txtPN;
        }
        else if (data.txtPC) {
          this.userData.Parameters[1].Value = data.txtPC;
        }
        this.doFilter();
      }
    });
  }

  doFilter() {
    this.presentLoading();
    this.serverService.getAny(this.userData) //getProducts
      .then(res => {
        this.dismissLoading();
        this.items = res;
        console.log(res);
      })
      .catch(this.handleError);
  }

  refreshFilter() {
    this.userData.Parameters[0].Value = "";
    this.userData.Parameters[1].Value = "";
    this.userData.Parameters[2].Value = "";
    this.userData.Parameters[3].Value = "";
    this.checklog = false;
    //this.doFilter();
  }

  private handleError(error: any) {
    this.dismissLoading();
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string[]) => {
      this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
  }

  selectProduct(product: Product) {

    if (product) {
      this.presentLoading();
      // console.log('select-product.selectProduct:' + JSON.stringify(product));
      this.product = product;
      console.log(this.product);
      this.imageURL = this.serverService.Settings.G3Settings.ImageUrl + '/Home/GetImage?itemcode=' + this.product.ItemCode;

      this.storeData.ProcName = "G3_GetProductDetails";
      this.storeData.Parameters[0].Value = product.ItemCode;
      this.storeData.Parameters[1].Value = (this.serverService.Settings.V3Settings.WarehouseCode) ? this.serverService.Settings.V3Settings.WarehouseCode : "";
      this.serverService.getAny(this.storeData).then(res => {
        console.log('G3_GetProductDetails');
        console.log(res);
        this.productDetails = res[0];
      }).catch(error => this.handleError(error));

      this.productData.Parameters[0].Value = product.ItemCode;
      this.productData.ProcName = "G3_GetItemDim1s";
      this.serverService.getAny(this.productData).then(res => {
        console.log('G3_GetItemDim1s');
        console.log(res);
        //this.dismissLoading();

      

        if (res) {
          this.colHeaders = res;

          this.storeData.ProcName = "G3_GetProductColorSizeMatrix3";
          this.storeData.Parameters[0].Value = product.ItemCode;
          this.storeData.Parameters[1].Value = (this.serverService.Settings.V3Settings.WarehouseCode) ? this.serverService.Settings.V3Settings.WarehouseCode : "";
          this.serverService.getAny(this.storeData).then(res => {
            console.log(this.serverService.Settings.V3Settings.WarehouseCode);
            console.log(product.ItemCode);
            console.log('G3_GetProductColorSizeMatrix3');
            console.log(res);
            this.dismissLoading();
            if (res.length > 0 && JSON.stringify(res).toString().indexOf('Exception') == -1) {
              this.productColorSizeMatrix = res;
              if (!this.color && this.productColorSizeMatrix[0]['R/B'])
                this.color = this.productColorSizeMatrix[0]['R/B'];
              if (this.color) {
                this.setSizes();
              }
            } else {// sadece renk
              this.storeData.ProcName = "G3_GetProductColors";
              this.storeData.Parameters[0].Value = product.ItemCode;
              this.storeData.Parameters[1].Value = (this.serverService.Settings.V3Settings.WarehouseCode) ? this.serverService.Settings.V3Settings.WarehouseCode : "";
              this.serverService.getAny(this.storeData).then(res => {
                console.log(res);
                // altakki satir, altakki if ten kaldırıldı
                // && ((res.length ==1 || res.length ==2) && res[0].ColorCode != "") 

                if (res.length > 0 && this.productColorSizeMatrix.length == 0) {
                  this.productColors = res;
                  console.log(this.serverService.getColorDescription(res[0].ColorCode))
                }
              })
            }
          }).catch(error => this.handleError(error));
        } 
        else{
          this.dismissLoading();
        }
      }).catch(error => this.handleError(error));
    }
    else this.viewCtrl.dismiss({ result: false });
  }

  getAvailableInventory(val){    
    this.productSelectedQuantityMatrix2 = [];   
    this.getAvailableInventoryV3.Parameters[0].Value = this.product.ItemCode;
    this.getAvailableInventoryV3.Parameters[1].Value = val;    
    this.serverService.getAny(this.getAvailableInventoryV3).then(res => {
      console.log('G3_getAvailableInventory');   
      console.log(res);    
      this.productColorSizeMatrixNew = res;
      for (let x of this.productColorSizeMatrixNew) {      
        let count: number = x.AvailableInventoryQty1;
        this.productSelectedQuantityMatrix2.push({
          Size: x.ItemDim1Code,
          MaxQuantity: count,
          SelectedQuantity: "0"
        });      
      }      
      console.log(this.productSelectedQuantityMatrix2); 
    }).catch(error => this.handleError(error));
  }

  public setSizes() {
    this.sizes = this.productColorSizeMatrix.find(x => x['R/B'] == this.color);
    if(this.checkdatabase == 1){
      this.getAvailableInventory(this.color);
    }    
    this.productSelectedQuantityMatrix = [];       
    for (let col of this.colHeaders) {
      if (col.ItemDim1Code != 'R/B' && col.ItemDim1Code != '') {
        let max: number = this.sizes[col.ItemDim1Code];
        this.productSelectedQuantityMatrix.push({
          Size: col.ItemDim1Code,
          MaxQuantity: max,
          SelectedQuantity: "0"
        });
      }
    }
    console.log('sizes');
    console.log(this.sizes);
    //console.log(this.productSelectedQuantityMatrix);
    //console.log(this.productSelectedQuantityMatrix2);
  }

  showImage() {
    let url = this.imageURL;
    url += (this.color) ? ('&colorcode=' + this.color) : '';
    this.photoViewer.show(url);
  }

  addToBasket() {
    let count: number = 0;  
    if(this.checkdatabase == 1){
      if (this.sizes) {
        for (let item of this.productSelectedQuantityMatrix2) {
          count += Number(item.SelectedQuantity);        
        }      
        if (count > 0) {
          if (this.productColorSizeMatrix && this.sizes) {
            if (!this.color) {
              this.showAlert('Bilgi', 'Lütfen renk seçeniz.');
              return;
            }
          }
          this.addProduct();
        }
        else {
          this.translateService.get('SELECT-PRODUCT_NO_PRODUCT_TO_ADD_BASKET_MESSAGE_TEXT').subscribe((value: string) => {
            this.presentToast(value);
          });
        }
      } else {
        this.addProductwithItemCode();
      }
    }else{
      if (this.sizes) {
        for (let item of this.productSelectedQuantityMatrix) {
          count += Number(item.SelectedQuantity);        
        }      
        if (count > 0) {
          if (this.productColorSizeMatrix && this.sizes) {
            if (!this.color) {
              this.showAlert('Bilgi', 'Lütfen renk seçeniz.');
              return;
            }
          }
          this.addProduct();
        }
        else {
          this.translateService.get('SELECT-PRODUCT_NO_PRODUCT_TO_ADD_BASKET_MESSAGE_TEXT').subscribe((value: string) => {
            this.presentToast(value);
          });
        }
      } else {
        this.addProductwithItemCode();
      }
    }      
  }

  addProduct() {
    this.presentLoading();
    if(this.checkdatabase == 1){
      for (let item of this.productSelectedQuantityMatrix2) {
        if (Number(item.SelectedQuantity) > 0) {
          this.getBarcodeData.Parameters[0].Value = this.product.ItemCode;
          this.getBarcodeData.Parameters[1].Value = this.color ? this.color : '';
          this.getBarcodeData.Parameters[2].Value = item.Size ?item.Size : '';
          this.serverService.getAny(this.getBarcodeData).then(res => {
            let current: Product = new Product();
            current.ItemCode = this.product.ItemCode;
            current.ItemDescription = this.product.ItemDescription;
            current.MesureCode = this.product.MesureCode;
            console.log(res)
  
            current.UsedBarcode = res[0].Barcode;
            this.barcodeData.Parameters[0].Value = res[0].Barcode;
            this.barcodeData.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
            this.serverService.getProductPrice(this.barcodeData).then(res => {
              console.log(res);             
              current.MesureCode = this.product.MesureCode;
              current.PriceList = res.PriceList;
              current.LDisRate1 = 0;
              current.Content = this.productDetails.ProductAtt08Desc ? this.productDetails.ProductAtt08Desc : '';
              current.ColorCode = this.productColorSizeMatrixNew[0]['ColorCode'];
              current.ItemDim1Code = item.Size;
              current.Qty1 = parseInt(item.SelectedQuantity);
              current.MaxQty = item.MaxQuantity;
              current.BarcodeType = res.BarcodeType;
              console.log('current', current)
              this.pushProduct(current);
             
              this.dismiss();
             this.dismissLoading();
            }).catch(error => this.handleError(error));
          }).catch(error => this.handleError(error));
        }
      }
      this.translateService.get('SELECT-PRODUCT_PRODUCTS_ADDED_TO_BASKET_SUCCESSFULLY_MESSAGE_TEXT').subscribe((value: string) => {
        this.presentToast(value);
      });
    }else{
      for (let item of this.productSelectedQuantityMatrix) {
        if (Number(item.SelectedQuantity) > 0) {
          this.getBarcodeData.Parameters[0].Value = this.product.ItemCode;
          this.getBarcodeData.Parameters[1].Value = this.color ? this.color : '';
          this.getBarcodeData.Parameters[2].Value = item.Size ?item.Size : '';
          this.serverService.getAny(this.getBarcodeData).then(res => {
            console.log(res);
            let current: Product = new Product();
            current.ItemCode = this.product.ItemCode;
            current.ItemDescription = this.product.ItemDescription;
            
            console.log(res)
            console.log(this.product)
            current.MesureCode = res[0].MesureCode;
            current.UsedBarcode = res[0].Barcode;
            this.barcodeData.Parameters[0].Value = res[0].Barcode;
            this.barcodeData.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
            this.serverService.getProductPrice(this.barcodeData).then(res => {
              console.log(res);
              //current.MesureCode = this.product.MesureCode;
              current.PriceList = res.PriceList;
              current.LDisRate1 = 0;
              current.Content = this.productDetails.ProductAtt08Desc ? this.productDetails.ProductAtt08Desc : '';
              current.ColorCode = res.ColorCode;
              current.ItemDim1Code = res.ItemDim1Code;
              current.Qty1 = parseInt(item.SelectedQuantity);
              current.MaxQty = item.MaxQuantity;
              current.BarcodeType = res.BarcodeType;
              console.log('current', current)
              this.pushProduct(current);
             
              this.dismiss();
             this.dismissLoading();
            }).catch(error => this.handleError(error));
          }).catch(error => this.handleError(error));
        }
      }
      this.translateService.get('SELECT-PRODUCT_PRODUCTS_ADDED_TO_BASKET_SUCCESSFULLY_MESSAGE_TEXT').subscribe((value: string) => {
        this.presentToast(value);
      });
    }    
  }

  addProductwithItemCode() {
    this.presentLoading();

    this.getBarcodeData.Parameters[0].Value = this.product.ItemCode;
    this.getBarcodeData.Parameters[1].Value = this.color ? this.color : '';
    this.getBarcodeData.Parameters[2].Value = this.selectedSize ? this.selectedSize : '';
    // console.log(this.getBarcodeData);
    this.serverService.getAny(this.getBarcodeData).then(res => {
       console.log('addProductwithItemCode-RES',res);
      this.product.UsedBarcode = res[0].Barcode;

      this.barcodeData.Parameters[0].Value = res[0].Barcode;
      this.barcodeData.Parameters[1].Value = this.serverService.Settings.V3Settings.PriceGroupCode;
      
      // console.log(this.barcodeData)
      this.serverService.getProductPrice(this.barcodeData).then(res => {
         console.log(res);
        // console.log(this.product)
        this.product.PriceList = res.PriceList;
        // for(let n = 0; n<res.PriceList.length;n++){
        //   if(res.PriceList[n].PaymentPlanCode==""){
        //     this.product.Price = res.PriceList[n].Price;
        //     this.product.PaymentPlanCode = res.PriceList[n].PaymentPlanCode
        //   }
        // }
        this.product.MesureCode = this.productDetails.MesureCode;
        this.product.LDisRate1 = 0;
        this.product.ColorCode = res.ColorCode;
        this.product.ItemDim1Code = res.ItemDim1Code;
        this.product.Qty1 = parseInt(this.selectedQuantityP);
        this.product.BarcodeType = res.BarcodeType;
        console.log(this.product)
        this.pushProduct(this.product);
        this.loading.dismiss();
        this.dismiss();
      }).catch(error => this.handleError(error));
    }).catch(error => this.handleError(error));
  }

  pushProduct(current: Product) {
    for (let item of this.serverService.Items) {
      if (item.ItemCode == current.ItemCode && item.UsedBarcode == current.UsedBarcode) {
        item.Qty1 += Number(current.Qty1);

        return
      }
    }

    //current.Content = this.productDetails.ProductAtt08Desc ? this.productDetails.ProductAtt08Desc : '';

    //Toptan Satis ise
    if (this.serverService.Settings.V3Settings.SalesType == 1){
      current.Price = this.productDetails.SalePrice;    
    }      
    else{
      current.Price = this.productDetails.RetailSalePrice;
      current.UseSerialNumber = this.productDetails.UseSerialNumber;
    }    
    this.serverService.Items.push(current);
  }

  
  dismiss() {
    
    setTimeout(() => { 
      this.viewCtrl.dismiss({ result: true });
    }, 500);
  }

  increase() {
    var temp = parseInt(this.selectedQuantityP);
    temp++;
    this.selectedQuantityP = temp.toString();
  }

  decrease() {
    var temp = parseInt(this.selectedQuantityP);
    if (temp > 1) {
      temp--;
      this.selectedQuantityP = temp.toString();
    } else {

    }
  }

  presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'X'
    });
    toast.present();
  }

  public showAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  public presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Lütfen bekleyiniz...",
    });
    this.loading.present();
  }

  public dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  selectAttribute() {
    this.translateService.get(['ALERT_BUTTON_OK_TEXT', 'ALERT_BUTTON_CANCEL_TEXT', 'ALERT_SELECTION_TITLE_TEXT', 'SELECT-PRODUCT_SELECT_ATTRIBUTE_TYPE_TEXT', 'SELECT-PRODUCT_SELECT_ATTRIBUTE_CODE_TEXT']).subscribe((value: string[]) => {
      let inputs = [];
      let attributeTypeData = {
        "ProcName": "G3_GetAttributeTypes",
        "Parameters": [{ "Name": "Language", "Value": "TR" }]
      };
      let attributeCodeData = {
        "ProcName": "G3_GetAttributes",
        "Parameters": [{ "Name": "Language", "Value": "TR" }, { "Name": "AttributeTypeCode", "Value": "" }]
      };
      this.serverService.getAny(attributeTypeData).then(res => {
        for (let attributeType of res) {
          inputs.push({
            type: 'radio',
            label: attributeType.AttributeTypeDescription,
            value: attributeType.AttributeTypeCode
          });
        }
        let prompt = this.alertCtrl.create({
          title: value['SELECT-PRODUCT_SELECT_ATTRIBUTE_TYPE_TEXT'],
          buttons: [
            {
              text: value['ALERT_BUTTON_CANCEL_TEXT'],
              role: 'cancel',
              handler: data => {

              }
            },
            {
              text: value['ALERT_BUTTON_OK_TEXT'],
              handler: data => {
                if (data) {
                  inputs = [];
                  attributeCodeData.Parameters[1].Value = data;
                  this.serverService.getAny(attributeCodeData).then(res => {
                    for (let attribute of res) {
                      inputs.push({
                        type: 'radio',
                        label: attribute.AttributeDescription,
                        value: attribute.AttributeCode
                      });
                    }

                    let prompt = this.alertCtrl.create({
                      title: value['SELECT-PRODUCT_SELECT_ATTRIBUTE_CODE_TEXT'],
                      buttons: [
                        {
                          text: value['ALERT_BUTTON_CANCEL_TEXT'],
                          role: 'cancel',
                          handler: data => {

                          }
                        },
                        {
                          text: value['ALERT_BUTTON_OK_TEXT'],
                          handler: data => {
                            if (data) {
                              this.attributeCode = data;
                              this.doFilter();
                            }
                          }
                        }
                      ],
                      inputs: inputs
                    });
                    prompt.present();

                  }).catch(this.handleError);
                }
              }
            }
          ],
          inputs: inputs
        });
        prompt.present();
      }).catch(this.handleError);
    });
  }

  resetAttribute() {
    this.attributeCode = '';
    this.doFilter();
  }

  alertQuantity(item) {
    if (item.SelectedQuantity > item.MaxQuantity) {
      this.translateService.get('QUANTITY_EXCEDED_TEXT').subscribe((value: string) => {
        this.presentToast(value);
        console.log(value);
      });
    }
  }
}
