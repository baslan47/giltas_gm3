import {Component} from '@angular/core';
import {NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {G3Settings, Integrator, Settings, V3Settings} from "../../models/models";
import {ServerService} from "../../classes/server.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  settings: Settings = new Settings();
  settingsList: Settings[] = [];
  saveAs: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
              public storage: Storage, public toastCtrl: ToastController, public serverService: ServerService,
              public translateService: TranslateService) {
    this.settings.Integrator = new Integrator();
    this.settings.G3Settings = new G3Settings();
    this.settings.V3Settings = new V3Settings();
    this.getSettings();
  }

  saveSettings() {
    const item = this.settingsList.find(x => x.Header == this.settings.Header);
    const index = this.settingsList.indexOf(item, 0);
    if (index > -1) {
      this.settingsList.splice(index, 1);
    }
    this.settings.Header = this.settings.Header;

    // for deep copy problem
    const newSettings: Settings = JSON.parse(JSON.stringify(this.settings));

    // to save the current token.
    if(this.serverService.Settings)
      newSettings.Token = this.serverService.Settings.Token;

    // to push to 0 index
    this.settingsList.splice(0, 0, newSettings);
    this.serverService.Settings = newSettings;

    console.log(newSettings);
    console.log(this.settings);
    console.log(this.settingsList);
    this.storage.set('settingsList', this.settingsList).then((data) => {
      this.translateService.get('SETTINGS_SETTINGS_SAVED_SUCCESSFULLY_MESSAGE_TEXT').subscribe((value: string) => {
        this.presentToast(value);
      });

      this.navCtrl.pop();
    }), error => console.error('Error storing item', error);
  }

  getSettings() {
    this.storage.get('settingsList').then(data => {
      console.log(data);
      if (data) {
        this.settingsList = data;
        if (this.settingsList && this.settingsList.length > 0) {
          const newSettings: Settings = JSON.parse(JSON.stringify(this.settingsList[0]));
          this.settings = newSettings;
        }
      }
      console.log(this.settings);
    }, error => console.error(error));
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'X'
    });
    toast.present();
  }

  //Ceyiz
  setSettings0() {
    this.settings.Header = "Giltaş Demo";

    this.settings.G3Settings.ImageUrl = 'http://85.105.106.125:8084';
    this.settings.G3Settings.OrderCount = 10;

    this.settings.Integrator.Url = 'http://85.105.106.125:8083';
    this.settings.Integrator.UserGroupCode = 'V3';
    this.settings.Integrator.DatabaseName = 'Ceyiz_V3';
    this.settings.Integrator.UserName = 'V3';
    this.settings.Integrator.Password = '1';

    this.settings.V3Settings.OfficeCode = 'M01';
    this.settings.V3Settings.StoreCode = 'M01';
    this.settings.V3Settings.WarehouseCode = 'M01';
    this.settings.V3Settings.DiscountType = 2;
    this.settings.V3Settings.SalesType = 2;
    this.settings.V3Settings.PaymentTerm = 30;
    this.settings.V3Settings.PriceGroupCode = 'PS';
    this.settings.V3Settings.DiscountTypeCode = '1';
    this.settings.V3Settings.DiscountReasonCode = '10';
    this.settings.V3Settings.IsPercentage = true;
    this.settings.V3Settings.POSTerminalID = "1";
    this.settings.V3Settings.PaymentCode = "M01";
  }

  //KonfSun
  setSettingsK() {
    this.settings.Header = "Giltaş Konfsun";

    this.settings.G3Settings.ImageUrl = 'http://85.105.106.125:8084';
    this.settings.G3Settings.OrderCount = 10;

    // this.settings.Integrator.Url = 'http://85.105.106.125:8083';
    // this.settings.Integrator.UserGroupCode = 'V3';
    // this.settings.Integrator.DatabaseName = 'KonfSun';
    // this.settings.Integrator.UserName = 'V3';
    // this.settings.Integrator.Password = '1';

    this.settings.V3Settings.OfficeCode = 'F01'; //
    this.settings.V3Settings.StoreCode = '1-5-1';
    this.settings.V3Settings.WarehouseCode = '1-0-1';
    this.settings.V3Settings.DiscountType = 2;
    this.settings.V3Settings.SalesType = 2;
    this.settings.V3Settings.PaymentTerm = 30;
    this.settings.V3Settings.PriceGroupCode = 'PS';
    this.settings.V3Settings.DiscountTypeCode = '1';
    this.settings.V3Settings.DiscountReasonCode = '10';
    this.settings.V3Settings.IsPercentage = true;
    this.settings.V3Settings.POSTerminalID = "1";
    this.settings.V3Settings.PaymentCode = "100-001-300";
  }

  //Ozsanal
  setSettingsO() {
    this.settings.Header = "Giltaş Özşanal";

    this.settings.G3Settings.ImageUrl = 'http://195.175.66.150:8084';
    this.settings.G3Settings.OrderCount = 10;

    this.settings.Integrator.Url = 'http://195.175.66.150:8083';
    this.settings.Integrator.UserGroupCode = 'V3';
    this.settings.Integrator.DatabaseName = 'SANALV3';
    this.settings.Integrator.UserName = 'ISOM';
    this.settings.Integrator.Password = '1';

    this.settings.V3Settings.OfficeCode = 'S01';
    this.settings.V3Settings.StoreCode = 'S01';
    this.settings.V3Settings.WarehouseCode = 'S01';
    this.settings.V3Settings.DiscountType = 2;
    this.settings.V3Settings.SalesType = 3;
    this.settings.V3Settings.PaymentTerm = 30;
    this.settings.V3Settings.PriceGroupCode = 'HF';
    this.settings.V3Settings.DiscountTypeCode = '1';
    this.settings.V3Settings.DiscountReasonCode = '10';
    this.settings.V3Settings.IsPercentage = true;
    this.settings.V3Settings.POSTerminalID = "1";
    this.settings.V3Settings.PaymentCode = "100-01-0001";
    this.settings.V3Settings.IsProductPriceByGrCode = true;
    this.settings.V3Settings.IsSuspended = true;
  }

  //V3_TEST : SQL2012
  setSettingsV3TEST() {
    this.settings.G3Settings.ImageUrl = 'http://85.105.106.125:8084';
    this.settings.G3Settings.OrderCount = 10;

    this.settings.Integrator.Url = 'http://85.105.106.125:8083';
    this.settings.Integrator.UserGroupCode = 'V3';
    this.settings.Integrator.DatabaseName = 'V3_TEST';
    this.settings.Integrator.UserName = 'V3';
    this.settings.Integrator.Password = '1';

    this.settings.V3Settings.OfficeCode = 'M';
    this.settings.V3Settings.StoreCode = '';
    this.settings.V3Settings.DiscountType = 2;
    this.settings.V3Settings.SalesType = 3;
    this.settings.V3Settings.PaymentTerm = 30;
    this.settings.V3Settings.PriceGroupCode = 'PS';
    this.settings.V3Settings.DiscountTypeCode = '1';
    this.settings.V3Settings.DiscountReasonCode = '1';
    this.settings.V3Settings.IsPercentage = true;
    this.settings.V3Settings.POSTerminalID = "1";
    this.settings.V3Settings.PaymentCode = "100-001-300";
  }

  back() {
    this.viewCtrl.dismiss();
  }

  onSelectChange(event: number) {
    const newSettings: Settings = JSON.parse(JSON.stringify(this.settingsList[event]));
    this.settings = newSettings;
  }

  showEmptyListToast() {
    if (this.settingsList.length == 0) {
      this.translateService.get('LIST_EMPTY_TEXT').subscribe((value: string) => {
        this.presentToast(value);
      });
    }
  }
}
