import { Component } from '@angular/core';
import { NavController, NavParams, Loading, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { ServerService } from '../../classes/server.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-shipments',
  templateUrl: 'shipments.html',
})
export class ShipmentsPage {

  userData = {
    "ProcName": "G3_GetNotAcceptedShipments",
    "Parameters": [{ "Name": "StoreCode", "Value": "" }]
  };
  loading: Loading;
  list: any;

  shipmentData = {
    "ModelType": 0,
    "Lines": []
  }

  line = {
    "ShipmentHeaderID": "",
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public serverService: ServerService, public alertCtrl: AlertController,
    public translateService: TranslateService, public loadingCtrl: LoadingController, public toastCtrl: ToastController, ) {
  }

  ionViewDidLoad() {
    this.getShipments();
    console.log('ionViewDidLoad OpenOrdersPage');
  }

  getShipments() {
    this.presentLoading();
    this.userData.Parameters[0].Value = this.serverService.Settings.V3Settings.StoreCode;
    this.serverService.getAny(this.userData).then(res => {
      console.log(res);
      this.list = res;
      if (Array.isArray(res)) this.serverService.notAcceptedShipmentCount = res.length;
      this.dismissLoading();
    }).catch(this.handleError);
  }

  handleError(error: any) {
    this.dismissLoading();
    console.error('An error occurred', error);
    this.translateService.get(['ALERT_ERROR_TITLE_TEXT', 'ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT']).subscribe((value: string[]) => {
      this.showAlert(value['ALERT_ERROR_TITLE_TEXT'], value['ALERT_ERROR_SERVER_CONNECTION_MESSAGE_TEXT'] + error);
    });
  }

  presentLoading() {
    this.translateService.get('LOADING_CONTENT_TEXT').subscribe((value: string) => {
      this.loading = this.loadingCtrl.create({
        content: value,
      });
      this.loading.present();
    });
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  showAlert(title: string, subTitle: string) {
    this.translateService.get('ALERT_BUTTON_OK_TEXT').subscribe((value: string) => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: [value]
      });
      alert.present();
    });
  }

  private presentConfirmAlert(item) {
    let alert = this.alertCtrl.create({
      title: 'Mal Kabul',
      subTitle: item.ItemCode + ' nolu ürün kodu, ' + item.FromStoreDescription + ' mağazasından mal kabul işlemi Onaylıyor musunuz?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'İptal',
          role: 'cancel',
        },
        {
          text: 'Tamam',
          handler: () => {
            this.acceptShipment(item);
          }
        },
      ]
    });
    alert.present();
  }

  acceptShipment(item) {
    this.line.ShipmentHeaderID = item.ShipmentHeaderID;
    this.shipmentData.Lines.push(this.line);
    console.log(this.shipmentData);
    this.presentLoading();

    this.serverService.approveTransfer(this.shipmentData).then(res => {
      this.dismissLoading();
      console.log(res)
      if (res) {
        this.presentToast('Mal kabul işlemi tamamlandi.' + ' \r\n ' + res.Result);
        this.getShipments();
      }
    }).catch(this.handleError);
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: 'X'
    });
    toast.present();
  }

}
