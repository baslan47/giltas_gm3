import { Component } from '@angular/core';
import { CustomersPage } from '../customers/customers';
import { HomePage } from '../home/home';
import { ProposalPage } from '../proposal/proposal';
import { ServerService } from "../../classes/server.service";
import { ProductsPage } from "../products/products";
import { NotificationsPage } from '../notifications/notifications';

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {

    userData = {
        "ProcName": "",
        "Parameters": [{ "Name": "StoreCode", "Value": "" }]
    };

    ModuleLicense: string;

    tab1Root = HomePage;
    tab2Root;
    tab3Root;
    tab4Root;
    tab5Root = NotificationsPage;

    constructor(public serverService: ServerService) {
        this.ModuleLicense = this.serverService.Settings.G3License.Modules;
        if (this.ModuleLicense.charAt(0) != "0") {
            this.tab2Root = ProductsPage;
        }
        if (this.ModuleLicense.charAt(1) != "0") {
            this.tab3Root = CustomersPage;
        }
        if (this.ModuleLicense.charAt(2) != "0") {
            this.tab4Root = ProposalPage;
        }
        this.getOpenOrders();
        this.getNotAcceptedShipments();
    }

    getOpenOrders() {
        this.userData.ProcName = "G3_GetOpenOrders";
        this.userData.Parameters[0].Value = this.serverService.Settings.V3Settings.StoreCode;
        this.serverService.getAny(this.userData).then(res => {
            console.log(res);
            if (Array.isArray(res)) this.serverService.openOrdersCount = res.length;
        }).catch(this.handleError);
    }

    getNotAcceptedShipments() {
        this.userData.ProcName = "G3_GetNotAcceptedShipments";
        this.userData.Parameters[0].Value = this.serverService.Settings.V3Settings.StoreCode;
        this.serverService.getAny(this.userData).then(res => {
            console.log(res);
            if (Array.isArray(res)) this.serverService.notAcceptedShipmentCount = res.length;
        }).catch(this.handleError);
    }

    handleError(error: any) {
        console.error('An error occurred', error);        
    }
}
