import {Component} from '@angular/core';
import {Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ServerService} from "../../classes/server.service";

@Component({
    selector: 'page-warehouse',
    templateUrl: 'warehouse.html',
})
export class WarehousePage {

    loading: Loading;
    product: any;
    color: any;
    size: any;
    qty: any[] = new Array();
    realqty: any;
    tempItemDim: any[] = [];
    productPrices: any[] = [];
    checkdatabase: number = 0;
    productColorSizeMatrix: any[] = [];
    PaymentPlanDescriptions: any[] = [];    
    colHeaders: any;
    ProductWarehouse: any[] = [];
    storeData = {"ProcName": "", "Parameters": [{"Name": "ItemCode", "Value": ""}, {"Name": "StoreCode", "Value": ""},
            {"Name": "WarehouseCode", "Value": ""}]};

    getAvailableInventoryV3 = {
        "ProcName": "G3_GetAvailableInventory", "Parameters": [{ "Name": "ItemCode", "Value": "" },
        { "Name": "ColorCode", "Value": "" }]
        };            

    constructor(public navCtrl: NavController, public navParams: NavParams, public serverService: ServerService,
                public loadingCtrl: LoadingController,) {
        this.product = this.navParams.data.product;
        this.productPrices = this.navParams.data.productPrices;
        this.productColorSizeMatrix = this.navParams.data.productColorSizeMatrix;
        this.colHeaders = this.navParams.data.colHeaders;
        this.PaymentPlanDescriptions = this.navParams.data.PaymentPlanDescriptions;
        this.color = this.product.ColorCode ? this.product.ColorCode : '';
        this.size = this.product.ItemDim1Code ? this.product.ItemDim1Code : '';
        //JS if(this.serverService.Settings.Integrator.DatabaseName == 'TEST_HAMA' || this.serverService.Settings.Integrator.DatabaseName == 'Data1_Hamam' || this.serverService.Settings.Integrator.DatabaseName == 'Data2_Eke' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR' || this.serverService.Settings.Integrator.DatabaseName == 'Hama_FR2'){
        if (this.serverService.Settings.Integrator.DatabaseName != 'SANALV3') {
            this.checkdatabase = 1;
        }  

        console.log(this.color + " " + this.size);
        console.log(this.product);
        console.log(this.productPrices);
        console.log(this.PaymentPlanDescriptions);
        console.log(this.productColorSizeMatrix);
        console.log(this.colHeaders);  
    }

    ngOnInit() {        
        if(this.checkdatabase != 1){
            this.presentLoading();
            this.storeData.ProcName = "G3_GetProductWarehouse";
            this.storeData.Parameters[0].Value = this.product.ItemCode;        
            this.storeData.Parameters[1].Value = (this.serverService.Settings.V3Settings.OfficeCode) ? this.serverService.Settings.V3Settings.OfficeCode : "";
            this.storeData.Parameters[2].Value = (this.serverService.Settings.V3Settings.WarehouseCode) ? this.serverService.Settings.V3Settings.WarehouseCode : "";
            this.serverService.getAny(this.storeData).then(res => {
                console.log('G3_GetProductWarehouse');
                console.log(res);
                this.ProductWarehouse = res; 
                this.qty = this.ProductWarehouse.filter(x => x.ColorCode == this.color && x.ItemDim1Code == this.size);
                this.realqty = this.qty[0]['Inventory'];
                console.log(this.qty[0]['Inventory']);
                this.dismissLoading();
    
            }).catch(error => this.handleError(error));                       
        }else{                    
            if(this.checkdatabase == 1){
                for(let i = 0; i < this.productColorSizeMatrix.length; i++){
                  this.tempItemDim = [];
                  this.getAvailableInventoryV3.Parameters[0].Value = this.product.ItemCode;
                  this.getAvailableInventoryV3.Parameters[1].Value = this.productColorSizeMatrix[i]['R/B'];    
                  this.serverService.getAny(this.getAvailableInventoryV3).then(res => {
                    console.log(res);   
                    this.tempItemDim = res; 
                    if(this.size != '' && this.color != ''){
                        if(this.tempItemDim[0]['ColorCode'] == this.color && this.tempItemDim[0]['ItemDim1Code'] == this.size){
                            this.realqty = this.tempItemDim[0]['AvailableInventoryQty1'];
                        } 
                    }                                                                         
                    for(let k = 0; k < this.tempItemDim.length; k++){
                      this.productColorSizeMatrix[i][this.tempItemDim[k]['ItemDim1Code']] = this.tempItemDim[k]['AvailableInventoryQty1'];                      
                    }
                  }).catch(error => this.handleError(error));                        
                }                
                console.log(this.realqty);                  
              }
        }       
    }

    public presentLoading() {
        this.loading = this.loadingCtrl.create({
            content: "Lütfen bekleyiniz...",
        });
        this.loading.present();
    }

    public dismissLoading() {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    }

    getPriceGroup(item: string){
        var tempPriceGroupCode: string;    
        if(item.charAt(0) == "€"){      
          tempPriceGroupCode = '€'
          return tempPriceGroupCode;
        }else if(item.charAt(0) == "$"){
          tempPriceGroupCode = '$'      
          return tempPriceGroupCode;
        }else{
          tempPriceGroupCode = '₺'      
          return tempPriceGroupCode;
        }       
    }


    private handleError(error: any) {
        this.dismissLoading();
        console.error('An error occurred', error);
    }

    getData(dim1: any, item: any){
        if(dim1.ItemDim1Code == 'R/B'){
            const color = this.serverService.getColorDescription(item[dim1.ItemDim1Code]);
            
            return color;
        }else{
            const value = item[dim1.ItemDim1Code];
            return value;
        }
    }
}
