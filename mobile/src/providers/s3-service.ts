import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as aws from "aws-sdk";
import {  
   AlertController,
} from "ionic-angular";

@Injectable()
export class AwsServiceProvider {
    
  i = 0; 
  data = [];  
  awsIdLine = [1, 3, 5, 7, 9, 12];  

  constructor(public http: HttpClient, private loader: AlertController) {
    console.log('Hello AwsServiceProvider Provider');
  }

  public SYSTEM_PARAMS = {
    REGION: "us-west-2",
    COGNITO_POOL: {
      UserPoolId: "us-west-2_Ab129faBb",
      ClientId: "7lhlkkfbfb4q5kpp90urffao"
    },
    COGNITO_IDENTITY: {
      IDENTITY_POOL_ID: "us-west-2:71c844ce-6e16-42f9-960f-67bea0405111"
    },
    S3: {
      BUCKET_NAME: "connectionbckt"
    }
  };

  upload(image, imageName) {    
    return new Promise((resolve, reject) => {
      aws.config.region = this.SYSTEM_PARAMS.REGION;
      aws.config.credentials = new aws.CognitoIdentityCredentials({
        IdentityPoolId: this.SYSTEM_PARAMS.COGNITO_IDENTITY.IDENTITY_POOL_ID        
      });
  
      var s3 = new aws.S3({
        apiVersion: "2006-03-01",
        params: { Bucket: this.SYSTEM_PARAMS.S3.BUCKET_NAME }
      });
  
      let buf = new Buffer(image, "base64");
  
      var data = {
        Bucket: this.SYSTEM_PARAMS.S3.BUCKET_NAME,
        Key: imageName,
        Body: buf,
        ContentEncoding: "base64",
        ContentType: "image/jpeg"
      };
  
      s3.putObject(data, (err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res);
        }
      }); 

    });
  }

  getBinary(encodedFile) {
    var base64Image = encodedFile.split("data:image/jpeg;base64,")[1];
    var binaryImg = atob(base64Image);
    var length = binaryImg.length;
    var ab = new ArrayBuffer(length);
    var ua = new Uint8Array(ab);
    for (var i = 0; i < length; i++) {
      ua[i] = binaryImg.charCodeAt(i);
    }

    // var blob = new Blob([ab], {
    //   type: "image/jpeg"
    // });

    return ab;
  }

  text_rekognition(image, check){

    let loading = this.loader.create({
      title: 'Lütfen Bekleyin',
      subTitle: 'Alanlar Oluşturuluyor...',
    });

    loading.present();  

    return new Promise((resolve, reject) => {
      
      aws.config.region = this.SYSTEM_PARAMS.REGION;
      aws.config.credentials = new aws.CognitoIdentityCredentials({
        IdentityPoolId: this.SYSTEM_PARAMS.COGNITO_IDENTITY.IDENTITY_POOL_ID        
      });

      var recog = new aws.Rekognition({
        apiVersion: "2016-06-27"
      });
      
      var data = {
        Image: {
          Bytes: this.getBinary(image),
        }        
      }      

      recog.detectText(data,(err, res) =>{
        if (err) {
          console.log(err, err.stack);
        } else if(!check){   
          this.i = 0;                    
          this.data = [];
          res.TextDetections.filter(x => x.Type == 'LINE').forEach(val => {
            if(val.Id == this.awsIdLine[this.i]){
              if(val.Id == 12){
                var tempString = val.DetectedText.indexOf(" ");
                var DY = val.DetectedText.substr(0, tempString);
                var DT = val.DetectedText.substr(tempString + 1, val.DetectedText.length - tempString);                
                console.log(DY);
                this.data.push(DY);
                console.log(DT);
                this.data.push(DT);
              }else{
                this.data.push(val.DetectedText);
                this.i++;
              }              
            }                    
          })
          console.log(this.data);                             
          resolve(this.data);
          loading.dismiss();                   
        }
        else if(check){                                     
          res.TextDetections.filter(x => x.Type == 'LINE').forEach(val => {
            if(val.Id == 8){
              this.data.push(val.DetectedText);
            }
            if(val.Id == 10){
              var tempString = val.DetectedText.indexOf(" ");
              var cNo = val.DetectedText.substr(0, tempString);
              this.data.push(cNo);
              console.log(cNo);
              var tempString2 = val.DetectedText.lastIndexOf(" ");
              var aSNo = val.DetectedText.substr(tempString + 1, tempString2 - tempString - 1); 
              this.data.push(aSNo);             
              console.log(aSNo);
              var sNo = val.DetectedText.substr(tempString2 + 1, val.DetectedText.length - tempString);
              this.data.push(sNo);     
              console.log(sNo);
            }
          })          
          console.log(this.data);                             
          resolve(this.data);
          loading.dismiss();  
        }
      });
    })
  }

  amazon_Polly(){    
    return new Promise((resolve, reject) => {
      aws.config.region = this.SYSTEM_PARAMS.REGION;
      aws.config.credentials = new aws.CognitoIdentityCredentials({
        IdentityPoolId: this.SYSTEM_PARAMS.COGNITO_IDENTITY.IDENTITY_POOL_ID        
      });

      var polly = new aws.Polly({
        apiVersion: "2016-06-10"
      });
      
      var data = {
        LanguageCode: "tr-TR"
      };

      polly.describeVoices(data, (err, res) => {
        if(err){
          console.log(err, err.stack);
        }else{
          console.log(res);
          resolve(res);
        }
      })
    })
  }
}
