<?php
ini_set('display_errors', 'Off');
include 'dao/settings.php';
include 'logincheck.php';
include 'languages.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Giltaş Mobile</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Giltaş Mobile" />
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" /> 
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <script type="application/x-javascript"> 
            addEventListener("load", function() { 
            setTimeout(hideURLbar, 0); 
            }, false); 
            function hideURLbar(){ 
            window.scrollTo(0,1); 
            }
        </script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <link href="<?php echo $GLOBALS['ROOT'] ?>css/bootstrap.min.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href="<?php echo $GLOBALS['ROOT'] ?>css/style.css" rel='stylesheet' type='text/css' />
        <link href="<?php echo $GLOBALS['ROOT'] ?>css/font-awesome.css" rel="stylesheet"/> 
        <script src="<?php echo $GLOBALS['ROOT'] ?>js/jquery.min.js"></script>
        <script>var $m = jQuery.noConflict();</script>

        <!--Mainly scripts -->
        <script src="<?php echo $GLOBALS['ROOT'] ?>js/jquery.metisMenu.js"></script>
        <script src="<?php echo $GLOBALS['ROOT'] ?>js/jquery.slimscroll.min.js"></script>
        <!-- Custom and plugin javascript -->
        <link href="<?php echo $GLOBALS['ROOT'] ?>css/custom.css" rel="stylesheet"/>
        <script src="<?php echo $GLOBALS['ROOT'] ?>js/custom.js"></script>
        <script src="<?php echo $GLOBALS['ROOT'] ?>js/screenfull.js"></script>
        <script>
            $m(function () {
                $m('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

                if (!screenfull.enabled) {
                    return false;
                }
                $m('#toggle').click(function () {
                    screenfull.toggle($m('#container')[0]);
                });
            });
        </script>       
        <!--skycons-icons-->
        <script src="<?php echo $GLOBALS['ROOT'] ?>js/skycons.js"></script>
        <!--//skycons-icons-->

        <!--scrolling js-->
        <!-- <script src="<?php echo $GLOBALS['ROOT'] ?>js/jquery.nicescroll.js"></script> -->
        <!-- <script src="<?php echo $GLOBALS['ROOT'] ?>js/scripts.js"></script> -->
        <!--//scrolling js-->
        <script src="<?php echo $GLOBALS['ROOT'] ?>js/bootstrap.min.js"></script>

        <!--Easyui mobile css & js-->
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['ROOT'] ?>lib/easyui/themes/metro/easyui.css"/>  
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['ROOT'] ?>lib/easyui/themes/mobile.css"/>  
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['ROOT'] ?>lib/easyui/themes/icon.css"/>  
        <script type="text/javascript" src="<?php echo $GLOBALS['ROOT'] ?>lib/easyui/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['ROOT'] ?>lib/easyui/jquery.easyui.min.js"></script> 
        <script type="text/javascript" src="<?php echo $GLOBALS['ROOT'] ?>lib/easyui/jquery.easyui.mobile.js"></script> 
        <!--Sabah Tech css & js-->
        <script type="text/javascript" src="<?php echo $GLOBALS['ROOT'] ?>js/main.js"></script> 
        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['ROOT'] ?>css/main.css"/>    

        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBALS['ROOT'] ?>js/lightbox/themes/default/jquery.lightbox.css" />
        <script type="text/javascript" src="<?php echo $GLOBALS['ROOT'] ?>js/lightbox/jquery.lightbox.min.js"></script>
        <script type="text/javascript" src="<?php echo $GLOBALS['ROOT'] ?>js/datagrid-groupview.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('.lightbox').lightbox();
            });
        </script>

        <script type="text/javascript" >
            $(function () {
                $.ajax({
                    type: "POST",
                    url: "<?php echo $GLOBALS['ROOT'] ?>operations/notification_operations.php",
                    data: {op: 2},
                    dataType: "json",
                    success: function (result) {
                        if (result.success) {
                            if (result.count === "0") {
                                $("#notification-count").hide();
                            } else {
                                $("#notification-count").show();
                                $("#notification-count").text(result.count);
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);
                    }
                });
            });
        </script>
    </head>
    <body>
        <!--<div id="wrapper">-->
        <!----->
        <nav class="navbar-default navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<h1> <a class="navbar-brand" href="index.php">METAL WORK</a></h1>-->
                <a href="<?php echo $GLOBALS['ROOT'] ?>index.php"> <img src="<?php echo $GLOBALS['ROOT'] ?>images/logo.png"/> </a>
            </div>

            <!-- Brand and toggle get grouped for better mobile display -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="drop-men" >
                <ul class=" nav_1">

                    <li class="dropdown at-drop">
                        <a href="#" class="dropdown-toggle dropdown-at " data-toggle="dropdown"><i class="fa fa-globe"></i> <span id = "notification-count" class="number"></span></a>
                    </li>
                    <li class="dropdown">
                        <a href="<?php echo $GLOBALS['ROOT'] ?>operations/notifications.php" class="dropdown-toggle dropdown-at" data-toggle="dropdown">
                            <span class=" name-caret"><?php echo $_SESSION["login_name"]; ?>
                                <i class="caret"></i>
                            </span>
                            <img src="<?php echo $GLOBALS['ROOT'] ?>images/wo.png">
                        </a>
                        </a>
                        <ul class="dropdown-menu " role="menu">
                            <li><a href="<?php echo $GLOBALS['ROOT'] ?>profile.php"><i class="fa fa-user"></i><?php echo $lang['profile'] ?></a></li>
                            <li><a href="<?php echo $GLOBALS['ROOT'] ?>operations/notifications.php"><i class="fa fa-envelope"></i><?php echo $lang['notifications'] ?></a></li>  
                            <li><a href="<?php echo $GLOBALS['ROOT'] ?>operations/send_notification.php"><i class="fa fa-send"></i><?php echo $lang['send_notification'] ?></a></li>                              
                            <li><a href="<?php echo $GLOBALS['ROOT'] ?>logout.php"><i class="fa fa-lock"></i><?php echo $lang['logout'] ?></a></li>
                        </ul>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
            <div class="clearfix">

            </div>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <li>
                            <a href="index.php" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label"><?php echo $lang['dashboard'] ?></span> </a>
                        </li>
                        <?php if ($_SESSION["login_user_group_id"] == 1) { ?>
                            <li>
                                <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label"><?php echo $lang['definitions'] ?></span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li><a href="<?php echo $GLOBALS['ROOT'] ?>definitions/users.php" class=" hvr-bounce-to-right"> <i class="fa fa-list nav_icon"></i><?php echo $lang['users'] ?></a></li>
                                    <li><a href="<?php echo $GLOBALS['ROOT'] ?>definitions/user_groups.php" class=" hvr-bounce-to-right"><i class="fa fa-group nav_icon"></i><?php echo $lang['user_groups'] ?></a></li>
                                </ul>
                            </li>
                        <?php } ?>
                        <li>

                            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label"><?php echo $lang['operations'] ?></span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">

                                <li><a href="<?php echo $GLOBALS['ROOT'] ?>definitions/personel.php" class=" hvr-bounce-to-right"> <i class="fa fa-list nav_icon"></i>Personel</a></li>
                                <?php if ($_SESSION["login_user_group_id"] == 1) { ?>
                                    <li><a href="<?php echo $GLOBALS['ROOT'] ?>definitions/customer.php" class=" hvr-bounce-to-right"> <i class="fa fa-list nav_icon"></i><?php echo $lang['customers'] ?></a></li>
                                    <li><a href="<?php echo $GLOBALS['ROOT'] ?>operations/notifications.php" class=" hvr-bounce-to-right"> <i class="fa fa-list nav_icon"></i><?php echo $lang['notifications'] ?></a></li>
                                    <li><a href="#" class=" hvr-bounce-to-right"> <i class="fa fa-list nav_icon"></i><?php echo $lang['authentication'] ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li>
                            <a href="" class=" hvr-bounce-to-right"><i class="fa fa-picture-o nav_icon"></i> <span class="nav-label"><?php echo $lang['reports'] ?></span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">         
                            </ul>
                        </li>
                </div>
            </div>
        </nav>            
    </body>
</html>
