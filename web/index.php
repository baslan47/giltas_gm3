<?php
//echo "<script type='text/javascript'>alert('index');</script>";
include 'header.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $lang['crm'] ?></title> 
        <script type="text/javascript">
            Array.prototype.reduce = undefined;
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="content-main">
                    <!--banner-->	
                    <div class="banner">
                        <h2>
                            <a href="index.php">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <span><?php echo $lang['dashboard'] ?></span>
                        </h2>
                    </div>
                    <!--//banner-->
                    <?php
                    include 'dashboard/dashboard.php';
                    ?>

                </div>
                <?php
                include 'footer.php';
                ?>
            </div>
        </div>        
    </body>
</html>
