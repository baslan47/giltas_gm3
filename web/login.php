<?php
//echo "<script type='text/javascript'>alert('login');</script>";
require 'languages.php';
?>

<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">  
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <title>Giltaş Mobile</title>  
        <link rel="stylesheet" type="text/css" href="lib/easyui/themes/metro/easyui.css">  
        <link rel="stylesheet" type="text/css" href="lib/easyui/themes/mobile.css">  
        <link rel="stylesheet" type="text/css" href="lib/easyui/themes/icon.css">  
        <link rel="stylesheet" type="text/css" href="css/main.css">        
        <script type="text/javascript" src="lib/easyui/jquery.min.js"></script>  
        <script type="text/javascript" src="lib/easyui/jquery.easyui.min.js"></script> 
        <script type="text/javascript" src="lib/easyui/jquery.easyui.mobile.js"></script> 
        <script type="text/javascript" src="js/jquery_cookie.js"></script> 
    </head>

    <script type="text/javascript" >
        function checkLogin() {
            var username = document.getElementById('username').value;
            var password = document.getElementById('password').value;
            connect(username,password);
        }
        
        function connect(username, password){
            $.ajax({
                type: "POST",
                url: "definitions/user_operations.php",
                data: {op: 4, username: username, password: password},
                dataType: "json",
                success: function (result) {
                    if (result.success) {
                        remember_me();
                        window.location = 'index.php';
                    } else {
                        $("#login_error").css("visibility", "visible");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                }
            });
        }

        function runFunctionIfEnter(e) {
            var keynum;
            if (window.event) {
                keynum = e.keyCode;
            } // IE
            else if (e.which) {
                keynum = e.which;
            }// other browsers
            if (keynum === 13) {  // if the key that was pressed was Enter (ascii code 13)                
                checkLogin();
            }
        }

        jQuery(document).keypress(function (e) {
            runFunctionIfEnter(e);
        });
        
        function load_me() {
            var u = $.cookie("username"); //"USERNAME" COOKIE
            var p = $.cookie("password"); //"PASSWORD" COOKIE                 
            if (u === '' || u === undefined || p === undefined || p === '') {
                return;
            }
            connect(u,p);
        }
        
        function remember_me() {
            if ($("#checkbox").prop('checked') === true) {
                var u = $("#username").val(); //VALUE OF USERNAME
                var p = $("#password").val(); //VALUE OF PASSWORD
                $.cookie("username", u, {expires: 3650}); //SETS IN DAYS (10 YEARS)
                $.cookie("password", p, {expires: 3650}); //SETS IN DAYS (10 YEARS)                
            }
        }
    </script> 

    <body onload="load_me();" style="background-image:url('images/login-background.jpg'); background-repeat:repeat;">
        <div class="top-Center">
            <div class="easyui-navpanel">
                <header>
                    <div class="m-toolbar">
                        <span class="m-title"><?php echo $lang['welcome_msg'] ?></span>
                    </div>
                </header>
                <div style="margin:20px auto;width:175px;height:105px;overflow:hidden">
                    <img src="images/logo.png" style="margin:0;width:100%;height:100%;">
                </div>
                <div style="padding:0 20px">
                    <div style="margin-bottom:10px">
                        <input id="username" class="easyui-textbox" data-options="prompt:'Kullanıcı Adı',iconCls:'icon-man'" style="width:100%;height:38px">
                    </div>
                    <div>
                        <input id="password" class="easyui-passwordbox" data-options="prompt:'Şifre'" style="width:100%;height:38px">
                    </div>
                    <div class="check">
                        <label ><input type="checkbox" id="checkbox" name="checkbox" ><i> </i><?php echo $lang['keep me logged in'] ?></label>                        
                        <label class="login-error" id="login_error" name="login_error" ><?php echo $lang['invalidLogin'] ?></label> 
                    </div>                    
                    <div style="text-align:center;margin-top:20px">
                        <a href="#" class="easyui-linkbutton" onclick="checkLogin();" style="width:100%;height:40px"><span style="font-size:16px"><?php echo $lang['login'] ?></span></a>
                    </div>
                    <div style="text-align:center;margin-top:20px">
                        <a href="#" class="easyui-linkbutton" plain="true" outline="true" style="width:100px;height:35px"><span style="font-size:16px"><?php echo $lang['register'] ?></span></a> 
                    </div>
                </div>
            </div>
        </div>
    </body>    
</html>