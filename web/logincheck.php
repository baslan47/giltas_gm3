<?php
if (!isset($_SESSION)) {    
    session_start();
}

if (isset($_SESSION["login"])) {
    $loggedIn = $_SESSION["login"];
} else {
    $loggedIn = false;
}
if (!$loggedIn) {
    $msg = $GLOBALS['FULL_ROOT'];
    header("Location:". $GLOBALS['ROOT'] . "login.php");       
    exit;
}
?>
