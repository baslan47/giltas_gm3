<?php
session_start();
if (isset($_SESSION['login'])) {
    unset($_SESSION['login']);
    unset($_SESSION['authentication']);
}
?>
<script type="text/javascript" src="lib/easyui/jquery.min.js"></script>  
<script type="text/javascript" >
    $(function () {
        document.cookie = 'password=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        window.location = 'login.php';
    });
</script>

