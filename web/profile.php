<?php
include 'header.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo $lang['profile'] ?></title>
        <link href="css/notification.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="js/main.js"></script>        
        <script type="text/javascript" src="lib/noty/packaged/jquery.noty.packaged.min.js"></script>
        <script>
            var userId;
            $(document).ready(function () {
                $.ajax({
                    type: "POST",
                    url: "definitions/user_operations.php",
                    data: {op: 6},
                    dataType: "json",
                    success: function (result) {
                        if (result.success) {
                            $("#name").val(result.user.name);
                            $("#phone").val(result.user.phone);
                            $("#email").val(result.user.email);
                            $("#username").val(result.user.username);
                            $("#password").val('');
                            $("#address").val(result.user.address);
                            $("#language").val(result.user.language);
                            userId = result.user.id;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);

                    }
                });
                //get supported languages.
                $.ajax({
                    type: "POST",
                    url: "definitions/user_operations.php",
                    data: {op: 12},
                    dataType: "json",
                    success: function (result) {
                        var select = document.getElementById('language');
                        for (var i = 0; i < result.length; i++) {
                            var option = document.createElement("option");
                            option.id = result[i].id;
                            option.value = result[i].id;
                            option.innerHTML = result[i].name;
                            select.appendChild(option);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);

                    }
                });
            });
            function save() {
                var username = $("#username").val();
                var name = $("#name").val();
                var phone = $("#phone").val();
                var password = $("#password").val();
                var email = $("#email").val();
                var address = $("#address").val();
                var language = $("#language").val();
                $.ajax({
                    type: "POST",
                    url: "definitions/user_operations.php",
                    data: {op: 1, id: userId, username: username, name: name, phone: phone, email: email, password: password, address: address, language: language},
                    dataType: "json",
                    success: function (result) {
                        if (result.success) {
                            generateNotify("success", "Saved successfully");

                        } else {
                            generateNotify("error", result.msg);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "languages.php",
                    data: {lang: language},
                    dataType: "json",
                    success: function (result) {
                        if (result.success) {
                        } else {
                            generateNotify("error", result.msg);
                        }
                    }
                });
            }

        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="content-main">
                    <!--banner-->                    
                    <div class="banner">
                        <h2>
                            <a href="index.php"><?php echo $lang['home'] ?></a>
                            <i class="fa fa-angle-right"></i>
                            <span><?php echo $lang['profile'] ?></span>
                        </h2>
                    </div>
                    <!--//banner-->
                    <div class="content-easyui">
                        <form action="" method="post" autocomplete="off">
                            <section id="content">
                                <div id="content-div">
                                    <label for="name"> <?php echo $lang['name'] ?><?php echo $lang[':'] ?> </label>
                                    <input type="text"  id="name"/>
                                </div>
                                <div>
                                    <label> <?php echo $lang['phone'] ?><?php echo $lang[':'] ?> </label>
                                    <input type="text" id="phone" />
                                </div>
                                <div>
                                    <label> <?php echo $lang['email'] ?>:<?php echo $lang[''] ?></label>
                                    <input type="text" id="email" />
                                </div>
                                <div>
                                    <label> <?php echo $lang['username'] ?><?php echo $lang[':'] ?></label>
                                    <input type="text"  id="username"/>
                                </div>
                                <div>
                                    <label> <?php echo $lang['password'] ?><?php echo $lang[':'] ?></label>
                                    <input type="password"  id="password"/>
                                </div>
                                <div>
                                    <label> <?php echo $lang['address'] ?><?php echo $lang[':'] ?></label>
                                    <input type="address"  id="address"/>     
                                </div>
                                <div>
                                    <label> <?php echo $lang['language'] ?><?php echo $lang[':'] ?></label>                                    
                                    <select name="language" id="language">                                                                            
                                    </select>
                                </div>

                                <input type="button" value="<?php echo $lang['save'] ?>"  onclick="save();"/>
                            </section>
                        </form>
                    </div>
                    <?php include 'footer.php'; ?>
                </div>
            </div>
        </div>
    </body>
</html>
