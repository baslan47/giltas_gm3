<?php
define("ROOT", $_SERVER['DOCUMENT_ROOT']);
require_once (ROOT . '/crm/languages.php');
include ROOT . '/crm/config_db.php';
?>
<html>
<!doctype html>
<head>

    <!-- Basics -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Signup</title>

    <!-- CSS -->    
    <link rel="stylesheet" href="css/login-style.css">
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>   

    <link href="/crm/css/notification.css" rel="stylesheet" type="text/css" />
<!--    <script type="text/javascript" src="/crm/js/main.js"></script>-->
    <script type="text/javascript" src="lib/noty/packaged/jquery.noty.packaged.min.js"></script>

    <script type="text/javascript" >
        function register() {
            var name = document.getElementById('name').value;
            var email = document.getElementById('email').value;
            var phone = document.getElementById('phone').value;
            var username = document.getElementById('username').value;
            var password = document.getElementById('password').value;
            var address = document.getElementById('address').value;

            $.ajax({
                url: 'definitions/user_operations.php',
                type: "post",
                data: {op: 0, name: name, email: email, phone: phone, username: username, password: password, user_group_id: 3, address: address},
                dataType: 'json',
                success: function (result) {
                    if (result.success) {
                        generateNotify('success', 'Registration info saved succesfully.');
                        window.location = 'login.php';
                    } else {
                        generateNotify('alert', result.msg);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                }
            });
            sleep(2000);
        }
       
        function sleep(delay) {
            var start = new Date().getTime();
            while (new Date().getTime() < start + delay)
                ;
        }

        function runFunctionIfEnter(e) {
            var keynum;
            if (window.event) {
                keynum = e.keyCode;
            } // IE
            else if (e.which) {
                keynum = e.which;
            }// other browsers
            if (keynum === 13) {  // if the key that was pressed was Enter (ascii code 13)                
                register();
            }
        }

        jQuery(document).keypress(function (e) {
            runFunctionIfEnter(e);
        });
        function generateNotify(type, message) {
            var n = noty({
                text: message,
                type: type,
                dismissQueue: true,
                animation: {
                    open: {height: 'toggle'}, // jQuery animate function property object
                    close: {height: 'toggle'}, // jQuery animate function property object
                    easing: 'swing', // easing
                    speed: 500 // opening & closing animation speed
                }
            });
        }
         function cancelRegistering(){
            window.location = 'login.php'; 
        }

    </script>

</head>

<!-- Main HTML -->

<body>
    <div id="container_signup">
        <form id="login" method="POST">
            <input id="name" name="name" type="name" placeHolder="<?php echo $lang['name'] ?>" >
            <input id="email" name="email" type="name" placeHolder="<?php echo $lang['email'] ?>">   
            <input id="phone" name="phone" type="name" placeHolder="<?php echo $lang['phone'] ?>">   
            <input id="username" name="username" type="name" placeHolder="<?php echo $lang['username'] ?>" >
            <input id="password" name="password" type="password" placeHolder="<?php echo $lang['password'] ?>">            
            <input id="address" name="address" type="name" placeHolder="<?php echo $lang['address'] ?>" >            

            <div id="lower">                
              <input name="cancel" type="button" value="<?php echo $lang['cancel'] ?>" onclick="cancelRegistering();">
               <input name="submit" type="button" value="<?php echo $lang['register'] ?>" onclick="register();"> 
            </div>
        </form>
    </div>
    <!-- End Page Content -->
</body>
</html>